#!/bin/bash

export ANT_OPTS=-Xmx512m

export DBFLUTE_HOME=../mydbflute/dbflute-1.0.5A

export MY_PROJECT_NAME=mysql

export MY_PROPERTIES_PATH=build.properties
