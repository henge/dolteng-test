package dltng.dbflute.bsentity.dbmeta;

import java.util.List;
import java.util.Map;

import org.seasar.dbflute.DBDef;
import org.seasar.dbflute.Entity;
import org.seasar.dbflute.dbmeta.AbstractDBMeta;
import org.seasar.dbflute.dbmeta.PropertyGateway;
import org.seasar.dbflute.dbmeta.info.*;
import org.seasar.dbflute.dbmeta.name.*;
import dltng.dbflute.allcommon.*;
import dltng.dbflute.exentity.*;

/**
 * The DB meta of PRODUCTS. (Singleton)
 * @author DBFlute(AutoGenerator)
 */
public class ProductsDbm extends AbstractDBMeta {

    // ===================================================================================
    //                                                                           Singleton
    //                                                                           =========
    private static final ProductsDbm _instance = new ProductsDbm();
    private ProductsDbm() {}
    public static ProductsDbm getInstance() { return _instance; }

    // ===================================================================================
    //                                                                       Current DBDef
    //                                                                       =============
    public DBDef getCurrentDBDef() { return DBCurrent.getInstance().currentDBDef(); }

    // ===================================================================================
    //                                                                    Property Gateway
    //                                                                    ================
    protected final Map<String, PropertyGateway> _epgMap = newHashMap();
    {
        setupEpg(_epgMap, new EpgId(), "id");
        setupEpg(_epgMap, new EpgCategoryId(), "categoryId");
        setupEpg(_epgMap, new EpgName(), "name");
        setupEpg(_epgMap, new EpgPrice(), "price");
        setupEpg(_epgMap, new EpgType(), "type");
        setupEpg(_epgMap, new EpgValidFlg(), "validFlg");
        setupEpg(_epgMap, new EpgRegistered(), "registered");
    }
    public PropertyGateway findPropertyGateway(String propertyName)
    { return doFindEpg(_epgMap, propertyName); }
    public static class EpgId implements PropertyGateway {
        public Object read(Entity e) { return ((Products)e).getId(); }
        public void write(Entity e, Object v) { ((Products)e).setId(cti(v)); }
    }
    public static class EpgCategoryId implements PropertyGateway {
        public Object read(Entity e) { return ((Products)e).getCategoryId(); }
        public void write(Entity e, Object v) { ((Products)e).setCategoryId(cti(v)); }
    }
    public static class EpgName implements PropertyGateway {
        public Object read(Entity e) { return ((Products)e).getName(); }
        public void write(Entity e, Object v) { ((Products)e).setName((String)v); }
    }
    public static class EpgPrice implements PropertyGateway {
        public Object read(Entity e) { return ((Products)e).getPrice(); }
        public void write(Entity e, Object v) { ((Products)e).setPrice(cti(v)); }
    }
    public static class EpgType implements PropertyGateway {
        public Object read(Entity e) { return ((Products)e).getType(); }
        public void write(Entity e, Object v) { ((Products)e).setType(cti(v)); }
    }
    public static class EpgValidFlg implements PropertyGateway {
        public Object read(Entity e) { return ((Products)e).getValidFlg(); }
        public void write(Entity e, Object v) { ((Products)e).setValidFlg(cti(v)); }
    }
    public static class EpgRegistered implements PropertyGateway {
        public Object read(Entity e) { return ((Products)e).getRegistered(); }
        public void write(Entity e, Object v) { ((Products)e).setRegistered((java.sql.Timestamp)v); }
    }

    // ===================================================================================
    //                                                                          Table Info
    //                                                                          ==========
    protected final String _tableDbName = "PRODUCTS";
    protected final String _tablePropertyName = "products";
    protected final TableSqlName _tableSqlName = new TableSqlName("PRODUCTS", _tableDbName);
    { _tableSqlName.xacceptFilter(DBFluteConfig.getInstance().getTableSqlNameFilter()); }
    public String getTableDbName() { return _tableDbName; }
    public String getTablePropertyName() { return _tablePropertyName; }
    public TableSqlName getTableSqlName() { return _tableSqlName; }

    // ===================================================================================
    //                                                                         Column Info
    //                                                                         ===========
    protected final ColumnInfo _columnId = cci("ID", "ID", null, null, true, "id", Integer.class, true, true, "INT", 10, 0, null, false, null, null, null, null, null);
    protected final ColumnInfo _columnCategoryId = cci("CATEGORY_ID", "CATEGORY_ID", null, null, true, "categoryId", Integer.class, false, false, "INT", 10, 0, null, false, null, null, null, null, null);
    protected final ColumnInfo _columnName = cci("NAME", "NAME", null, null, true, "name", String.class, false, false, "VARCHAR", 100, 0, null, false, null, null, null, null, null);
    protected final ColumnInfo _columnPrice = cci("PRICE", "PRICE", null, null, true, "price", Integer.class, false, false, "INT", 10, 0, null, false, null, null, null, null, null);
    protected final ColumnInfo _columnType = cci("TYPE", "TYPE", null, null, true, "type", Integer.class, false, false, "INT", 10, 0, null, false, null, null, null, null, null);
    protected final ColumnInfo _columnValidFlg = cci("VALID_FLG", "VALID_FLG", null, null, true, "validFlg", Integer.class, false, false, "INT", 10, 0, null, false, null, null, null, null, CDef.DefMeta.Flg);
    protected final ColumnInfo _columnRegistered = cci("REGISTERED", "REGISTERED", null, null, true, "registered", java.sql.Timestamp.class, false, false, "TIMESTAMP", 19, 0, "CURRENT_TIMESTAMP", false, null, null, null, null, null);

    public ColumnInfo columnId() { return _columnId; }
    public ColumnInfo columnCategoryId() { return _columnCategoryId; }
    public ColumnInfo columnName() { return _columnName; }
    public ColumnInfo columnPrice() { return _columnPrice; }
    public ColumnInfo columnType() { return _columnType; }
    public ColumnInfo columnValidFlg() { return _columnValidFlg; }
    public ColumnInfo columnRegistered() { return _columnRegistered; }

    protected List<ColumnInfo> ccil() {
        List<ColumnInfo> ls = newArrayList();
        ls.add(columnId());
        ls.add(columnCategoryId());
        ls.add(columnName());
        ls.add(columnPrice());
        ls.add(columnType());
        ls.add(columnValidFlg());
        ls.add(columnRegistered());
        return ls;
    }

    { initializeInformationResource(); }

    // ===================================================================================
    //                                                                         Unique Info
    //                                                                         ===========
    // -----------------------------------------------------
    //                                       Primary Element
    //                                       ---------------
    protected UniqueInfo cpui() { return hpcpui(columnId()); }
    public boolean hasPrimaryKey() { return true; }
    public boolean hasCompoundPrimaryKey() { return false; }

    // ===================================================================================
    //                                                                       Relation Info
    //                                                                       =============
    // -----------------------------------------------------
    //                                      Foreign Property
    //                                      ----------------

    // -----------------------------------------------------
    //                                     Referrer Property
    //                                     -----------------

    // ===================================================================================
    //                                                                        Various Info
    //                                                                        ============
    public boolean hasIdentity() { return true; }

    // ===================================================================================
    //                                                                           Type Name
    //                                                                           =========
    public String getEntityTypeName() { return "dltng.dbflute.exentity.Products"; }
    public String getConditionBeanTypeName() { return "dltng.dbflute.cbean.ProductsCB"; }
    public String getBehaviorTypeName() { return "dltng.dbflute.exbhv.ProductsBhv"; }

    // ===================================================================================
    //                                                                         Object Type
    //                                                                         ===========
    public Class<Products> getEntityType() { return Products.class; }

    // ===================================================================================
    //                                                                     Object Instance
    //                                                                     ===============
    public Entity newEntity() { return newMyEntity(); }
    public Products newMyEntity() { return new Products(); }

    // ===================================================================================
    //                                                                   Map Communication
    //                                                                   =================
    public void acceptPrimaryKeyMap(Entity e, Map<String, ? extends Object> m)
    { doAcceptPrimaryKeyMap((Products)e, m); }
    public void acceptAllColumnMap(Entity e, Map<String, ? extends Object> m)
    { doAcceptAllColumnMap((Products)e, m); }
    public Map<String, Object> extractPrimaryKeyMap(Entity e) { return doExtractPrimaryKeyMap(e); }
    public Map<String, Object> extractAllColumnMap(Entity e) { return doExtractAllColumnMap(e); }
}
