package dltng.dbflute.bsentity;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import org.seasar.dbflute.dbmeta.DBMeta;
import org.seasar.dbflute.Entity;
import dltng.dbflute.allcommon.DBMetaInstanceHandler;
import dltng.dbflute.allcommon.CDef;
import dltng.dbflute.exentity.*;

/**
 * The entity of PRODUCTS as TABLE. <br />
 * <pre>
 * [primary-key]
 *     ID
 * 
 * [column]
 *     ID, CATEGORY_ID, NAME, PRICE, TYPE, VALID_FLG, REGISTERED
 * 
 * [sequence]
 *     
 * 
 * [identity]
 *     ID
 * 
 * [version-no]
 *     
 * 
 * [foreign table]
 *     
 * 
 * [referrer table]
 *     
 * 
 * [foreign property]
 *     
 * 
 * [referrer property]
 *     
 * 
 * [get/set template]
 * /= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
 * Integer id = entity.getId();
 * Integer categoryId = entity.getCategoryId();
 * String name = entity.getName();
 * Integer price = entity.getPrice();
 * Integer type = entity.getType();
 * Integer validFlg = entity.getValidFlg();
 * java.sql.Timestamp registered = entity.getRegistered();
 * entity.setId(id);
 * entity.setCategoryId(categoryId);
 * entity.setName(name);
 * entity.setPrice(price);
 * entity.setType(type);
 * entity.setValidFlg(validFlg);
 * entity.setRegistered(registered);
 * = = = = = = = = = =/
 * </pre>
 * @author DBFlute(AutoGenerator)
 */
public abstract class BsProducts implements Entity, Serializable, Cloneable {

    // ===================================================================================
    //                                                                          Definition
    //                                                                          ==========
    /** Serial version UID. (Default) */
    private static final long serialVersionUID = 1L;

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    // -----------------------------------------------------
    //                                                Column
    //                                                ------
    /** ID: {PK, ID, NotNull, INT(10)} */
    protected Integer _id;

    /** CATEGORY_ID: {IX, NotNull, INT(10)} */
    protected Integer _categoryId;

    /** NAME: {NotNull, VARCHAR(100)} */
    protected String _name;

    /** PRICE: {NotNull, INT(10)} */
    protected Integer _price;

    /** TYPE: {NotNull, INT(10)} */
    protected Integer _type;

    /** VALID_FLG: {NotNull, INT(10), classification=Flg} */
    protected Integer _validFlg;

    /** REGISTERED: {NotNull, TIMESTAMP(19), default=[CURRENT_TIMESTAMP]} */
    protected java.sql.Timestamp _registered;

    // -----------------------------------------------------
    //                                              Internal
    //                                              --------
    /** The modified properties for this entity. (NotNull) */
    protected final EntityModifiedProperties __modifiedProperties = newModifiedProperties();

    /** Is the entity created by DBFlute select process? */
    protected boolean __createdBySelect;

    // ===================================================================================
    //                                                                          Table Name
    //                                                                          ==========
    /**
     * {@inheritDoc}
     */
    public String getTableDbName() {
        return "PRODUCTS";
    }

    /**
     * {@inheritDoc}
     */
    public String getTablePropertyName() { // according to Java Beans rule
        return "products";
    }

    // ===================================================================================
    //                                                                              DBMeta
    //                                                                              ======
    /**
     * {@inheritDoc}
     */
    public DBMeta getDBMeta() {
        return DBMetaInstanceHandler.findDBMeta(getTableDbName());
    }

    // ===================================================================================
    //                                                                         Primary Key
    //                                                                         ===========
    /**
     * {@inheritDoc}
     */
    public boolean hasPrimaryKeyValue() {
        if (getId() == null) { return false; }
        return true;
    }

    // ===================================================================================
    //                                                             Classification Property
    //                                                             =======================
    /**
     * Get the value of validFlg as the classification of Flg. <br />
     * VALID_FLG: {NotNull, INT(10), classification=Flg} <br />
     * 有効フラグ
     * <p>It's treated as case insensitive and if the code value is null, it returns null.</p>
     * @return The instance of classification definition (as ENUM type). (NullAllowed: when the column value is null)
     */
    public CDef.Flg getValidFlgAsFlg() {
        return CDef.Flg.codeOf(getValidFlg());
    }

    /**
     * Set the value of validFlg as the classification of Flg. <br />
     * VALID_FLG: {NotNull, INT(10), classification=Flg} <br />
     * 有効フラグ
     * @param cdef The instance of classification definition (as ENUM type). (NullAllowed: if null, null value is set to the column)
     */
    public void setValidFlgAsFlg(CDef.Flg cdef) {
        setValidFlg(cdef != null ? InternalUtil.toNumber(cdef.code(), Integer.class) : null);
    }

    // ===================================================================================
    //                                                              Classification Setting
    //                                                              ======================
    /**
     * Set the value of validFlg as Valid (1). <br />
     * 有効: 有効の状態。リストに表示される
     */
    public void setValidFlg_Valid() {
        setValidFlgAsFlg(CDef.Flg.Valid);
    }

    /**
     * Set the value of validFlg as Invalid (0). <br />
     * 無効: 無効の状態。リストに表示されない
     */
    public void setValidFlg_Invalid() {
        setValidFlgAsFlg(CDef.Flg.Invalid);
    }

    // ===================================================================================
    //                                                        Classification Determination
    //                                                        ============================
    /**
     * Is the value of validFlg Valid? <br />
     * 有効: 有効の状態。リストに表示される
     * <p>It's treated as case insensitive and if the code value is null, it returns false.</p>
     * @return The determination, true or false.
     */
    public boolean isValidFlgValid() {
        CDef.Flg cdef = getValidFlgAsFlg();
        return cdef != null ? cdef.equals(CDef.Flg.Valid) : false;
    }

    /**
     * Is the value of validFlg Invalid? <br />
     * 無効: 無効の状態。リストに表示されない
     * <p>It's treated as case insensitive and if the code value is null, it returns false.</p>
     * @return The determination, true or false.
     */
    public boolean isValidFlgInvalid() {
        CDef.Flg cdef = getValidFlgAsFlg();
        return cdef != null ? cdef.equals(CDef.Flg.Invalid) : false;
    }

    // ===================================================================================
    //                                                           Classification Name/Alias
    //                                                           =========================
    /**
     * Get the value of the column 'validFlg' as classification name.
     * @return The string of classification name. (NullAllowed: when the column value is null)
     */
    public String getValidFlgName() {
        CDef.Flg cdef = getValidFlgAsFlg();
        return cdef != null ? cdef.name() : null;
    }

    /**
     * Get the value of the column 'validFlg' as classification alias.
     * @return The string of classification alias. (NullAllowed: when the column value is null)
     */
    public String getValidFlgAlias() {
        CDef.Flg cdef = getValidFlgAsFlg();
        return cdef != null ? cdef.alias() : null;
    }

    // ===================================================================================
    //                                                                    Foreign Property
    //                                                                    ================
    // ===================================================================================
    //                                                                   Referrer Property
    //                                                                   =================
    protected <ELEMENT> List<ELEMENT> newReferrerList() {
        return new ArrayList<ELEMENT>();
    }

    // ===================================================================================
    //                                                                 Modified Properties
    //                                                                 ===================
    /**
     * {@inheritDoc}
     */
    public Set<String> modifiedProperties() {
        return __modifiedProperties.getPropertyNames();
    }

    /**
     * {@inheritDoc}
     */
    public void clearModifiedInfo() {
        __modifiedProperties.clear();
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasModification() {
        return !__modifiedProperties.isEmpty();
    }

    protected EntityModifiedProperties newModifiedProperties() {
        return new EntityModifiedProperties();
    }

    // ===================================================================================
    //                                                                     Birthplace Mark
    //                                                                     ===============
    /**
     * {@inheritDoc}
     */
    public void markAsSelect() {
        __createdBySelect = true;
    }

    /**
     * {@inheritDoc}
     */
    public boolean createdBySelect() {
        return __createdBySelect;
    }

    // ===================================================================================
    //                                                                      Basic Override
    //                                                                      ==============
    /**
     * Determine the object is equal with this. <br />
     * If primary-keys or columns of the other are same as this one, returns true.
     * @param other The other entity. (NullAllowed: if null, returns false fixedly)
     * @return Comparing result.
     */
    public boolean equals(Object other) {
        if (other == null || !(other instanceof BsProducts)) { return false; }
        BsProducts otherEntity = (BsProducts)other;
        if (!xSV(getId(), otherEntity.getId())) { return false; }
        return true;
    }
    protected boolean xSV(Object value1, Object value2) { // isSameValue()
        return InternalUtil.isSameValue(value1, value2);
    }

    /**
     * Calculate the hash-code from primary-keys or columns.
     * @return The hash-code from primary-key or columns.
     */
    public int hashCode() {
        int result = 17;
        result = xCH(result, getTableDbName());
        result = xCH(result, getId());
        return result;
    }
    protected int xCH(int result, Object value) { // calculateHashcode()
        return InternalUtil.calculateHashcode(result, value);
    }

    /**
     * {@inheritDoc}
     */
    public int instanceHash() {
        return super.hashCode();
    }

    /**
     * Convert to display string of entity's data. (no relation data)
     * @return The display string of all columns and relation existences. (NotNull)
     */
    public String toString() {
        return buildDisplayString(InternalUtil.toClassTitle(this), true, true);
    }

    /**
     * {@inheritDoc}
     */
    public String toStringWithRelation() {
        StringBuilder sb = new StringBuilder();
        sb.append(toString());
        return sb.toString();
    }

    /**
     * {@inheritDoc}
     */
    public String buildDisplayString(String name, boolean column, boolean relation) {
        StringBuilder sb = new StringBuilder();
        if (name != null) { sb.append(name).append(column || relation ? ":" : ""); }
        if (column) { sb.append(buildColumnString()); }
        if (relation) { sb.append(buildRelationString()); }
        sb.append("@").append(Integer.toHexString(hashCode()));
        return sb.toString();
    }
    protected String buildColumnString() {
        StringBuilder sb = new StringBuilder();
        String delimiter = ", ";
        sb.append(delimiter).append(getId());
        sb.append(delimiter).append(getCategoryId());
        sb.append(delimiter).append(getName());
        sb.append(delimiter).append(getPrice());
        sb.append(delimiter).append(getType());
        sb.append(delimiter).append(getValidFlg());
        sb.append(delimiter).append(getRegistered());
        if (sb.length() > delimiter.length()) {
            sb.delete(0, delimiter.length());
        }
        sb.insert(0, "{").append("}");
        return sb.toString();
    }
    protected String buildRelationString() {
        return "";
    }

    /**
     * Clone entity instance using super.clone(). (shallow copy) 
     * @return The cloned instance of this entity. (NotNull)
     */
    public Products clone() {
        try {
            return (Products)super.clone();
        } catch (CloneNotSupportedException e) {
            throw new IllegalStateException("Failed to clone the entity: " + toString(), e);
        }
    }

    // ===================================================================================
    //                                                                            Accessor
    //                                                                            ========
    /**
     * [get] ID: {PK, ID, NotNull, INT(10)} <br />
     * @return The value of the column 'ID'. (basically NotNull if selected: for the constraint)
     */
    public Integer getId() {
        return _id;
    }

    /**
     * [set] ID: {PK, ID, NotNull, INT(10)} <br />
     * @param id The value of the column 'ID'. (basically NotNull if update: for the constraint)
     */
    public void setId(Integer id) {
        __modifiedProperties.addPropertyName("id");
        this._id = id;
    }

    /**
     * [get] CATEGORY_ID: {IX, NotNull, INT(10)} <br />
     * @return The value of the column 'CATEGORY_ID'. (basically NotNull if selected: for the constraint)
     */
    public Integer getCategoryId() {
        return _categoryId;
    }

    /**
     * [set] CATEGORY_ID: {IX, NotNull, INT(10)} <br />
     * @param categoryId The value of the column 'CATEGORY_ID'. (basically NotNull if update: for the constraint)
     */
    public void setCategoryId(Integer categoryId) {
        __modifiedProperties.addPropertyName("categoryId");
        this._categoryId = categoryId;
    }

    /**
     * [get] NAME: {NotNull, VARCHAR(100)} <br />
     * @return The value of the column 'NAME'. (basically NotNull if selected: for the constraint)
     */
    public String getName() {
        return _name;
    }

    /**
     * [set] NAME: {NotNull, VARCHAR(100)} <br />
     * @param name The value of the column 'NAME'. (basically NotNull if update: for the constraint)
     */
    public void setName(String name) {
        __modifiedProperties.addPropertyName("name");
        this._name = name;
    }

    /**
     * [get] PRICE: {NotNull, INT(10)} <br />
     * @return The value of the column 'PRICE'. (basically NotNull if selected: for the constraint)
     */
    public Integer getPrice() {
        return _price;
    }

    /**
     * [set] PRICE: {NotNull, INT(10)} <br />
     * @param price The value of the column 'PRICE'. (basically NotNull if update: for the constraint)
     */
    public void setPrice(Integer price) {
        __modifiedProperties.addPropertyName("price");
        this._price = price;
    }

    /**
     * [get] TYPE: {NotNull, INT(10)} <br />
     * @return The value of the column 'TYPE'. (basically NotNull if selected: for the constraint)
     */
    public Integer getType() {
        return _type;
    }

    /**
     * [set] TYPE: {NotNull, INT(10)} <br />
     * @param type The value of the column 'TYPE'. (basically NotNull if update: for the constraint)
     */
    public void setType(Integer type) {
        __modifiedProperties.addPropertyName("type");
        this._type = type;
    }

    /**
     * [get] VALID_FLG: {NotNull, INT(10), classification=Flg} <br />
     * @return The value of the column 'VALID_FLG'. (basically NotNull if selected: for the constraint)
     */
    public Integer getValidFlg() {
        return _validFlg;
    }

    /**
     * [set] VALID_FLG: {NotNull, INT(10), classification=Flg} <br />
     * @param validFlg The value of the column 'VALID_FLG'. (basically NotNull if update: for the constraint)
     */
    public void setValidFlg(Integer validFlg) {
        __modifiedProperties.addPropertyName("validFlg");
        this._validFlg = validFlg;
    }

    /**
     * [get] REGISTERED: {NotNull, TIMESTAMP(19), default=[CURRENT_TIMESTAMP]} <br />
     * @return The value of the column 'REGISTERED'. (basically NotNull if selected: for the constraint)
     */
    public java.sql.Timestamp getRegistered() {
        return _registered;
    }

    /**
     * [set] REGISTERED: {NotNull, TIMESTAMP(19), default=[CURRENT_TIMESTAMP]} <br />
     * @param registered The value of the column 'REGISTERED'. (basically NotNull if update: for the constraint)
     */
    public void setRegistered(java.sql.Timestamp registered) {
        __modifiedProperties.addPropertyName("registered");
        this._registered = registered;
    }
}
