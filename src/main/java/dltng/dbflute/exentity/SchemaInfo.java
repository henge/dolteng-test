package dltng.dbflute.exentity;

import dltng.dbflute.bsentity.BsSchemaInfo;

/**
 * The entity of SCHEMA_INFO.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class SchemaInfo extends BsSchemaInfo {

    /** Serial version UID. (Default) */
    private static final long serialVersionUID = 1L;
}
