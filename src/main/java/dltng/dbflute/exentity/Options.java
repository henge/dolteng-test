package dltng.dbflute.exentity;

import dltng.dbflute.bsentity.BsOptions;

/**
 * The entity of OPTIONS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class Options extends BsOptions {

    /** Serial version UID. (Default) */
    private static final long serialVersionUID = 1L;
}
