package dltng.dbflute.exentity;

import dltng.dbflute.bsentity.BsProducts;

/**
 * The entity of PRODUCTS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class Products extends BsProducts {

    /** Serial version UID. (Default) */
    private static final long serialVersionUID = 1L;
}
