package dltng.dbflute.exentity;

import dltng.dbflute.bsentity.BsAdditions;

/**
 * The entity of ADDITIONS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class Additions extends BsAdditions {

    /** Serial version UID. (Default) */
    private static final long serialVersionUID = 1L;
}
