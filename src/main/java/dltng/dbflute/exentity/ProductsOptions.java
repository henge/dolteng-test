package dltng.dbflute.exentity;

import dltng.dbflute.bsentity.BsProductsOptions;

/**
 * The entity of PRODUCTS_OPTIONS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class ProductsOptions extends BsProductsOptions {

    /** Serial version UID. (Default) */
    private static final long serialVersionUID = 1L;
}
