package dltng.dbflute.exentity;

import dltng.dbflute.bsentity.BsCategories;

/**
 * The entity of CATEGORIES.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class Categories extends BsCategories {

    /** Serial version UID. (Default) */
    private static final long serialVersionUID = 1L;
}
