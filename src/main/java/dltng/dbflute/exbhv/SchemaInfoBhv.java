package dltng.dbflute.exbhv;

import dltng.dbflute.bsbhv.BsSchemaInfoBhv;

/**
 * The behavior of SCHEMA_INFO.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class SchemaInfoBhv extends BsSchemaInfoBhv {
}
