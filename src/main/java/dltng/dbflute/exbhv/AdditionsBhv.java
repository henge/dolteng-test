package dltng.dbflute.exbhv;

import dltng.dbflute.bsbhv.BsAdditionsBhv;

/**
 * The behavior of ADDITIONS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class AdditionsBhv extends BsAdditionsBhv {
}
