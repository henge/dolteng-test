package dltng.dbflute.exbhv;

import dltng.dbflute.bsbhv.BsProductsOptionsBhv;

/**
 * The behavior of PRODUCTS_OPTIONS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class ProductsOptionsBhv extends BsProductsOptionsBhv {
}
