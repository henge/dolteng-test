package dltng.dbflute.exbhv;

import dltng.dbflute.bsbhv.BsOptionsBhv;

/**
 * The behavior of OPTIONS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class OptionsBhv extends BsOptionsBhv {
}
