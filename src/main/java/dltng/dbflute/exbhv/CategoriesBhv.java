package dltng.dbflute.exbhv;

import dltng.dbflute.bsbhv.BsCategoriesBhv;

/**
 * The behavior of CATEGORIES.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class CategoriesBhv extends BsCategoriesBhv {
}
