package dltng.dbflute.exbhv;

import java.util.List;

import dltng.dbflute.bsbhv.BsProductsBhv;
import dltng.dbflute.cbean.ProductsCB;
import dltng.dbflute.exentity.Products;

/**
 * The behavior of PRODUCTS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class ProductsBhv extends BsProductsBhv {

	public List<Products> findAllOrderById() {
		ProductsCB cb = new ProductsCB();
		cb.query().setValidFlg_Equal_Valid();
		cb.query().addOrderBy_Price_Asc();

		return selectList(cb);
	}
}
