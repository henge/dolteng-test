package dltng.dbflute.cbean;

import dltng.dbflute.cbean.bs.BsSchemaInfoCB;

/**
 * The condition-bean of SCHEMA_INFO.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class SchemaInfoCB extends BsSchemaInfoCB {
}
