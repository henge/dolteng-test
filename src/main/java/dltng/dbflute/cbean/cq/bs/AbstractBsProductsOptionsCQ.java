package dltng.dbflute.cbean.cq.bs;

import java.util.*;

import org.seasar.dbflute.cbean.*;
import org.seasar.dbflute.cbean.chelper.*;
import org.seasar.dbflute.cbean.ckey.*;
import org.seasar.dbflute.cbean.coption.*;
import org.seasar.dbflute.cbean.cvalue.ConditionValue;
import org.seasar.dbflute.cbean.sqlclause.SqlClause;
import org.seasar.dbflute.dbmeta.DBMetaProvider;
import dltng.dbflute.allcommon.*;
import dltng.dbflute.cbean.*;
import dltng.dbflute.cbean.cq.*;

/**
 * The abstract condition-query of PRODUCTS_OPTIONS.
 * @author DBFlute(AutoGenerator)
 */
public abstract class AbstractBsProductsOptionsCQ extends AbstractConditionQuery {

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    public AbstractBsProductsOptionsCQ(ConditionQuery childQuery, SqlClause sqlClause, String aliasName, int nestLevel) {
        super(childQuery, sqlClause, aliasName, nestLevel);
    }

    // ===================================================================================
    //                                                                     DBMeta Provider
    //                                                                     ===============
    @Override
    protected DBMetaProvider xgetDBMetaProvider() {
        return DBMetaInstanceHandler.getProvider();
    }

    // ===================================================================================
    //                                                                          Table Name
    //                                                                          ==========
    public String getTableDbName() {
        return "PRODUCTS_OPTIONS";
    }

    // ===================================================================================
    //                                                                               Query
    //                                                                               =====
    
    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br />
     * PRODUCT_ID: {PK, NotNull, INT(10)}
     * @param productId The value of productId as equal. (NullAllowed: if null, no condition)
     */
    public void setProductId_Equal(Integer productId) {
        doSetProductId_Equal(productId);
    }

    protected void doSetProductId_Equal(Integer productId) {
        regProductId(CK_EQ, productId);
    }

    /**
     * NotEqual(&lt;&gt;). And NullIgnored, OnlyOnceRegistered. <br />
     * PRODUCT_ID: {PK, NotNull, INT(10)}
     * @param productId The value of productId as notEqual. (NullAllowed: if null, no condition)
     */
    public void setProductId_NotEqual(Integer productId) {
        doSetProductId_NotEqual(productId);
    }

    protected void doSetProductId_NotEqual(Integer productId) {
        regProductId(CK_NES, productId);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br />
     * PRODUCT_ID: {PK, NotNull, INT(10)}
     * @param productId The value of productId as greaterThan. (NullAllowed: if null, no condition)
     */
    public void setProductId_GreaterThan(Integer productId) {
        regProductId(CK_GT, productId);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br />
     * PRODUCT_ID: {PK, NotNull, INT(10)}
     * @param productId The value of productId as lessThan. (NullAllowed: if null, no condition)
     */
    public void setProductId_LessThan(Integer productId) {
        regProductId(CK_LT, productId);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br />
     * PRODUCT_ID: {PK, NotNull, INT(10)}
     * @param productId The value of productId as greaterEqual. (NullAllowed: if null, no condition)
     */
    public void setProductId_GreaterEqual(Integer productId) {
        regProductId(CK_GE, productId);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br />
     * PRODUCT_ID: {PK, NotNull, INT(10)}
     * @param productId The value of productId as lessEqual. (NullAllowed: if null, no condition)
     */
    public void setProductId_LessEqual(Integer productId) {
        regProductId(CK_LE, productId);
    }

    /**
     * RangeOf with various options. (versatile) <br />
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br />
     * And NullIgnored, OnlyOnceRegistered. <br />
     * PRODUCT_ID: {PK, NotNull, INT(10)}
     * @param minNumber The min number of productId. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of productId. (NullAllowed: if null, no to-condition)
     * @param rangeOfOption The option of range-of. (NotNull)
     */
    public void setProductId_RangeOf(Integer minNumber, Integer maxNumber, RangeOfOption rangeOfOption) {
        regROO(minNumber, maxNumber, getCValueProductId(), "PRODUCT_ID", rangeOfOption);
    }

    /**
     * InScope {in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br />
     * PRODUCT_ID: {PK, NotNull, INT(10)}
     * @param productIdList The collection of productId as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setProductId_InScope(Collection<Integer> productIdList) {
        doSetProductId_InScope(productIdList);
    }

    protected void doSetProductId_InScope(Collection<Integer> productIdList) {
        regINS(CK_INS, cTL(productIdList), getCValueProductId(), "PRODUCT_ID");
    }

    /**
     * NotInScope {not in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br />
     * PRODUCT_ID: {PK, NotNull, INT(10)}
     * @param productIdList The collection of productId as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setProductId_NotInScope(Collection<Integer> productIdList) {
        doSetProductId_NotInScope(productIdList);
    }

    protected void doSetProductId_NotInScope(Collection<Integer> productIdList) {
        regINS(CK_NINS, cTL(productIdList), getCValueProductId(), "PRODUCT_ID");
    }

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br />
     * PRODUCT_ID: {PK, NotNull, INT(10)}
     */
    public void setProductId_IsNull() { regProductId(CK_ISN, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br />
     * PRODUCT_ID: {PK, NotNull, INT(10)}
     */
    public void setProductId_IsNotNull() { regProductId(CK_ISNN, DOBJ); }

    protected void regProductId(ConditionKey k, Object v) { regQ(k, v, getCValueProductId(), "PRODUCT_ID"); }
    abstract protected ConditionValue getCValueProductId();
    
    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br />
     * OPTION_ID: {PK, NotNull, INT(10)}
     * @param optionId The value of optionId as equal. (NullAllowed: if null, no condition)
     */
    public void setOptionId_Equal(Integer optionId) {
        doSetOptionId_Equal(optionId);
    }

    protected void doSetOptionId_Equal(Integer optionId) {
        regOptionId(CK_EQ, optionId);
    }

    /**
     * NotEqual(&lt;&gt;). And NullIgnored, OnlyOnceRegistered. <br />
     * OPTION_ID: {PK, NotNull, INT(10)}
     * @param optionId The value of optionId as notEqual. (NullAllowed: if null, no condition)
     */
    public void setOptionId_NotEqual(Integer optionId) {
        doSetOptionId_NotEqual(optionId);
    }

    protected void doSetOptionId_NotEqual(Integer optionId) {
        regOptionId(CK_NES, optionId);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br />
     * OPTION_ID: {PK, NotNull, INT(10)}
     * @param optionId The value of optionId as greaterThan. (NullAllowed: if null, no condition)
     */
    public void setOptionId_GreaterThan(Integer optionId) {
        regOptionId(CK_GT, optionId);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br />
     * OPTION_ID: {PK, NotNull, INT(10)}
     * @param optionId The value of optionId as lessThan. (NullAllowed: if null, no condition)
     */
    public void setOptionId_LessThan(Integer optionId) {
        regOptionId(CK_LT, optionId);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br />
     * OPTION_ID: {PK, NotNull, INT(10)}
     * @param optionId The value of optionId as greaterEqual. (NullAllowed: if null, no condition)
     */
    public void setOptionId_GreaterEqual(Integer optionId) {
        regOptionId(CK_GE, optionId);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br />
     * OPTION_ID: {PK, NotNull, INT(10)}
     * @param optionId The value of optionId as lessEqual. (NullAllowed: if null, no condition)
     */
    public void setOptionId_LessEqual(Integer optionId) {
        regOptionId(CK_LE, optionId);
    }

    /**
     * RangeOf with various options. (versatile) <br />
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br />
     * And NullIgnored, OnlyOnceRegistered. <br />
     * OPTION_ID: {PK, NotNull, INT(10)}
     * @param minNumber The min number of optionId. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of optionId. (NullAllowed: if null, no to-condition)
     * @param rangeOfOption The option of range-of. (NotNull)
     */
    public void setOptionId_RangeOf(Integer minNumber, Integer maxNumber, RangeOfOption rangeOfOption) {
        regROO(minNumber, maxNumber, getCValueOptionId(), "OPTION_ID", rangeOfOption);
    }

    /**
     * InScope {in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br />
     * OPTION_ID: {PK, NotNull, INT(10)}
     * @param optionIdList The collection of optionId as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setOptionId_InScope(Collection<Integer> optionIdList) {
        doSetOptionId_InScope(optionIdList);
    }

    protected void doSetOptionId_InScope(Collection<Integer> optionIdList) {
        regINS(CK_INS, cTL(optionIdList), getCValueOptionId(), "OPTION_ID");
    }

    /**
     * NotInScope {not in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br />
     * OPTION_ID: {PK, NotNull, INT(10)}
     * @param optionIdList The collection of optionId as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setOptionId_NotInScope(Collection<Integer> optionIdList) {
        doSetOptionId_NotInScope(optionIdList);
    }

    protected void doSetOptionId_NotInScope(Collection<Integer> optionIdList) {
        regINS(CK_NINS, cTL(optionIdList), getCValueOptionId(), "OPTION_ID");
    }

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br />
     * OPTION_ID: {PK, NotNull, INT(10)}
     */
    public void setOptionId_IsNull() { regOptionId(CK_ISN, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br />
     * OPTION_ID: {PK, NotNull, INT(10)}
     */
    public void setOptionId_IsNotNull() { regOptionId(CK_ISNN, DOBJ); }

    protected void regOptionId(ConditionKey k, Object v) { regQ(k, v, getCValueOptionId(), "OPTION_ID"); }
    abstract protected ConditionValue getCValueOptionId();

    // ===================================================================================
    //                                                                       Very Internal
    //                                                                       =============
    // very internal (for suppressing warn about 'Not Use Import')
    protected String xabCB() { return ProductsOptionsCB.class.getName(); }
    protected String xabCQ() { return ProductsOptionsCQ.class.getName(); }
    protected String xabLSO() { return LikeSearchOption.class.getName(); }
    protected String xabSSQS() { return HpSSQSetupper.class.getName(); }
}
