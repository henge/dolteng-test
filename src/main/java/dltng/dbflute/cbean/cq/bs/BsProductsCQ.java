package dltng.dbflute.cbean.cq.bs;

import java.util.Map;

import org.seasar.dbflute.cbean.*;
import org.seasar.dbflute.cbean.cvalue.ConditionValue;
import org.seasar.dbflute.cbean.sqlclause.SqlClause;
import org.seasar.dbflute.exception.IllegalConditionBeanOperationException;
import dltng.dbflute.cbean.cq.ciq.*;
import dltng.dbflute.cbean.*;
import dltng.dbflute.cbean.cq.*;

/**
 * The base condition-query of PRODUCTS.
 * @author DBFlute(AutoGenerator)
 */
public class BsProductsCQ extends AbstractBsProductsCQ {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected ProductsCIQ _inlineQuery;

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    public BsProductsCQ(ConditionQuery childQuery, SqlClause sqlClause, String aliasName, int nestLevel) {
        super(childQuery, sqlClause, aliasName, nestLevel);
    }

    // ===================================================================================
    //                                                                 InlineView/OrClause
    //                                                                 ===================
    /**
     * Prepare InlineView query. <br />
     * {select ... from ... left outer join (select * from PRODUCTS) where FOO = [value] ...}
     * <pre>
     * cb.query().queryMemberStatus().<span style="color: #FD4747">inline()</span>.setFoo...;
     * </pre>
     * @return The condition-query for InlineView query. (NotNull)
     */
    public ProductsCIQ inline() {
        if (_inlineQuery == null) { _inlineQuery = xcreateCIQ(); }
        _inlineQuery.xsetOnClause(false); return _inlineQuery;
    }

    protected ProductsCIQ xcreateCIQ() {
        ProductsCIQ ciq = xnewCIQ();
        ciq.xsetBaseCB(_baseCB);
        return ciq;
    }

    protected ProductsCIQ xnewCIQ() {
        return new ProductsCIQ(xgetReferrerQuery(), xgetSqlClause(), xgetAliasName(), xgetNestLevel(), this);
    }

    /**
     * Prepare OnClause query. <br />
     * {select ... from ... left outer join PRODUCTS on ... and FOO = [value] ...}
     * <pre>
     * cb.query().queryMemberStatus().<span style="color: #FD4747">on()</span>.setFoo...;
     * </pre>
     * @return The condition-query for OnClause query. (NotNull)
     * @throws IllegalConditionBeanOperationException When this condition-query is base query.
     */
    public ProductsCIQ on() {
        if (isBaseQuery()) { throw new IllegalConditionBeanOperationException("OnClause for local table is unavailable!"); }
        ProductsCIQ inlineQuery = inline(); inlineQuery.xsetOnClause(true); return inlineQuery;
    }

    // ===================================================================================
    //                                                                               Query
    //                                                                               =====

    protected ConditionValue _id;
    public ConditionValue getId() {
        if (_id == null) { _id = nCV(); }
        return _id;
    }
    protected ConditionValue getCValueId() { return getId(); }

    /** 
     * Add order-by as ascend. <br />
     * ID: {PK, ID, NotNull, INT(10)}
     * @return this. (NotNull)
     */
    public BsProductsCQ addOrderBy_Id_Asc() { regOBA("ID"); return this; }

    /**
     * Add order-by as descend. <br />
     * ID: {PK, ID, NotNull, INT(10)}
     * @return this. (NotNull)
     */
    public BsProductsCQ addOrderBy_Id_Desc() { regOBD("ID"); return this; }

    protected ConditionValue _categoryId;
    public ConditionValue getCategoryId() {
        if (_categoryId == null) { _categoryId = nCV(); }
        return _categoryId;
    }
    protected ConditionValue getCValueCategoryId() { return getCategoryId(); }

    /** 
     * Add order-by as ascend. <br />
     * CATEGORY_ID: {IX, NotNull, INT(10)}
     * @return this. (NotNull)
     */
    public BsProductsCQ addOrderBy_CategoryId_Asc() { regOBA("CATEGORY_ID"); return this; }

    /**
     * Add order-by as descend. <br />
     * CATEGORY_ID: {IX, NotNull, INT(10)}
     * @return this. (NotNull)
     */
    public BsProductsCQ addOrderBy_CategoryId_Desc() { regOBD("CATEGORY_ID"); return this; }

    protected ConditionValue _name;
    public ConditionValue getName() {
        if (_name == null) { _name = nCV(); }
        return _name;
    }
    protected ConditionValue getCValueName() { return getName(); }

    /** 
     * Add order-by as ascend. <br />
     * NAME: {NotNull, VARCHAR(100)}
     * @return this. (NotNull)
     */
    public BsProductsCQ addOrderBy_Name_Asc() { regOBA("NAME"); return this; }

    /**
     * Add order-by as descend. <br />
     * NAME: {NotNull, VARCHAR(100)}
     * @return this. (NotNull)
     */
    public BsProductsCQ addOrderBy_Name_Desc() { regOBD("NAME"); return this; }

    protected ConditionValue _price;
    public ConditionValue getPrice() {
        if (_price == null) { _price = nCV(); }
        return _price;
    }
    protected ConditionValue getCValuePrice() { return getPrice(); }

    /** 
     * Add order-by as ascend. <br />
     * PRICE: {NotNull, INT(10)}
     * @return this. (NotNull)
     */
    public BsProductsCQ addOrderBy_Price_Asc() { regOBA("PRICE"); return this; }

    /**
     * Add order-by as descend. <br />
     * PRICE: {NotNull, INT(10)}
     * @return this. (NotNull)
     */
    public BsProductsCQ addOrderBy_Price_Desc() { regOBD("PRICE"); return this; }

    protected ConditionValue _type;
    public ConditionValue getType() {
        if (_type == null) { _type = nCV(); }
        return _type;
    }
    protected ConditionValue getCValueType() { return getType(); }

    /** 
     * Add order-by as ascend. <br />
     * TYPE: {NotNull, INT(10)}
     * @return this. (NotNull)
     */
    public BsProductsCQ addOrderBy_Type_Asc() { regOBA("TYPE"); return this; }

    /**
     * Add order-by as descend. <br />
     * TYPE: {NotNull, INT(10)}
     * @return this. (NotNull)
     */
    public BsProductsCQ addOrderBy_Type_Desc() { regOBD("TYPE"); return this; }

    protected ConditionValue _validFlg;
    public ConditionValue getValidFlg() {
        if (_validFlg == null) { _validFlg = nCV(); }
        return _validFlg;
    }
    protected ConditionValue getCValueValidFlg() { return getValidFlg(); }

    /** 
     * Add order-by as ascend. <br />
     * VALID_FLG: {NotNull, INT(10), classification=Flg}
     * @return this. (NotNull)
     */
    public BsProductsCQ addOrderBy_ValidFlg_Asc() { regOBA("VALID_FLG"); return this; }

    /**
     * Add order-by as descend. <br />
     * VALID_FLG: {NotNull, INT(10), classification=Flg}
     * @return this. (NotNull)
     */
    public BsProductsCQ addOrderBy_ValidFlg_Desc() { regOBD("VALID_FLG"); return this; }

    protected ConditionValue _registered;
    public ConditionValue getRegistered() {
        if (_registered == null) { _registered = nCV(); }
        return _registered;
    }
    protected ConditionValue getCValueRegistered() { return getRegistered(); }

    /** 
     * Add order-by as ascend. <br />
     * REGISTERED: {NotNull, TIMESTAMP(19), default=[CURRENT_TIMESTAMP]}
     * @return this. (NotNull)
     */
    public BsProductsCQ addOrderBy_Registered_Asc() { regOBA("REGISTERED"); return this; }

    /**
     * Add order-by as descend. <br />
     * REGISTERED: {NotNull, TIMESTAMP(19), default=[CURRENT_TIMESTAMP]}
     * @return this. (NotNull)
     */
    public BsProductsCQ addOrderBy_Registered_Desc() { regOBD("REGISTERED"); return this; }

    // ===================================================================================
    //                                                             SpecifiedDerivedOrderBy
    //                                                             =======================
    /**
     * Add order-by for specified derived column as ascend.
     * <pre>
     * cb.specify().derivedPurchaseList().max(new SubQuery&lt;PurchaseCB&gt;() {
     *     public void query(PurchaseCB subCB) {
     *         subCB.specify().columnPurchaseDatetime();
     *     }
     * }, <span style="color: #FD4747">aliasName</span>);
     * <span style="color: #3F7E5E">// order by [alias-name] asc</span>
     * cb.<span style="color: #FD4747">addSpecifiedDerivedOrderBy_Asc</span>(<span style="color: #FD4747">aliasName</span>);
     * </pre>
     * @param aliasName The alias name specified at (Specify)DerivedReferrer. (NotNull)
     * @return this. (NotNull)
     */
    public BsProductsCQ addSpecifiedDerivedOrderBy_Asc(String aliasName)
    { registerSpecifiedDerivedOrderBy_Asc(aliasName); return this; }

    /**
     * Add order-by for specified derived column as descend.
     * <pre>
     * cb.specify().derivedPurchaseList().max(new SubQuery&lt;PurchaseCB&gt;() {
     *     public void query(PurchaseCB subCB) {
     *         subCB.specify().columnPurchaseDatetime();
     *     }
     * }, <span style="color: #FD4747">aliasName</span>);
     * <span style="color: #3F7E5E">// order by [alias-name] desc</span>
     * cb.<span style="color: #FD4747">addSpecifiedDerivedOrderBy_Desc</span>(<span style="color: #FD4747">aliasName</span>);
     * </pre>
     * @param aliasName The alias name specified at (Specify)DerivedReferrer. (NotNull)
     * @return this. (NotNull)
     */
    public BsProductsCQ addSpecifiedDerivedOrderBy_Desc(String aliasName)
    { registerSpecifiedDerivedOrderBy_Desc(aliasName); return this; }

    // ===================================================================================
    //                                                                         Union Query
    //                                                                         ===========
    protected void reflectRelationOnUnionQuery(ConditionQuery baseQueryAsSuper, ConditionQuery unionQueryAsSuper) {
    }

    // ===================================================================================
    //                                                                       Foreign Query
    //                                                                       =============
    protected Map<String, Object> xfindFixedConditionDynamicParameterMap(String property) {
        return null;
    }

    // ===================================================================================
    //                                                                     ScalarCondition
    //                                                                     ===============
    protected Map<String, ProductsCQ> _scalarConditionMap;
    public Map<String, ProductsCQ> getScalarCondition() { return _scalarConditionMap; }
    public String keepScalarCondition(ProductsCQ subQuery) {
        if (_scalarConditionMap == null) { _scalarConditionMap = newLinkedHashMapSized(4); }
        String key = "subQueryMapKey" + (_scalarConditionMap.size() + 1);
        _scalarConditionMap.put(key, subQuery); return "scalarCondition." + key;
    }

    // ===================================================================================
    //                                                                       MyselfDerived
    //                                                                       =============
    protected Map<String, ProductsCQ> _specifyMyselfDerivedMap;
    public Map<String, ProductsCQ> getSpecifyMyselfDerived() { return _specifyMyselfDerivedMap; }
    public String keepSpecifyMyselfDerived(ProductsCQ subQuery) {
        if (_specifyMyselfDerivedMap == null) { _specifyMyselfDerivedMap = newLinkedHashMapSized(4); }
        String key = "subQueryMapKey" + (_specifyMyselfDerivedMap.size() + 1);
        _specifyMyselfDerivedMap.put(key, subQuery); return "specifyMyselfDerived." + key;
    }

    protected Map<String, ProductsCQ> _queryMyselfDerivedMap;
    public Map<String, ProductsCQ> getQueryMyselfDerived() { return _queryMyselfDerivedMap; }
    public String keepQueryMyselfDerived(ProductsCQ subQuery) {
        if (_queryMyselfDerivedMap == null) { _queryMyselfDerivedMap = newLinkedHashMapSized(4); }
        String key = "subQueryMapKey" + (_queryMyselfDerivedMap.size() + 1);
        _queryMyselfDerivedMap.put(key, subQuery); return "queryMyselfDerived." + key;
    }
    protected Map<String, Object> _qyeryMyselfDerivedParameterMap;
    public Map<String, Object> getQueryMyselfDerivedParameter() { return _qyeryMyselfDerivedParameterMap; }
    public String keepQueryMyselfDerivedParameter(Object parameterValue) {
        if (_qyeryMyselfDerivedParameterMap == null) { _qyeryMyselfDerivedParameterMap = newLinkedHashMapSized(4); }
        String key = "subQueryParameterKey" + (_qyeryMyselfDerivedParameterMap.size() + 1);
        _qyeryMyselfDerivedParameterMap.put(key, parameterValue); return "queryMyselfDerivedParameter." + key;
    }

    // ===================================================================================
    //                                                                        MyselfExists
    //                                                                        ============
    protected Map<String, ProductsCQ> _myselfExistsMap;
    public Map<String, ProductsCQ> getMyselfExists() { return _myselfExistsMap; }
    public String keepMyselfExists(ProductsCQ subQuery) {
        if (_myselfExistsMap == null) { _myselfExistsMap = newLinkedHashMapSized(4); }
        String key = "subQueryMapKey" + (_myselfExistsMap.size() + 1);
        _myselfExistsMap.put(key, subQuery); return "myselfExists." + key;
    }

    // ===================================================================================
    //                                                                       MyselfInScope
    //                                                                       =============
    protected Map<String, ProductsCQ> _myselfInScopeMap;
    public Map<String, ProductsCQ> getMyselfInScope() { return _myselfInScopeMap; }
    public String keepMyselfInScope(ProductsCQ subQuery) {
        if (_myselfInScopeMap == null) { _myselfInScopeMap = newLinkedHashMapSized(4); }
        String key = "subQueryMapKey" + (_myselfInScopeMap.size() + 1);
        _myselfInScopeMap.put(key, subQuery); return "myselfInScope." + key;
    }

    // ===================================================================================
    //                                                                       Very Internal
    //                                                                       =============
    // very internal (for suppressing warn about 'Not Use Import')
    protected String xCB() { return ProductsCB.class.getName(); }
    protected String xCQ() { return ProductsCQ.class.getName(); }
    protected String xMap() { return Map.class.getName(); }
}
