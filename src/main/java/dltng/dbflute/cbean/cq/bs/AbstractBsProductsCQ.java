package dltng.dbflute.cbean.cq.bs;

import java.util.*;

import org.seasar.dbflute.cbean.*;
import org.seasar.dbflute.cbean.chelper.*;
import org.seasar.dbflute.cbean.ckey.*;
import org.seasar.dbflute.cbean.coption.*;
import org.seasar.dbflute.cbean.cvalue.ConditionValue;
import org.seasar.dbflute.cbean.sqlclause.SqlClause;
import org.seasar.dbflute.dbmeta.DBMetaProvider;
import dltng.dbflute.allcommon.*;
import dltng.dbflute.cbean.*;
import dltng.dbflute.cbean.cq.*;

/**
 * The abstract condition-query of PRODUCTS.
 * @author DBFlute(AutoGenerator)
 */
public abstract class AbstractBsProductsCQ extends AbstractConditionQuery {

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    public AbstractBsProductsCQ(ConditionQuery childQuery, SqlClause sqlClause, String aliasName, int nestLevel) {
        super(childQuery, sqlClause, aliasName, nestLevel);
    }

    // ===================================================================================
    //                                                                     DBMeta Provider
    //                                                                     ===============
    @Override
    protected DBMetaProvider xgetDBMetaProvider() {
        return DBMetaInstanceHandler.getProvider();
    }

    // ===================================================================================
    //                                                                          Table Name
    //                                                                          ==========
    public String getTableDbName() {
        return "PRODUCTS";
    }

    // ===================================================================================
    //                                                                               Query
    //                                                                               =====
    
    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br />
     * ID: {PK, ID, NotNull, INT(10)}
     * @param id The value of id as equal. (NullAllowed: if null, no condition)
     */
    public void setId_Equal(Integer id) {
        doSetId_Equal(id);
    }

    protected void doSetId_Equal(Integer id) {
        regId(CK_EQ, id);
    }

    /**
     * NotEqual(&lt;&gt;). And NullIgnored, OnlyOnceRegistered. <br />
     * ID: {PK, ID, NotNull, INT(10)}
     * @param id The value of id as notEqual. (NullAllowed: if null, no condition)
     */
    public void setId_NotEqual(Integer id) {
        doSetId_NotEqual(id);
    }

    protected void doSetId_NotEqual(Integer id) {
        regId(CK_NES, id);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br />
     * ID: {PK, ID, NotNull, INT(10)}
     * @param id The value of id as greaterThan. (NullAllowed: if null, no condition)
     */
    public void setId_GreaterThan(Integer id) {
        regId(CK_GT, id);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br />
     * ID: {PK, ID, NotNull, INT(10)}
     * @param id The value of id as lessThan. (NullAllowed: if null, no condition)
     */
    public void setId_LessThan(Integer id) {
        regId(CK_LT, id);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br />
     * ID: {PK, ID, NotNull, INT(10)}
     * @param id The value of id as greaterEqual. (NullAllowed: if null, no condition)
     */
    public void setId_GreaterEqual(Integer id) {
        regId(CK_GE, id);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br />
     * ID: {PK, ID, NotNull, INT(10)}
     * @param id The value of id as lessEqual. (NullAllowed: if null, no condition)
     */
    public void setId_LessEqual(Integer id) {
        regId(CK_LE, id);
    }

    /**
     * RangeOf with various options. (versatile) <br />
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br />
     * And NullIgnored, OnlyOnceRegistered. <br />
     * ID: {PK, ID, NotNull, INT(10)}
     * @param minNumber The min number of id. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of id. (NullAllowed: if null, no to-condition)
     * @param rangeOfOption The option of range-of. (NotNull)
     */
    public void setId_RangeOf(Integer minNumber, Integer maxNumber, RangeOfOption rangeOfOption) {
        regROO(minNumber, maxNumber, getCValueId(), "ID", rangeOfOption);
    }

    /**
     * InScope {in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br />
     * ID: {PK, ID, NotNull, INT(10)}
     * @param idList The collection of id as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setId_InScope(Collection<Integer> idList) {
        doSetId_InScope(idList);
    }

    protected void doSetId_InScope(Collection<Integer> idList) {
        regINS(CK_INS, cTL(idList), getCValueId(), "ID");
    }

    /**
     * NotInScope {not in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br />
     * ID: {PK, ID, NotNull, INT(10)}
     * @param idList The collection of id as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setId_NotInScope(Collection<Integer> idList) {
        doSetId_NotInScope(idList);
    }

    protected void doSetId_NotInScope(Collection<Integer> idList) {
        regINS(CK_NINS, cTL(idList), getCValueId(), "ID");
    }

    /**
     * IsNull {is null}. And OnlyOnceRegistered. <br />
     * ID: {PK, ID, NotNull, INT(10)}
     */
    public void setId_IsNull() { regId(CK_ISN, DOBJ); }

    /**
     * IsNotNull {is not null}. And OnlyOnceRegistered. <br />
     * ID: {PK, ID, NotNull, INT(10)}
     */
    public void setId_IsNotNull() { regId(CK_ISNN, DOBJ); }

    protected void regId(ConditionKey k, Object v) { regQ(k, v, getCValueId(), "ID"); }
    abstract protected ConditionValue getCValueId();
    
    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br />
     * CATEGORY_ID: {IX, NotNull, INT(10)}
     * @param categoryId The value of categoryId as equal. (NullAllowed: if null, no condition)
     */
    public void setCategoryId_Equal(Integer categoryId) {
        doSetCategoryId_Equal(categoryId);
    }

    protected void doSetCategoryId_Equal(Integer categoryId) {
        regCategoryId(CK_EQ, categoryId);
    }

    /**
     * NotEqual(&lt;&gt;). And NullIgnored, OnlyOnceRegistered. <br />
     * CATEGORY_ID: {IX, NotNull, INT(10)}
     * @param categoryId The value of categoryId as notEqual. (NullAllowed: if null, no condition)
     */
    public void setCategoryId_NotEqual(Integer categoryId) {
        doSetCategoryId_NotEqual(categoryId);
    }

    protected void doSetCategoryId_NotEqual(Integer categoryId) {
        regCategoryId(CK_NES, categoryId);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br />
     * CATEGORY_ID: {IX, NotNull, INT(10)}
     * @param categoryId The value of categoryId as greaterThan. (NullAllowed: if null, no condition)
     */
    public void setCategoryId_GreaterThan(Integer categoryId) {
        regCategoryId(CK_GT, categoryId);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br />
     * CATEGORY_ID: {IX, NotNull, INT(10)}
     * @param categoryId The value of categoryId as lessThan. (NullAllowed: if null, no condition)
     */
    public void setCategoryId_LessThan(Integer categoryId) {
        regCategoryId(CK_LT, categoryId);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br />
     * CATEGORY_ID: {IX, NotNull, INT(10)}
     * @param categoryId The value of categoryId as greaterEqual. (NullAllowed: if null, no condition)
     */
    public void setCategoryId_GreaterEqual(Integer categoryId) {
        regCategoryId(CK_GE, categoryId);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br />
     * CATEGORY_ID: {IX, NotNull, INT(10)}
     * @param categoryId The value of categoryId as lessEqual. (NullAllowed: if null, no condition)
     */
    public void setCategoryId_LessEqual(Integer categoryId) {
        regCategoryId(CK_LE, categoryId);
    }

    /**
     * RangeOf with various options. (versatile) <br />
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br />
     * And NullIgnored, OnlyOnceRegistered. <br />
     * CATEGORY_ID: {IX, NotNull, INT(10)}
     * @param minNumber The min number of categoryId. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of categoryId. (NullAllowed: if null, no to-condition)
     * @param rangeOfOption The option of range-of. (NotNull)
     */
    public void setCategoryId_RangeOf(Integer minNumber, Integer maxNumber, RangeOfOption rangeOfOption) {
        regROO(minNumber, maxNumber, getCValueCategoryId(), "CATEGORY_ID", rangeOfOption);
    }

    /**
     * InScope {in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br />
     * CATEGORY_ID: {IX, NotNull, INT(10)}
     * @param categoryIdList The collection of categoryId as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setCategoryId_InScope(Collection<Integer> categoryIdList) {
        doSetCategoryId_InScope(categoryIdList);
    }

    protected void doSetCategoryId_InScope(Collection<Integer> categoryIdList) {
        regINS(CK_INS, cTL(categoryIdList), getCValueCategoryId(), "CATEGORY_ID");
    }

    /**
     * NotInScope {not in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br />
     * CATEGORY_ID: {IX, NotNull, INT(10)}
     * @param categoryIdList The collection of categoryId as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setCategoryId_NotInScope(Collection<Integer> categoryIdList) {
        doSetCategoryId_NotInScope(categoryIdList);
    }

    protected void doSetCategoryId_NotInScope(Collection<Integer> categoryIdList) {
        regINS(CK_NINS, cTL(categoryIdList), getCValueCategoryId(), "CATEGORY_ID");
    }

    protected void regCategoryId(ConditionKey k, Object v) { regQ(k, v, getCValueCategoryId(), "CATEGORY_ID"); }
    abstract protected ConditionValue getCValueCategoryId();

    /**
     * Equal(=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br />
     * NAME: {NotNull, VARCHAR(100)}
     * @param name The value of name as equal. (NullAllowed: if null (or empty), no condition)
     */
    public void setName_Equal(String name) {
        doSetName_Equal(fRES(name));
    }

    protected void doSetName_Equal(String name) {
        regName(CK_EQ, name);
    }

    /**
     * NotEqual(&lt;&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br />
     * NAME: {NotNull, VARCHAR(100)}
     * @param name The value of name as notEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setName_NotEqual(String name) {
        doSetName_NotEqual(fRES(name));
    }

    protected void doSetName_NotEqual(String name) {
        regName(CK_NES, name);
    }

    /**
     * GreaterThan(&gt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br />
     * NAME: {NotNull, VARCHAR(100)}
     * @param name The value of name as greaterThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setName_GreaterThan(String name) {
        regName(CK_GT, fRES(name));
    }

    /**
     * LessThan(&lt;). And NullOrEmptyIgnored, OnlyOnceRegistered. <br />
     * NAME: {NotNull, VARCHAR(100)}
     * @param name The value of name as lessThan. (NullAllowed: if null (or empty), no condition)
     */
    public void setName_LessThan(String name) {
        regName(CK_LT, fRES(name));
    }

    /**
     * GreaterEqual(&gt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br />
     * NAME: {NotNull, VARCHAR(100)}
     * @param name The value of name as greaterEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setName_GreaterEqual(String name) {
        regName(CK_GE, fRES(name));
    }

    /**
     * LessEqual(&lt;=). And NullOrEmptyIgnored, OnlyOnceRegistered. <br />
     * NAME: {NotNull, VARCHAR(100)}
     * @param name The value of name as lessEqual. (NullAllowed: if null (or empty), no condition)
     */
    public void setName_LessEqual(String name) {
        regName(CK_LE, fRES(name));
    }

    /**
     * InScope {in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br />
     * NAME: {NotNull, VARCHAR(100)}
     * @param nameList The collection of name as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setName_InScope(Collection<String> nameList) {
        doSetName_InScope(nameList);
    }

    public void doSetName_InScope(Collection<String> nameList) {
        regINS(CK_INS, cTL(nameList), getCValueName(), "NAME");
    }

    /**
     * NotInScope {not in ('a', 'b')}. And NullOrEmptyIgnored, NullOrEmptyElementIgnored, SeveralRegistered. <br />
     * NAME: {NotNull, VARCHAR(100)}
     * @param nameList The collection of name as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setName_NotInScope(Collection<String> nameList) {
        doSetName_NotInScope(nameList);
    }

    public void doSetName_NotInScope(Collection<String> nameList) {
        regINS(CK_NINS, cTL(nameList), getCValueName(), "NAME");
    }

    /**
     * PrefixSearch {like 'xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br />
     * NAME: {NotNull, VARCHAR(100)}
     * @param name The value of name as prefixSearch. (NullAllowed: if null (or empty), no condition)
     */
    public void setName_PrefixSearch(String name) {
        setName_LikeSearch(name, cLSOP());
    }

    /**
     * LikeSearch with various options. (versatile) {like '%xxx%' escape ...}. And NullOrEmptyIgnored, SeveralRegistered. <br />
     * NAME: {NotNull, VARCHAR(100)} <br />
     * <pre>e.g. setName_LikeSearch("xxx", new <span style="color: #FD4747">LikeSearchOption</span>().likeContain());</pre>
     * @param name The value of name as likeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of like-search. (NotNull)
     */
    public void setName_LikeSearch(String name, LikeSearchOption likeSearchOption) {
        regLSQ(CK_LS, fRES(name), getCValueName(), "NAME", likeSearchOption);
    }

    /**
     * NotLikeSearch with various options. (versatile) {not like 'xxx%' escape ...} <br />
     * And NullOrEmptyIgnored, SeveralRegistered. <br />
     * NAME: {NotNull, VARCHAR(100)}
     * @param name The value of name as notLikeSearch. (NullAllowed: if null (or empty), no condition)
     * @param likeSearchOption The option of not-like-search. (NotNull)
     */
    public void setName_NotLikeSearch(String name, LikeSearchOption likeSearchOption) {
        regLSQ(CK_NLS, fRES(name), getCValueName(), "NAME", likeSearchOption);
    }

    protected void regName(ConditionKey k, Object v) { regQ(k, v, getCValueName(), "NAME"); }
    abstract protected ConditionValue getCValueName();
    
    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br />
     * PRICE: {NotNull, INT(10)}
     * @param price The value of price as equal. (NullAllowed: if null, no condition)
     */
    public void setPrice_Equal(Integer price) {
        doSetPrice_Equal(price);
    }

    protected void doSetPrice_Equal(Integer price) {
        regPrice(CK_EQ, price);
    }

    /**
     * NotEqual(&lt;&gt;). And NullIgnored, OnlyOnceRegistered. <br />
     * PRICE: {NotNull, INT(10)}
     * @param price The value of price as notEqual. (NullAllowed: if null, no condition)
     */
    public void setPrice_NotEqual(Integer price) {
        doSetPrice_NotEqual(price);
    }

    protected void doSetPrice_NotEqual(Integer price) {
        regPrice(CK_NES, price);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br />
     * PRICE: {NotNull, INT(10)}
     * @param price The value of price as greaterThan. (NullAllowed: if null, no condition)
     */
    public void setPrice_GreaterThan(Integer price) {
        regPrice(CK_GT, price);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br />
     * PRICE: {NotNull, INT(10)}
     * @param price The value of price as lessThan. (NullAllowed: if null, no condition)
     */
    public void setPrice_LessThan(Integer price) {
        regPrice(CK_LT, price);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br />
     * PRICE: {NotNull, INT(10)}
     * @param price The value of price as greaterEqual. (NullAllowed: if null, no condition)
     */
    public void setPrice_GreaterEqual(Integer price) {
        regPrice(CK_GE, price);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br />
     * PRICE: {NotNull, INT(10)}
     * @param price The value of price as lessEqual. (NullAllowed: if null, no condition)
     */
    public void setPrice_LessEqual(Integer price) {
        regPrice(CK_LE, price);
    }

    /**
     * RangeOf with various options. (versatile) <br />
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br />
     * And NullIgnored, OnlyOnceRegistered. <br />
     * PRICE: {NotNull, INT(10)}
     * @param minNumber The min number of price. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of price. (NullAllowed: if null, no to-condition)
     * @param rangeOfOption The option of range-of. (NotNull)
     */
    public void setPrice_RangeOf(Integer minNumber, Integer maxNumber, RangeOfOption rangeOfOption) {
        regROO(minNumber, maxNumber, getCValuePrice(), "PRICE", rangeOfOption);
    }

    /**
     * InScope {in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br />
     * PRICE: {NotNull, INT(10)}
     * @param priceList The collection of price as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setPrice_InScope(Collection<Integer> priceList) {
        doSetPrice_InScope(priceList);
    }

    protected void doSetPrice_InScope(Collection<Integer> priceList) {
        regINS(CK_INS, cTL(priceList), getCValuePrice(), "PRICE");
    }

    /**
     * NotInScope {not in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br />
     * PRICE: {NotNull, INT(10)}
     * @param priceList The collection of price as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setPrice_NotInScope(Collection<Integer> priceList) {
        doSetPrice_NotInScope(priceList);
    }

    protected void doSetPrice_NotInScope(Collection<Integer> priceList) {
        regINS(CK_NINS, cTL(priceList), getCValuePrice(), "PRICE");
    }

    protected void regPrice(ConditionKey k, Object v) { regQ(k, v, getCValuePrice(), "PRICE"); }
    abstract protected ConditionValue getCValuePrice();
    
    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br />
     * TYPE: {NotNull, INT(10)}
     * @param type The value of type as equal. (NullAllowed: if null, no condition)
     */
    public void setType_Equal(Integer type) {
        doSetType_Equal(type);
    }

    protected void doSetType_Equal(Integer type) {
        regType(CK_EQ, type);
    }

    /**
     * NotEqual(&lt;&gt;). And NullIgnored, OnlyOnceRegistered. <br />
     * TYPE: {NotNull, INT(10)}
     * @param type The value of type as notEqual. (NullAllowed: if null, no condition)
     */
    public void setType_NotEqual(Integer type) {
        doSetType_NotEqual(type);
    }

    protected void doSetType_NotEqual(Integer type) {
        regType(CK_NES, type);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br />
     * TYPE: {NotNull, INT(10)}
     * @param type The value of type as greaterThan. (NullAllowed: if null, no condition)
     */
    public void setType_GreaterThan(Integer type) {
        regType(CK_GT, type);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br />
     * TYPE: {NotNull, INT(10)}
     * @param type The value of type as lessThan. (NullAllowed: if null, no condition)
     */
    public void setType_LessThan(Integer type) {
        regType(CK_LT, type);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br />
     * TYPE: {NotNull, INT(10)}
     * @param type The value of type as greaterEqual. (NullAllowed: if null, no condition)
     */
    public void setType_GreaterEqual(Integer type) {
        regType(CK_GE, type);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br />
     * TYPE: {NotNull, INT(10)}
     * @param type The value of type as lessEqual. (NullAllowed: if null, no condition)
     */
    public void setType_LessEqual(Integer type) {
        regType(CK_LE, type);
    }

    /**
     * RangeOf with various options. (versatile) <br />
     * {(default) minNumber &lt;= column &lt;= maxNumber} <br />
     * And NullIgnored, OnlyOnceRegistered. <br />
     * TYPE: {NotNull, INT(10)}
     * @param minNumber The min number of type. (NullAllowed: if null, no from-condition)
     * @param maxNumber The max number of type. (NullAllowed: if null, no to-condition)
     * @param rangeOfOption The option of range-of. (NotNull)
     */
    public void setType_RangeOf(Integer minNumber, Integer maxNumber, RangeOfOption rangeOfOption) {
        regROO(minNumber, maxNumber, getCValueType(), "TYPE", rangeOfOption);
    }

    /**
     * InScope {in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br />
     * TYPE: {NotNull, INT(10)}
     * @param typeList The collection of type as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setType_InScope(Collection<Integer> typeList) {
        doSetType_InScope(typeList);
    }

    protected void doSetType_InScope(Collection<Integer> typeList) {
        regINS(CK_INS, cTL(typeList), getCValueType(), "TYPE");
    }

    /**
     * NotInScope {not in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br />
     * TYPE: {NotNull, INT(10)}
     * @param typeList The collection of type as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setType_NotInScope(Collection<Integer> typeList) {
        doSetType_NotInScope(typeList);
    }

    protected void doSetType_NotInScope(Collection<Integer> typeList) {
        regINS(CK_NINS, cTL(typeList), getCValueType(), "TYPE");
    }

    protected void regType(ConditionKey k, Object v) { regQ(k, v, getCValueType(), "TYPE"); }
    abstract protected ConditionValue getCValueType();
    
    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br />
     * VALID_FLG: {NotNull, INT(10), classification=Flg}
     * @param validFlg The value of validFlg as equal. (NullAllowed: if null, no condition)
     */
    public void setValidFlg_Equal(Integer validFlg) {
        doSetValidFlg_Equal(validFlg);
    }

    /**
     * Equal(=). As Flg. And NullIgnored, OnlyOnceRegistered. <br />
     * VALID_FLG: {NotNull, INT(10), classification=Flg} <br />
     * 有効フラグ
     * @param cdef The instance of classification definition (as ENUM type). (NullAllowed: if null, no condition)
     */
    public void setValidFlg_Equal_AsFlg(CDef.Flg cdef) {
        doSetValidFlg_Equal(cTNum(cdef != null ? cdef.code() : null, Integer.class));
    }

    /**
     * Equal(=). As Valid (1). And NullIgnored, OnlyOnceRegistered. <br />
     * 有効: 有効の状態。リストに表示される
     */
    public void setValidFlg_Equal_Valid() {
        setValidFlg_Equal_AsFlg(CDef.Flg.Valid);
    }

    /**
     * Equal(=). As Invalid (0). And NullIgnored, OnlyOnceRegistered. <br />
     * 無効: 無効の状態。リストに表示されない
     */
    public void setValidFlg_Equal_Invalid() {
        setValidFlg_Equal_AsFlg(CDef.Flg.Invalid);
    }

    protected void doSetValidFlg_Equal(Integer validFlg) {
        regValidFlg(CK_EQ, validFlg);
    }

    /**
     * NotEqual(&lt;&gt;). And NullIgnored, OnlyOnceRegistered. <br />
     * VALID_FLG: {NotNull, INT(10), classification=Flg}
     * @param validFlg The value of validFlg as notEqual. (NullAllowed: if null, no condition)
     */
    public void setValidFlg_NotEqual(Integer validFlg) {
        doSetValidFlg_NotEqual(validFlg);
    }

    /**
     * NotEqual(&lt;&gt;). As Flg. And NullIgnored, OnlyOnceRegistered. <br />
     * VALID_FLG: {NotNull, INT(10), classification=Flg} <br />
     * 有効フラグ
     * @param cdef The instance of classification definition (as ENUM type). (NullAllowed: if null, no condition)
     */
    public void setValidFlg_NotEqual_AsFlg(CDef.Flg cdef) {
        doSetValidFlg_NotEqual(cTNum(cdef != null ? cdef.code() : null, Integer.class));
    }

    /**
     * NotEqual(&lt;&gt;). As Valid (1). And NullIgnored, OnlyOnceRegistered. <br />
     * 有効: 有効の状態。リストに表示される
     */
    public void setValidFlg_NotEqual_Valid() {
        setValidFlg_NotEqual_AsFlg(CDef.Flg.Valid);
    }

    /**
     * NotEqual(&lt;&gt;). As Invalid (0). And NullIgnored, OnlyOnceRegistered. <br />
     * 無効: 無効の状態。リストに表示されない
     */
    public void setValidFlg_NotEqual_Invalid() {
        setValidFlg_NotEqual_AsFlg(CDef.Flg.Invalid);
    }

    protected void doSetValidFlg_NotEqual(Integer validFlg) {
        regValidFlg(CK_NES, validFlg);
    }

    /**
     * InScope {in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br />
     * VALID_FLG: {NotNull, INT(10), classification=Flg}
     * @param validFlgList The collection of validFlg as inScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setValidFlg_InScope(Collection<Integer> validFlgList) {
        doSetValidFlg_InScope(validFlgList);
    }

    /**
     * InScope {in (1, 2)}. As Flg. And NullIgnored, NullElementIgnored, SeveralRegistered. <br />
     * VALID_FLG: {NotNull, INT(10), classification=Flg} <br />
     * 有効フラグ
     * @param cdefList The list of classification definition (as ENUM type). (NullAllowed: if null (or empty), no condition)
     */
    public void setValidFlg_InScope_AsFlg(Collection<CDef.Flg> cdefList) {
        doSetValidFlg_InScope(cTNumL(cdefList, Integer.class));
    }

    protected void doSetValidFlg_InScope(Collection<Integer> validFlgList) {
        regINS(CK_INS, cTL(validFlgList), getCValueValidFlg(), "VALID_FLG");
    }

    /**
     * NotInScope {not in (1, 2)}. And NullIgnored, NullElementIgnored, SeveralRegistered. <br />
     * VALID_FLG: {NotNull, INT(10), classification=Flg}
     * @param validFlgList The collection of validFlg as notInScope. (NullAllowed: if null (or empty), no condition)
     */
    public void setValidFlg_NotInScope(Collection<Integer> validFlgList) {
        doSetValidFlg_NotInScope(validFlgList);
    }

    /**
     * NotInScope {not in (1, 2)}. As Flg. And NullIgnored, NullElementIgnored, SeveralRegistered. <br />
     * VALID_FLG: {NotNull, INT(10), classification=Flg} <br />
     * 有効フラグ
     * @param cdefList The list of classification definition (as ENUM type). (NullAllowed: if null (or empty), no condition)
     */
    public void setValidFlg_NotInScope_AsFlg(Collection<CDef.Flg> cdefList) {
        doSetValidFlg_NotInScope(cTNumL(cdefList, Integer.class));
    }

    protected void doSetValidFlg_NotInScope(Collection<Integer> validFlgList) {
        regINS(CK_NINS, cTL(validFlgList), getCValueValidFlg(), "VALID_FLG");
    }

    protected void regValidFlg(ConditionKey k, Object v) { regQ(k, v, getCValueValidFlg(), "VALID_FLG"); }
    abstract protected ConditionValue getCValueValidFlg();

    /**
     * Equal(=). And NullIgnored, OnlyOnceRegistered. <br />
     * REGISTERED: {NotNull, TIMESTAMP(19), default=[CURRENT_TIMESTAMP]}
     * @param registered The value of registered as equal. (NullAllowed: if null, no condition)
     */
    public void setRegistered_Equal(java.sql.Timestamp registered) {
        regRegistered(CK_EQ,  registered);
    }

    /**
     * GreaterThan(&gt;). And NullIgnored, OnlyOnceRegistered. <br />
     * REGISTERED: {NotNull, TIMESTAMP(19), default=[CURRENT_TIMESTAMP]}
     * @param registered The value of registered as greaterThan. (NullAllowed: if null, no condition)
     */
    public void setRegistered_GreaterThan(java.sql.Timestamp registered) {
        regRegistered(CK_GT,  registered);
    }

    /**
     * LessThan(&lt;). And NullIgnored, OnlyOnceRegistered. <br />
     * REGISTERED: {NotNull, TIMESTAMP(19), default=[CURRENT_TIMESTAMP]}
     * @param registered The value of registered as lessThan. (NullAllowed: if null, no condition)
     */
    public void setRegistered_LessThan(java.sql.Timestamp registered) {
        regRegistered(CK_LT,  registered);
    }

    /**
     * GreaterEqual(&gt;=). And NullIgnored, OnlyOnceRegistered. <br />
     * REGISTERED: {NotNull, TIMESTAMP(19), default=[CURRENT_TIMESTAMP]}
     * @param registered The value of registered as greaterEqual. (NullAllowed: if null, no condition)
     */
    public void setRegistered_GreaterEqual(java.sql.Timestamp registered) {
        regRegistered(CK_GE,  registered);
    }

    /**
     * LessEqual(&lt;=). And NullIgnored, OnlyOnceRegistered. <br />
     * REGISTERED: {NotNull, TIMESTAMP(19), default=[CURRENT_TIMESTAMP]}
     * @param registered The value of registered as lessEqual. (NullAllowed: if null, no condition)
     */
    public void setRegistered_LessEqual(java.sql.Timestamp registered) {
        regRegistered(CK_LE, registered);
    }

    /**
     * FromTo with various options. (versatile) {(default) fromDatetime &lt;= column &lt;= toDatetime} <br />
     * And NullIgnored, OnlyOnceRegistered. <br />
     * REGISTERED: {NotNull, TIMESTAMP(19), default=[CURRENT_TIMESTAMP]}
     * <pre>e.g. setRegistered_FromTo(fromDate, toDate, new <span style="color: #FD4747">FromToOption</span>().compareAsDate());</pre>
     * @param fromDatetime The from-datetime(yyyy/MM/dd HH:mm:ss.SSS) of registered. (NullAllowed: if null, no from-condition)
     * @param toDatetime The to-datetime(yyyy/MM/dd HH:mm:ss.SSS) of registered. (NullAllowed: if null, no to-condition)
     * @param fromToOption The option of from-to. (NotNull)
     */
    public void setRegistered_FromTo(java.util.Date fromDatetime, java.util.Date toDatetime, FromToOption fromToOption) {
        regFTQ((fromDatetime != null ? new java.sql.Timestamp(fromDatetime.getTime()) : null), (toDatetime != null ? new java.sql.Timestamp(toDatetime.getTime()) : null), getCValueRegistered(), "REGISTERED", fromToOption);
    }

    /**
     * DateFromTo. (Date means yyyy/MM/dd) {fromDate &lt;= column &lt; toDate + 1 day} <br />
     * And NullIgnored, OnlyOnceRegistered. <br />
     * REGISTERED: {NotNull, TIMESTAMP(19), default=[CURRENT_TIMESTAMP]}
     * <pre>
     * e.g. from:{2007/04/10 08:24:53} to:{2007/04/16 14:36:29}
     *  column &gt;= '2007/04/10 00:00:00' and column <span style="color: #FD4747">&lt; '2007/04/17 00:00:00'</span>
     * </pre>
     * @param fromDate The from-date(yyyy/MM/dd) of registered. (NullAllowed: if null, no from-condition)
     * @param toDate The to-date(yyyy/MM/dd) of registered. (NullAllowed: if null, no to-condition)
     */
    public void setRegistered_DateFromTo(java.util.Date fromDate, java.util.Date toDate) {
        setRegistered_FromTo(fromDate, toDate, new FromToOption().compareAsDate());
    }

    protected void regRegistered(ConditionKey k, Object v) { regQ(k, v, getCValueRegistered(), "REGISTERED"); }
    abstract protected ConditionValue getCValueRegistered();

    // ===================================================================================
    //                                                                     ScalarCondition
    //                                                                     ===============
    /**
     * Prepare ScalarCondition as equal. <br />
     * {where FOO = (select max(BAR) from ...)
     * <pre>
     * cb.query().<span style="color: #FD4747">scalar_Equal()</span>.max(new SubQuery&lt;ProductsCB&gt;() {
     *     public void query(ProductsCB subCB) {
     *         subCB.specify().setXxx... <span style="color: #3F7E5E">// derived column for function</span>
     *         subCB.query().setYyy...
     *     }
     * });
     * </pre>
     * @return The object to set up a function. (NotNull)
     */
    public HpSSQFunction<ProductsCB> scalar_Equal() {
        return xcreateSSQFunction(CK_EQ.getOperand());
    }

    /**
     * Prepare ScalarCondition as equal. <br />
     * {where FOO &lt;&gt; (select max(BAR) from ...)
     * <pre>
     * cb.query().<span style="color: #FD4747">scalar_NotEqual()</span>.max(new SubQuery&lt;ProductsCB&gt;() {
     *     public void query(ProductsCB subCB) {
     *         subCB.specify().setXxx... <span style="color: #3F7E5E">// derived column for function</span>
     *         subCB.query().setYyy...
     *     }
     * });
     * </pre>
     * @return The object to set up a function. (NotNull)
     */
    public HpSSQFunction<ProductsCB> scalar_NotEqual() {
        return xcreateSSQFunction(CK_NES.getOperand());
    }

    /**
     * Prepare ScalarCondition as greaterThan. <br />
     * {where FOO &gt; (select max(BAR) from ...)
     * <pre>
     * cb.query().<span style="color: #FD4747">scalar_GreaterThan()</span>.max(new SubQuery&lt;ProductsCB&gt;() {
     *     public void query(ProductsCB subCB) {
     *         subCB.specify().setFoo... <span style="color: #3F7E5E">// derived column for function</span>
     *         subCB.query().setBar...
     *     }
     * });
     * </pre>
     * @return The object to set up a function. (NotNull)
     */
    public HpSSQFunction<ProductsCB> scalar_GreaterThan() {
        return xcreateSSQFunction(CK_GT.getOperand());
    }

    /**
     * Prepare ScalarCondition as lessThan. <br />
     * {where FOO &lt; (select max(BAR) from ...)
     * <pre>
     * cb.query().<span style="color: #FD4747">scalar_LessThan()</span>.max(new SubQuery&lt;ProductsCB&gt;() {
     *     public void query(ProductsCB subCB) {
     *         subCB.specify().setFoo... <span style="color: #3F7E5E">// derived column for function</span>
     *         subCB.query().setBar...
     *     }
     * });
     * </pre>
     * @return The object to set up a function. (NotNull)
     */
    public HpSSQFunction<ProductsCB> scalar_LessThan() {
        return xcreateSSQFunction(CK_LT.getOperand());
    }

    /**
     * Prepare ScalarCondition as greaterEqual. <br />
     * {where FOO &gt;= (select max(BAR) from ...)
     * <pre>
     * cb.query().<span style="color: #FD4747">scalar_GreaterEqual()</span>.max(new SubQuery&lt;ProductsCB&gt;() {
     *     public void query(ProductsCB subCB) {
     *         subCB.specify().setFoo... <span style="color: #3F7E5E">// derived column for function</span>
     *         subCB.query().setBar...
     *     }
     * });
     * </pre>
     * @return The object to set up a function. (NotNull)
     */
    public HpSSQFunction<ProductsCB> scalar_GreaterEqual() {
        return xcreateSSQFunction(CK_GE.getOperand());
    }

    /**
     * Prepare ScalarCondition as lessEqual. <br />
     * {where FOO &lt;= (select max(BAR) from ...)
     * <pre>
     * cb.query().<span style="color: #FD4747">scalar_LessEqual()</span>.max(new SubQuery&lt;ProductsCB&gt;() {
     *     public void query(ProductsCB subCB) {
     *         subCB.specify().setFoo... <span style="color: #3F7E5E">// derived column for function</span>
     *         subCB.query().setBar...
     *     }
     * });
     * </pre>
     * @return The object to set up a function. (NotNull)
     */
    public HpSSQFunction<ProductsCB> scalar_LessEqual() {
        return xcreateSSQFunction(CK_LE.getOperand());
    }

    protected HpSSQFunction<ProductsCB> xcreateSSQFunction(final String operand) {
        return new HpSSQFunction<ProductsCB>(new HpSSQSetupper<ProductsCB>() {
            public void setup(String function, SubQuery<ProductsCB> subQuery, HpSSQOption<ProductsCB> option) {
                xscalarCondition(function, subQuery, operand, option);
            }
        });
    }

    protected void xscalarCondition(String function, SubQuery<ProductsCB> subQuery, String operand, HpSSQOption<ProductsCB> option) {
        assertObjectNotNull("subQuery<ProductsCB>", subQuery);
        ProductsCB cb = xcreateScalarConditionCB(); subQuery.query(cb);
        String subQueryPropertyName = keepScalarCondition(cb.query()); // for saving query-value
        option.setPartitionByCBean(xcreateScalarConditionPartitionByCB()); // for using partition-by
        registerScalarCondition(function, cb.query(), subQueryPropertyName, operand, option);
    }
    public abstract String keepScalarCondition(ProductsCQ subQuery);

    protected ProductsCB xcreateScalarConditionCB() {
        ProductsCB cb = new ProductsCB();
        cb.xsetupForScalarCondition(this);
        return cb;
    }

    protected ProductsCB xcreateScalarConditionPartitionByCB() {
        ProductsCB cb = new ProductsCB();
        cb.xsetupForScalarConditionPartitionBy(this);
        return cb;
    }

    // ===================================================================================
    //                                                                       MyselfDerived
    //                                                                       =============
    public void xsmyselfDerive(String function, SubQuery<ProductsCB> subQuery, String aliasName, DerivedReferrerOption option) {
        assertObjectNotNull("subQuery<ProductsCB>", subQuery);
        ProductsCB cb = new ProductsCB(); cb.xsetupForDerivedReferrer(this); subQuery.query(cb);
        String subQueryPropertyName = keepSpecifyMyselfDerived(cb.query()); // for saving query-value.
        registerSpecifyMyselfDerived(function, cb.query(), "ID", "ID", subQueryPropertyName, "myselfDerived", aliasName, option);
    }
    public abstract String keepSpecifyMyselfDerived(ProductsCQ subQuery);

    /**
     * Prepare for (Query)MyselfDerived (SubQuery).
     * @return The object to set up a function for myself table. (NotNull)
     */
    public HpQDRFunction<ProductsCB> myselfDerived() {
        return xcreateQDRFunctionMyselfDerived();
    }
    protected HpQDRFunction<ProductsCB> xcreateQDRFunctionMyselfDerived() {
        return new HpQDRFunction<ProductsCB>(new HpQDRSetupper<ProductsCB>() {
            public void setup(String function, SubQuery<ProductsCB> subQuery, String operand, Object value, DerivedReferrerOption option) {
                xqderiveMyselfDerived(function, subQuery, operand, value, option);
            }
        });
    }
    public void xqderiveMyselfDerived(String function, SubQuery<ProductsCB> subQuery, String operand, Object value, DerivedReferrerOption option) {
        assertObjectNotNull("subQuery<ProductsCB>", subQuery);
        ProductsCB cb = new ProductsCB(); cb.xsetupForDerivedReferrer(this); subQuery.query(cb);
        String subQueryPropertyName = keepQueryMyselfDerived(cb.query()); // for saving query-value.
        String parameterPropertyName = keepQueryMyselfDerivedParameter(value);
        registerQueryMyselfDerived(function, cb.query(), "ID", "ID", subQueryPropertyName, "myselfDerived", operand, value, parameterPropertyName, option);
    }
    public abstract String keepQueryMyselfDerived(ProductsCQ subQuery);
    public abstract String keepQueryMyselfDerivedParameter(Object parameterValue);

    // ===================================================================================
    //                                                                        MyselfExists
    //                                                                        ============
    /**
     * Prepare for MyselfExists (SubQuery).
     * @param subQuery The implementation of sub query. (NotNull)
     */
    public void myselfExists(SubQuery<ProductsCB> subQuery) {
        assertObjectNotNull("subQuery<ProductsCB>", subQuery);
        ProductsCB cb = new ProductsCB(); cb.xsetupForMyselfExists(this); subQuery.query(cb);
        String subQueryPropertyName = keepMyselfExists(cb.query()); // for saving query-value.
        registerMyselfExists(cb.query(), subQueryPropertyName);
    }
    public abstract String keepMyselfExists(ProductsCQ subQuery);

    // ===================================================================================
    //                                                                       MyselfInScope
    //                                                                       =============
    /**
     * Prepare for MyselfInScope (SubQuery).
     * @param subQuery The implementation of sub query. (NotNull)
     */
    public void myselfInScope(SubQuery<ProductsCB> subQuery) {
        assertObjectNotNull("subQuery<ProductsCB>", subQuery);
        ProductsCB cb = new ProductsCB(); cb.xsetupForMyselfInScope(this); subQuery.query(cb);
        String subQueryPropertyName = keepMyselfInScope(cb.query()); // for saving query-value.
        registerMyselfInScope(cb.query(), subQueryPropertyName);
    }
    public abstract String keepMyselfInScope(ProductsCQ subQuery);

    // ===================================================================================
    //                                                                       Very Internal
    //                                                                       =============
    // very internal (for suppressing warn about 'Not Use Import')
    protected String xabCB() { return ProductsCB.class.getName(); }
    protected String xabCQ() { return ProductsCQ.class.getName(); }
    protected String xabLSO() { return LikeSearchOption.class.getName(); }
    protected String xabSSQS() { return HpSSQSetupper.class.getName(); }
}
