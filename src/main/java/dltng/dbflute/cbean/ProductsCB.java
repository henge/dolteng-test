package dltng.dbflute.cbean;

import dltng.dbflute.cbean.bs.BsProductsCB;

/**
 * The condition-bean of PRODUCTS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class ProductsCB extends BsProductsCB {
}
