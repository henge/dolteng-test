package dltng.dbflute.cbean;

import dltng.dbflute.cbean.bs.BsCategoriesCB;

/**
 * The condition-bean of CATEGORIES.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class CategoriesCB extends BsCategoriesCB {
}
