package dltng.dbflute.cbean;

import dltng.dbflute.cbean.bs.BsOptionsCB;

/**
 * The condition-bean of OPTIONS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class OptionsCB extends BsOptionsCB {
}
