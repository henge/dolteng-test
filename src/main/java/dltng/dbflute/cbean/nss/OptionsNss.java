package dltng.dbflute.cbean.nss;

import dltng.dbflute.cbean.cq.OptionsCQ;

/**
 * The nest select set-upper of OPTIONS.
 * @author DBFlute(AutoGenerator)
 */
public class OptionsNss {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected OptionsCQ _query;
    public OptionsNss(OptionsCQ query) { _query = query; }
    public boolean hasConditionQuery() { return _query != null; }

    // ===================================================================================
    //                                                                     Nested Relation
    //                                                                     ===============

}
