package dltng.dbflute.cbean.nss;

import dltng.dbflute.cbean.cq.CategoriesCQ;

/**
 * The nest select set-upper of CATEGORIES.
 * @author DBFlute(AutoGenerator)
 */
public class CategoriesNss {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected CategoriesCQ _query;
    public CategoriesNss(CategoriesCQ query) { _query = query; }
    public boolean hasConditionQuery() { return _query != null; }

    // ===================================================================================
    //                                                                     Nested Relation
    //                                                                     ===============

}
