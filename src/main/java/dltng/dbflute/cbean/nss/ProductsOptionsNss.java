package dltng.dbflute.cbean.nss;

import dltng.dbflute.cbean.cq.ProductsOptionsCQ;

/**
 * The nest select set-upper of PRODUCTS_OPTIONS.
 * @author DBFlute(AutoGenerator)
 */
public class ProductsOptionsNss {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected ProductsOptionsCQ _query;
    public ProductsOptionsNss(ProductsOptionsCQ query) { _query = query; }
    public boolean hasConditionQuery() { return _query != null; }

    // ===================================================================================
    //                                                                     Nested Relation
    //                                                                     ===============

}
