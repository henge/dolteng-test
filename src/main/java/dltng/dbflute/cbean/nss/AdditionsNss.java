package dltng.dbflute.cbean.nss;

import dltng.dbflute.cbean.cq.AdditionsCQ;

/**
 * The nest select set-upper of ADDITIONS.
 * @author DBFlute(AutoGenerator)
 */
public class AdditionsNss {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected AdditionsCQ _query;
    public AdditionsNss(AdditionsCQ query) { _query = query; }
    public boolean hasConditionQuery() { return _query != null; }

    // ===================================================================================
    //                                                                     Nested Relation
    //                                                                     ===============

}
