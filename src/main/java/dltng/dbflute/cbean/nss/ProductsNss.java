package dltng.dbflute.cbean.nss;

import dltng.dbflute.cbean.cq.ProductsCQ;

/**
 * The nest select set-upper of PRODUCTS.
 * @author DBFlute(AutoGenerator)
 */
public class ProductsNss {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected ProductsCQ _query;
    public ProductsNss(ProductsCQ query) { _query = query; }
    public boolean hasConditionQuery() { return _query != null; }

    // ===================================================================================
    //                                                                     Nested Relation
    //                                                                     ===============

}
