package dltng.dbflute.cbean.nss;

import dltng.dbflute.cbean.cq.SchemaInfoCQ;

/**
 * The nest select set-upper of SCHEMA_INFO.
 * @author DBFlute(AutoGenerator)
 */
public class SchemaInfoNss {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    protected SchemaInfoCQ _query;
    public SchemaInfoNss(SchemaInfoCQ query) { _query = query; }
    public boolean hasConditionQuery() { return _query != null; }

    // ===================================================================================
    //                                                                     Nested Relation
    //                                                                     ===============

}
