package dltng.dbflute.cbean;

import dltng.dbflute.cbean.bs.BsProductsOptionsCB;

/**
 * The condition-bean of PRODUCTS_OPTIONS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class ProductsOptionsCB extends BsProductsOptionsCB {
}
