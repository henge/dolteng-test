package dltng.dbflute.cbean;

import dltng.dbflute.cbean.bs.BsAdditionsCB;

/**
 * The condition-bean of ADDITIONS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class AdditionsCB extends BsAdditionsCB {
}
