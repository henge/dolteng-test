package dltng.dbflute.bsbhv;

import java.util.List;

import org.seasar.dbflute.*;
import org.seasar.dbflute.bhv.*;
import org.seasar.dbflute.cbean.*;
import org.seasar.dbflute.dbmeta.DBMeta;
import org.seasar.dbflute.outsidesql.executor.*;
import dltng.dbflute.exbhv.*;
import dltng.dbflute.exentity.*;
import dltng.dbflute.bsentity.dbmeta.*;
import dltng.dbflute.cbean.*;

/**
 * The behavior of SCHEMA_INFO as TABLE. <br />
 * <pre>
 * [primary key]
 *     
 * 
 * [column]
 *     VERSION
 * 
 * [sequence]
 *     
 * 
 * [identity]
 *     
 * 
 * [version-no]
 *     
 * 
 * [foreign table]
 *     
 * 
 * [referrer table]
 *     
 * 
 * [foreign property]
 *     
 * 
 * [referrer property]
 *     
 * </pre>
 * @author DBFlute(AutoGenerator)
 */
public abstract class BsSchemaInfoBhv extends AbstractBehaviorReadable {

    // ===================================================================================
    //                                                                          Definition
    //                                                                          ==========
    /*df:beginQueryPath*/
    /*df:endQueryPath*/

    // ===================================================================================
    //                                                                          Table name
    //                                                                          ==========
    /** @return The name on database of table. (NotNull) */
    public String getTableDbName() { return "SCHEMA_INFO"; }

    // ===================================================================================
    //                                                                              DBMeta
    //                                                                              ======
    /** @return The instance of DBMeta. (NotNull) */
    public DBMeta getDBMeta() { return SchemaInfoDbm.getInstance(); }

    /** @return The instance of DBMeta as my table type. (NotNull) */
    public SchemaInfoDbm getMyDBMeta() { return SchemaInfoDbm.getInstance(); }

    // ===================================================================================
    //                                                                        New Instance
    //                                                                        ============
    /** {@inheritDoc} */
    public Entity newEntity() { return newMyEntity(); }

    /** {@inheritDoc} */
    public ConditionBean newConditionBean() { return newMyConditionBean(); }

    /** @return The instance of new entity as my table type. (NotNull) */
    public SchemaInfo newMyEntity() { return new SchemaInfo(); }

    /** @return The instance of new condition-bean as my table type. (NotNull) */
    public SchemaInfoCB newMyConditionBean() { return new SchemaInfoCB(); }

    // ===================================================================================
    //                                                                        Count Select
    //                                                                        ============
    /**
     * Select the count of uniquely-selected records by the condition-bean. {IgnorePagingCondition, IgnoreSpecifyColumn}<br />
     * SpecifyColumn is ignored but you can use it only to remove text type column for union's distinct.
     * <pre>
     * SchemaInfoCB cb = new SchemaInfoCB();
     * cb.query().setFoo...(value);
     * int count = schemaInfoBhv.<span style="color: #FD4747">selectCount</span>(cb);
     * </pre>
     * @param cb The condition-bean of SchemaInfo. (NotNull)
     * @return The count for the condition. (NotMinus)
     */
    public int selectCount(SchemaInfoCB cb) {
        return doSelectCountUniquely(cb);
    }

    protected int doSelectCountUniquely(SchemaInfoCB cb) { // called by selectCount(cb) 
        assertCBStateValid(cb);
        return delegateSelectCountUniquely(cb);
    }

    protected int doSelectCountPlainly(SchemaInfoCB cb) { // called by selectPage(cb)
        assertCBStateValid(cb);
        return delegateSelectCountPlainly(cb);
    }

    @Override
    protected int doReadCount(ConditionBean cb) {
        return selectCount(downcast(cb));
    }

    // ===================================================================================
    //                                                                       Entity Select
    //                                                                       =============
    /**
     * Select the entity by the condition-bean.
     * <pre>
     * SchemaInfoCB cb = new SchemaInfoCB();
     * cb.query().setFoo...(value);
     * SchemaInfo schemaInfo = schemaInfoBhv.<span style="color: #FD4747">selectEntity</span>(cb);
     * if (schemaInfo != null) {
     *     ... = schemaInfo.get...();
     * } else {
     *     ...
     * }
     * </pre>
     * @param cb The condition-bean of SchemaInfo. (NotNull)
     * @return The entity selected by the condition. (NullAllowed: if no data, it returns null)
     * @exception org.seasar.dbflute.exception.EntityDuplicatedException When the entity has been duplicated.
     * @exception org.seasar.dbflute.exception.SelectEntityConditionNotFoundException When the condition for selecting an entity is not found.
     */
    public SchemaInfo selectEntity(SchemaInfoCB cb) {
        return doSelectEntity(cb, SchemaInfo.class);
    }

    protected <ENTITY extends SchemaInfo> ENTITY doSelectEntity(final SchemaInfoCB cb, Class<ENTITY> entityType) {
        assertCBStateValid(cb);
        return helpSelectEntityInternally(cb, entityType, new InternalSelectEntityCallback<ENTITY, SchemaInfoCB>() {
            public List<ENTITY> callbackSelectList(SchemaInfoCB cb, Class<ENTITY> entityType) { return doSelectList(cb, entityType); } });
    }

    @Override
    protected Entity doReadEntity(ConditionBean cb) {
        return selectEntity(downcast(cb));
    }

    /**
     * Select the entity by the condition-bean with deleted check.
     * <pre>
     * SchemaInfoCB cb = new SchemaInfoCB();
     * cb.query().setFoo...(value);
     * SchemaInfo schemaInfo = schemaInfoBhv.<span style="color: #FD4747">selectEntityWithDeletedCheck</span>(cb);
     * ... = schemaInfo.get...(); <span style="color: #3F7E5E">// the entity always be not null</span>
     * </pre>
     * @param cb The condition-bean of SchemaInfo. (NotNull)
     * @return The entity selected by the condition. (NotNull: if no data, throws exception)
     * @exception org.seasar.dbflute.exception.EntityAlreadyDeletedException When the entity has already been deleted. (not found)
     * @exception org.seasar.dbflute.exception.EntityDuplicatedException When the entity has been duplicated.
     * @exception org.seasar.dbflute.exception.SelectEntityConditionNotFoundException When the condition for selecting an entity is not found.
     */
    public SchemaInfo selectEntityWithDeletedCheck(SchemaInfoCB cb) {
        return doSelectEntityWithDeletedCheck(cb, SchemaInfo.class);
    }

    protected <ENTITY extends SchemaInfo> ENTITY doSelectEntityWithDeletedCheck(final SchemaInfoCB cb, Class<ENTITY> entityType) {
        assertCBStateValid(cb);
        return helpSelectEntityWithDeletedCheckInternally(cb, entityType, new InternalSelectEntityWithDeletedCheckCallback<ENTITY, SchemaInfoCB>() {
            public List<ENTITY> callbackSelectList(SchemaInfoCB cb, Class<ENTITY> entityType) { return doSelectList(cb, entityType); } });
    }

    @Override
    protected Entity doReadEntityWithDeletedCheck(ConditionBean cb) {
        return selectEntityWithDeletedCheck(downcast(cb));
    }

    // ===================================================================================
    //                                                                         List Select
    //                                                                         ===========
    /**
     * Select the list as result bean.
     * <pre>
     * SchemaInfoCB cb = new SchemaInfoCB();
     * cb.query().setFoo...(value);
     * cb.query().addOrderBy_Bar...();
     * ListResultBean&lt;SchemaInfo&gt; schemaInfoList = schemaInfoBhv.<span style="color: #FD4747">selectList</span>(cb);
     * for (SchemaInfo schemaInfo : schemaInfoList) {
     *     ... = schemaInfo.get...();
     * }
     * </pre>
     * @param cb The condition-bean of SchemaInfo. (NotNull)
     * @return The result bean of selected list. (NotNull: if no data, returns empty list)
     * @exception org.seasar.dbflute.exception.DangerousResultSizeException When the result size is over the specified safety size.
     */
    public ListResultBean<SchemaInfo> selectList(SchemaInfoCB cb) {
        return doSelectList(cb, SchemaInfo.class);
    }

    protected <ENTITY extends SchemaInfo> ListResultBean<ENTITY> doSelectList(SchemaInfoCB cb, Class<ENTITY> entityType) {
        assertCBStateValid(cb); assertObjectNotNull("entityType", entityType);
        assertSpecifyDerivedReferrerEntityProperty(cb, entityType);
        return helpSelectListInternally(cb, entityType, new InternalSelectListCallback<ENTITY, SchemaInfoCB>() {
            public List<ENTITY> callbackSelectList(SchemaInfoCB cb, Class<ENTITY> entityType) { return delegateSelectList(cb, entityType); } });
    }

    @Override
    protected ListResultBean<? extends Entity> doReadList(ConditionBean cb) {
        return selectList(downcast(cb));
    }

    // ===================================================================================
    //                                                                         Page Select
    //                                                                         ===========
    /**
     * Select the page as result bean. <br />
     * (both count-select and paging-select are executed)
     * <pre>
     * SchemaInfoCB cb = new SchemaInfoCB();
     * cb.query().setFoo...(value);
     * cb.query().addOrderBy_Bar...();
     * cb.<span style="color: #FD4747">paging</span>(20, 3); <span style="color: #3F7E5E">// 20 records per a page and current page number is 3</span>
     * PagingResultBean&lt;SchemaInfo&gt; page = schemaInfoBhv.<span style="color: #FD4747">selectPage</span>(cb);
     * int allRecordCount = page.getAllRecordCount();
     * int allPageCount = page.getAllPageCount();
     * boolean isExistPrePage = page.isExistPrePage();
     * boolean isExistNextPage = page.isExistNextPage();
     * ...
     * for (SchemaInfo schemaInfo : page) {
     *     ... = schemaInfo.get...();
     * }
     * </pre>
     * @param cb The condition-bean of SchemaInfo. (NotNull)
     * @return The result bean of selected page. (NotNull: if no data, returns bean as empty list)
     * @exception org.seasar.dbflute.exception.DangerousResultSizeException When the result size is over the specified safety size.
     */
    public PagingResultBean<SchemaInfo> selectPage(SchemaInfoCB cb) {
        return doSelectPage(cb, SchemaInfo.class);
    }

    protected <ENTITY extends SchemaInfo> PagingResultBean<ENTITY> doSelectPage(SchemaInfoCB cb, Class<ENTITY> entityType) {
        assertCBStateValid(cb); assertObjectNotNull("entityType", entityType);
        return helpSelectPageInternally(cb, entityType, new InternalSelectPageCallback<ENTITY, SchemaInfoCB>() {
            public int callbackSelectCount(SchemaInfoCB cb) { return doSelectCountPlainly(cb); }
            public List<ENTITY> callbackSelectList(SchemaInfoCB cb, Class<ENTITY> entityType) { return doSelectList(cb, entityType); }
        });
    }

    @Override
    protected PagingResultBean<? extends Entity> doReadPage(ConditionBean cb) {
        return selectPage(downcast(cb));
    }

    // ===================================================================================
    //                                                                       Cursor Select
    //                                                                       =============
    /**
     * Select the cursor by the condition-bean.
     * <pre>
     * SchemaInfoCB cb = new SchemaInfoCB();
     * cb.query().setFoo...(value);
     * schemaInfoBhv.<span style="color: #FD4747">selectCursor</span>(cb, new EntityRowHandler&lt;SchemaInfo&gt;() {
     *     public void handle(SchemaInfo entity) {
     *         ... = entity.getFoo...();
     *     }
     * });
     * </pre>
     * @param cb The condition-bean of SchemaInfo. (NotNull)
     * @param entityRowHandler The handler of entity row of SchemaInfo. (NotNull)
     */
    public void selectCursor(SchemaInfoCB cb, EntityRowHandler<SchemaInfo> entityRowHandler) {
        doSelectCursor(cb, entityRowHandler, SchemaInfo.class);
    }

    protected <ENTITY extends SchemaInfo> void doSelectCursor(SchemaInfoCB cb, EntityRowHandler<ENTITY> entityRowHandler, Class<ENTITY> entityType) {
        assertCBStateValid(cb); assertObjectNotNull("entityRowHandler<SchemaInfo>", entityRowHandler); assertObjectNotNull("entityType", entityType);
        assertSpecifyDerivedReferrerEntityProperty(cb, entityType);
        helpSelectCursorInternally(cb, entityRowHandler, entityType, new InternalSelectCursorCallback<ENTITY, SchemaInfoCB>() {
            public void callbackSelectCursor(SchemaInfoCB cb, EntityRowHandler<ENTITY> entityRowHandler, Class<ENTITY> entityType) { delegateSelectCursor(cb, entityRowHandler, entityType); }
            public List<ENTITY> callbackSelectList(SchemaInfoCB cb, Class<ENTITY> entityType) { return doSelectList(cb, entityType); }
        });
    }

    // ===================================================================================
    //                                                                       Scalar Select
    //                                                                       =============
    /**
     * Select the scalar value derived by a function from uniquely-selected records. <br />
     * You should call a function method after this method called like as follows:
     * <pre>
     * schemaInfoBhv.<span style="color: #FD4747">scalarSelect</span>(Date.class).max(new ScalarQuery() {
     *     public void query(SchemaInfoCB cb) {
     *         cb.specify().<span style="color: #FD4747">columnFooDatetime()</span>; <span style="color: #3F7E5E">// required for a function</span>
     *         cb.query().setBarName_PrefixSearch("S");
     *     }
     * });
     * </pre>
     * @param <RESULT> The type of result.
     * @param resultType The type of result. (NotNull)
     * @return The scalar value derived by a function. (NullAllowed)
     */
    public <RESULT> SLFunction<SchemaInfoCB, RESULT> scalarSelect(Class<RESULT> resultType) {
        return doScalarSelect(resultType, newMyConditionBean());
    }

    protected <RESULT, CB extends SchemaInfoCB> SLFunction<CB, RESULT> doScalarSelect(Class<RESULT> resultType, CB cb) {
        assertObjectNotNull("resultType", resultType); assertCBStateValid(cb);
        cb.xsetupForScalarSelect(); cb.getSqlClause().disableSelectIndex(); // for when you use union
        return new SLFunction<CB, RESULT>(cb, resultType);
    }

    // ===================================================================================
    //                                                                            Sequence
    //                                                                            ========
    @Override
    protected Number doReadNextVal() {
        String msg = "This table is NOT related to sequence: " + getTableDbName();
        throw new UnsupportedOperationException(msg);
    }

    // ===================================================================================
    //                                                                   Pull out Relation
    //                                                                   =================

    // ===================================================================================
    //                                                                      Extract Column
    //                                                                      ==============

    // ===================================================================================
    //                                                                          OutsideSql
    //                                                                          ==========
    /**
     * Prepare the basic executor of outside-SQL to execute it. <br />
     * The invoker of behavior command should be not null when you call this method.
     * <pre>
     * You can use the methods for outside-SQL are as follows:
     * {Basic}
     *   o selectList()
     *   o execute()
     *   o call()
     * 
     * {Entity}
     *   o entityHandling().selectEntity()
     *   o entityHandling().selectEntityWithDeletedCheck()
     * 
     * {Paging}
     *   o autoPaging().selectList()
     *   o autoPaging().selectPage()
     *   o manualPaging().selectList()
     *   o manualPaging().selectPage()
     * 
     * {Cursor}
     *   o cursorHandling().selectCursor()
     * 
     * {Option}
     *   o dynamicBinding().selectList()
     *   o removeBlockComment().selectList()
     *   o removeLineComment().selectList()
     *   o formatSql().selectList()
     * </pre>
     * @return The basic executor of outside-SQL. (NotNull) 
     */
    public OutsideSqlBasicExecutor<SchemaInfoBhv> outsideSql() {
        return doOutsideSql();
    }

    // ===================================================================================
    //                                                                     Delegate Method
    //                                                                     ===============
    // [Behavior Command]
    // -----------------------------------------------------
    //                                                Select
    //                                                ------
    protected int delegateSelectCountUniquely(SchemaInfoCB cb) { return invoke(createSelectCountCBCommand(cb, true)); }
    protected int delegateSelectCountPlainly(SchemaInfoCB cb) { return invoke(createSelectCountCBCommand(cb, false)); }
    protected <ENTITY extends SchemaInfo> void delegateSelectCursor(SchemaInfoCB cb, EntityRowHandler<ENTITY> erh, Class<ENTITY> et)
    { invoke(createSelectCursorCBCommand(cb, erh, et)); }
    protected <ENTITY extends SchemaInfo> List<ENTITY> delegateSelectList(SchemaInfoCB cb, Class<ENTITY> et)
    { return invoke(createSelectListCBCommand(cb, et)); }

    // ===================================================================================
    //                                                                Optimistic Lock Info
    //                                                                ====================
    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean hasVersionNoValue(Entity entity) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean hasUpdateDateValue(Entity entity) {
        return false;
    }

    // ===================================================================================
    //                                                                     Downcast Helper
    //                                                                     ===============
    protected SchemaInfo downcast(Entity entity) {
        return helpEntityDowncastInternally(entity, SchemaInfo.class);
    }

    protected SchemaInfoCB downcast(ConditionBean cb) {
        return helpConditionBeanDowncastInternally(cb, SchemaInfoCB.class);
    }

    @SuppressWarnings("unchecked")
    protected List<SchemaInfo> downcast(List<? extends Entity> entityList) {
        return (List<SchemaInfo>)entityList;
    }
}
