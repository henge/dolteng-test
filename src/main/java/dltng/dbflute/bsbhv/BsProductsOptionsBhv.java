package dltng.dbflute.bsbhv;

import java.util.List;

import org.seasar.dbflute.*;
import org.seasar.dbflute.bhv.*;
import org.seasar.dbflute.cbean.*;
import org.seasar.dbflute.dbmeta.DBMeta;
import org.seasar.dbflute.outsidesql.executor.*;
import dltng.dbflute.exbhv.*;
import dltng.dbflute.exentity.*;
import dltng.dbflute.bsentity.dbmeta.*;
import dltng.dbflute.cbean.*;

/**
 * The behavior of PRODUCTS_OPTIONS as TABLE. <br />
 * <pre>
 * [primary key]
 *     PRODUCT_ID, OPTION_ID
 * 
 * [column]
 *     PRODUCT_ID, OPTION_ID
 * 
 * [sequence]
 *     
 * 
 * [identity]
 *     
 * 
 * [version-no]
 *     
 * 
 * [foreign table]
 *     
 * 
 * [referrer table]
 *     
 * 
 * [foreign property]
 *     
 * 
 * [referrer property]
 *     
 * </pre>
 * @author DBFlute(AutoGenerator)
 */
public abstract class BsProductsOptionsBhv extends AbstractBehaviorWritable {

    // ===================================================================================
    //                                                                          Definition
    //                                                                          ==========
    /*df:beginQueryPath*/
    /*df:endQueryPath*/

    // ===================================================================================
    //                                                                          Table name
    //                                                                          ==========
    /** @return The name on database of table. (NotNull) */
    public String getTableDbName() { return "PRODUCTS_OPTIONS"; }

    // ===================================================================================
    //                                                                              DBMeta
    //                                                                              ======
    /** @return The instance of DBMeta. (NotNull) */
    public DBMeta getDBMeta() { return ProductsOptionsDbm.getInstance(); }

    /** @return The instance of DBMeta as my table type. (NotNull) */
    public ProductsOptionsDbm getMyDBMeta() { return ProductsOptionsDbm.getInstance(); }

    // ===================================================================================
    //                                                                        New Instance
    //                                                                        ============
    /** {@inheritDoc} */
    public Entity newEntity() { return newMyEntity(); }

    /** {@inheritDoc} */
    public ConditionBean newConditionBean() { return newMyConditionBean(); }

    /** @return The instance of new entity as my table type. (NotNull) */
    public ProductsOptions newMyEntity() { return new ProductsOptions(); }

    /** @return The instance of new condition-bean as my table type. (NotNull) */
    public ProductsOptionsCB newMyConditionBean() { return new ProductsOptionsCB(); }

    // ===================================================================================
    //                                                                        Count Select
    //                                                                        ============
    /**
     * Select the count of uniquely-selected records by the condition-bean. {IgnorePagingCondition, IgnoreSpecifyColumn}<br />
     * SpecifyColumn is ignored but you can use it only to remove text type column for union's distinct.
     * <pre>
     * ProductsOptionsCB cb = new ProductsOptionsCB();
     * cb.query().setFoo...(value);
     * int count = productsOptionsBhv.<span style="color: #FD4747">selectCount</span>(cb);
     * </pre>
     * @param cb The condition-bean of ProductsOptions. (NotNull)
     * @return The count for the condition. (NotMinus)
     */
    public int selectCount(ProductsOptionsCB cb) {
        return doSelectCountUniquely(cb);
    }

    protected int doSelectCountUniquely(ProductsOptionsCB cb) { // called by selectCount(cb) 
        assertCBStateValid(cb);
        return delegateSelectCountUniquely(cb);
    }

    protected int doSelectCountPlainly(ProductsOptionsCB cb) { // called by selectPage(cb)
        assertCBStateValid(cb);
        return delegateSelectCountPlainly(cb);
    }

    @Override
    protected int doReadCount(ConditionBean cb) {
        return selectCount(downcast(cb));
    }

    // ===================================================================================
    //                                                                       Entity Select
    //                                                                       =============
    /**
     * Select the entity by the condition-bean.
     * <pre>
     * ProductsOptionsCB cb = new ProductsOptionsCB();
     * cb.query().setFoo...(value);
     * ProductsOptions productsOptions = productsOptionsBhv.<span style="color: #FD4747">selectEntity</span>(cb);
     * if (productsOptions != null) {
     *     ... = productsOptions.get...();
     * } else {
     *     ...
     * }
     * </pre>
     * @param cb The condition-bean of ProductsOptions. (NotNull)
     * @return The entity selected by the condition. (NullAllowed: if no data, it returns null)
     * @exception org.seasar.dbflute.exception.EntityDuplicatedException When the entity has been duplicated.
     * @exception org.seasar.dbflute.exception.SelectEntityConditionNotFoundException When the condition for selecting an entity is not found.
     */
    public ProductsOptions selectEntity(ProductsOptionsCB cb) {
        return doSelectEntity(cb, ProductsOptions.class);
    }

    protected <ENTITY extends ProductsOptions> ENTITY doSelectEntity(final ProductsOptionsCB cb, Class<ENTITY> entityType) {
        assertCBStateValid(cb);
        return helpSelectEntityInternally(cb, entityType, new InternalSelectEntityCallback<ENTITY, ProductsOptionsCB>() {
            public List<ENTITY> callbackSelectList(ProductsOptionsCB cb, Class<ENTITY> entityType) { return doSelectList(cb, entityType); } });
    }

    @Override
    protected Entity doReadEntity(ConditionBean cb) {
        return selectEntity(downcast(cb));
    }

    /**
     * Select the entity by the condition-bean with deleted check.
     * <pre>
     * ProductsOptionsCB cb = new ProductsOptionsCB();
     * cb.query().setFoo...(value);
     * ProductsOptions productsOptions = productsOptionsBhv.<span style="color: #FD4747">selectEntityWithDeletedCheck</span>(cb);
     * ... = productsOptions.get...(); <span style="color: #3F7E5E">// the entity always be not null</span>
     * </pre>
     * @param cb The condition-bean of ProductsOptions. (NotNull)
     * @return The entity selected by the condition. (NotNull: if no data, throws exception)
     * @exception org.seasar.dbflute.exception.EntityAlreadyDeletedException When the entity has already been deleted. (not found)
     * @exception org.seasar.dbflute.exception.EntityDuplicatedException When the entity has been duplicated.
     * @exception org.seasar.dbflute.exception.SelectEntityConditionNotFoundException When the condition for selecting an entity is not found.
     */
    public ProductsOptions selectEntityWithDeletedCheck(ProductsOptionsCB cb) {
        return doSelectEntityWithDeletedCheck(cb, ProductsOptions.class);
    }

    protected <ENTITY extends ProductsOptions> ENTITY doSelectEntityWithDeletedCheck(final ProductsOptionsCB cb, Class<ENTITY> entityType) {
        assertCBStateValid(cb);
        return helpSelectEntityWithDeletedCheckInternally(cb, entityType, new InternalSelectEntityWithDeletedCheckCallback<ENTITY, ProductsOptionsCB>() {
            public List<ENTITY> callbackSelectList(ProductsOptionsCB cb, Class<ENTITY> entityType) { return doSelectList(cb, entityType); } });
    }

    @Override
    protected Entity doReadEntityWithDeletedCheck(ConditionBean cb) {
        return selectEntityWithDeletedCheck(downcast(cb));
    }

    /**
     * Select the entity by the primary-key value.
     * @param productId The one of primary key. (NotNull)
     * @param optionId The one of primary key. (NotNull)
     * @return The entity selected by the PK. (NullAllowed: if no data, it returns null)
     * @exception org.seasar.dbflute.exception.EntityDuplicatedException When the entity has been duplicated.
     * @exception org.seasar.dbflute.exception.SelectEntityConditionNotFoundException When the condition for selecting an entity is not found.
     */
    public ProductsOptions selectByPKValue(Integer productId, Integer optionId) {
        return doSelectByPKValue(productId, optionId, ProductsOptions.class);
    }

    protected <ENTITY extends ProductsOptions> ENTITY doSelectByPKValue(Integer productId, Integer optionId, Class<ENTITY> entityType) {
        return doSelectEntity(buildPKCB(productId, optionId), entityType);
    }

    /**
     * Select the entity by the primary-key value with deleted check.
     * @param productId The one of primary key. (NotNull)
     * @param optionId The one of primary key. (NotNull)
     * @return The entity selected by the PK. (NotNull: if no data, throws exception)
     * @exception org.seasar.dbflute.exception.EntityAlreadyDeletedException When the entity has already been deleted. (not found)
     * @exception org.seasar.dbflute.exception.EntityDuplicatedException When the entity has been duplicated.
     * @exception org.seasar.dbflute.exception.SelectEntityConditionNotFoundException When the condition for selecting an entity is not found.
     */
    public ProductsOptions selectByPKValueWithDeletedCheck(Integer productId, Integer optionId) {
        return doSelectByPKValueWithDeletedCheck(productId, optionId, ProductsOptions.class);
    }

    protected <ENTITY extends ProductsOptions> ENTITY doSelectByPKValueWithDeletedCheck(Integer productId, Integer optionId, Class<ENTITY> entityType) {
        return doSelectEntityWithDeletedCheck(buildPKCB(productId, optionId), entityType);
    }

    private ProductsOptionsCB buildPKCB(Integer productId, Integer optionId) {
        assertObjectNotNull("productId", productId);assertObjectNotNull("optionId", optionId);
        ProductsOptionsCB cb = newMyConditionBean();
        cb.query().setProductId_Equal(productId);cb.query().setOptionId_Equal(optionId);
        return cb;
    }

    // ===================================================================================
    //                                                                         List Select
    //                                                                         ===========
    /**
     * Select the list as result bean.
     * <pre>
     * ProductsOptionsCB cb = new ProductsOptionsCB();
     * cb.query().setFoo...(value);
     * cb.query().addOrderBy_Bar...();
     * ListResultBean&lt;ProductsOptions&gt; productsOptionsList = productsOptionsBhv.<span style="color: #FD4747">selectList</span>(cb);
     * for (ProductsOptions productsOptions : productsOptionsList) {
     *     ... = productsOptions.get...();
     * }
     * </pre>
     * @param cb The condition-bean of ProductsOptions. (NotNull)
     * @return The result bean of selected list. (NotNull: if no data, returns empty list)
     * @exception org.seasar.dbflute.exception.DangerousResultSizeException When the result size is over the specified safety size.
     */
    public ListResultBean<ProductsOptions> selectList(ProductsOptionsCB cb) {
        return doSelectList(cb, ProductsOptions.class);
    }

    protected <ENTITY extends ProductsOptions> ListResultBean<ENTITY> doSelectList(ProductsOptionsCB cb, Class<ENTITY> entityType) {
        assertCBStateValid(cb); assertObjectNotNull("entityType", entityType);
        assertSpecifyDerivedReferrerEntityProperty(cb, entityType);
        return helpSelectListInternally(cb, entityType, new InternalSelectListCallback<ENTITY, ProductsOptionsCB>() {
            public List<ENTITY> callbackSelectList(ProductsOptionsCB cb, Class<ENTITY> entityType) { return delegateSelectList(cb, entityType); } });
    }

    @Override
    protected ListResultBean<? extends Entity> doReadList(ConditionBean cb) {
        return selectList(downcast(cb));
    }

    // ===================================================================================
    //                                                                         Page Select
    //                                                                         ===========
    /**
     * Select the page as result bean. <br />
     * (both count-select and paging-select are executed)
     * <pre>
     * ProductsOptionsCB cb = new ProductsOptionsCB();
     * cb.query().setFoo...(value);
     * cb.query().addOrderBy_Bar...();
     * cb.<span style="color: #FD4747">paging</span>(20, 3); <span style="color: #3F7E5E">// 20 records per a page and current page number is 3</span>
     * PagingResultBean&lt;ProductsOptions&gt; page = productsOptionsBhv.<span style="color: #FD4747">selectPage</span>(cb);
     * int allRecordCount = page.getAllRecordCount();
     * int allPageCount = page.getAllPageCount();
     * boolean isExistPrePage = page.isExistPrePage();
     * boolean isExistNextPage = page.isExistNextPage();
     * ...
     * for (ProductsOptions productsOptions : page) {
     *     ... = productsOptions.get...();
     * }
     * </pre>
     * @param cb The condition-bean of ProductsOptions. (NotNull)
     * @return The result bean of selected page. (NotNull: if no data, returns bean as empty list)
     * @exception org.seasar.dbflute.exception.DangerousResultSizeException When the result size is over the specified safety size.
     */
    public PagingResultBean<ProductsOptions> selectPage(ProductsOptionsCB cb) {
        return doSelectPage(cb, ProductsOptions.class);
    }

    protected <ENTITY extends ProductsOptions> PagingResultBean<ENTITY> doSelectPage(ProductsOptionsCB cb, Class<ENTITY> entityType) {
        assertCBStateValid(cb); assertObjectNotNull("entityType", entityType);
        return helpSelectPageInternally(cb, entityType, new InternalSelectPageCallback<ENTITY, ProductsOptionsCB>() {
            public int callbackSelectCount(ProductsOptionsCB cb) { return doSelectCountPlainly(cb); }
            public List<ENTITY> callbackSelectList(ProductsOptionsCB cb, Class<ENTITY> entityType) { return doSelectList(cb, entityType); }
        });
    }

    @Override
    protected PagingResultBean<? extends Entity> doReadPage(ConditionBean cb) {
        return selectPage(downcast(cb));
    }

    // ===================================================================================
    //                                                                       Cursor Select
    //                                                                       =============
    /**
     * Select the cursor by the condition-bean.
     * <pre>
     * ProductsOptionsCB cb = new ProductsOptionsCB();
     * cb.query().setFoo...(value);
     * productsOptionsBhv.<span style="color: #FD4747">selectCursor</span>(cb, new EntityRowHandler&lt;ProductsOptions&gt;() {
     *     public void handle(ProductsOptions entity) {
     *         ... = entity.getFoo...();
     *     }
     * });
     * </pre>
     * @param cb The condition-bean of ProductsOptions. (NotNull)
     * @param entityRowHandler The handler of entity row of ProductsOptions. (NotNull)
     */
    public void selectCursor(ProductsOptionsCB cb, EntityRowHandler<ProductsOptions> entityRowHandler) {
        doSelectCursor(cb, entityRowHandler, ProductsOptions.class);
    }

    protected <ENTITY extends ProductsOptions> void doSelectCursor(ProductsOptionsCB cb, EntityRowHandler<ENTITY> entityRowHandler, Class<ENTITY> entityType) {
        assertCBStateValid(cb); assertObjectNotNull("entityRowHandler<ProductsOptions>", entityRowHandler); assertObjectNotNull("entityType", entityType);
        assertSpecifyDerivedReferrerEntityProperty(cb, entityType);
        helpSelectCursorInternally(cb, entityRowHandler, entityType, new InternalSelectCursorCallback<ENTITY, ProductsOptionsCB>() {
            public void callbackSelectCursor(ProductsOptionsCB cb, EntityRowHandler<ENTITY> entityRowHandler, Class<ENTITY> entityType) { delegateSelectCursor(cb, entityRowHandler, entityType); }
            public List<ENTITY> callbackSelectList(ProductsOptionsCB cb, Class<ENTITY> entityType) { return doSelectList(cb, entityType); }
        });
    }

    // ===================================================================================
    //                                                                       Scalar Select
    //                                                                       =============
    /**
     * Select the scalar value derived by a function from uniquely-selected records. <br />
     * You should call a function method after this method called like as follows:
     * <pre>
     * productsOptionsBhv.<span style="color: #FD4747">scalarSelect</span>(Date.class).max(new ScalarQuery() {
     *     public void query(ProductsOptionsCB cb) {
     *         cb.specify().<span style="color: #FD4747">columnFooDatetime()</span>; <span style="color: #3F7E5E">// required for a function</span>
     *         cb.query().setBarName_PrefixSearch("S");
     *     }
     * });
     * </pre>
     * @param <RESULT> The type of result.
     * @param resultType The type of result. (NotNull)
     * @return The scalar value derived by a function. (NullAllowed)
     */
    public <RESULT> SLFunction<ProductsOptionsCB, RESULT> scalarSelect(Class<RESULT> resultType) {
        return doScalarSelect(resultType, newMyConditionBean());
    }

    protected <RESULT, CB extends ProductsOptionsCB> SLFunction<CB, RESULT> doScalarSelect(Class<RESULT> resultType, CB cb) {
        assertObjectNotNull("resultType", resultType); assertCBStateValid(cb);
        cb.xsetupForScalarSelect(); cb.getSqlClause().disableSelectIndex(); // for when you use union
        return new SLFunction<CB, RESULT>(cb, resultType);
    }

    // ===================================================================================
    //                                                                            Sequence
    //                                                                            ========
    @Override
    protected Number doReadNextVal() {
        String msg = "This table is NOT related to sequence: " + getTableDbName();
        throw new UnsupportedOperationException(msg);
    }

    // ===================================================================================
    //                                                                   Pull out Relation
    //                                                                   =================

    // ===================================================================================
    //                                                                      Extract Column
    //                                                                      ==============

    // ===================================================================================
    //                                                                       Entity Update
    //                                                                       =============
    /**
     * Insert the entity modified-only. (DefaultConstraintsEnabled)
     * <pre>
     * ProductsOptions productsOptions = new ProductsOptions();
     * <span style="color: #3F7E5E">// if auto-increment, you don't need to set the PK value</span>
     * productsOptions.setFoo...(value);
     * productsOptions.setBar...(value);
     * <span style="color: #3F7E5E">// you don't need to set values of common columns</span>
     * <span style="color: #3F7E5E">//productsOptions.setRegisterUser(value);</span>
     * <span style="color: #3F7E5E">//productsOptions.set...;</span>
     * productsOptionsBhv.<span style="color: #FD4747">insert</span>(productsOptions);
     * ... = productsOptions.getPK...(); <span style="color: #3F7E5E">// if auto-increment, you can get the value after</span>
     * </pre>
     * <p>While, when the entity is created by select, all columns are registered.</p>
     * @param productsOptions The entity of insert target. (NotNull, PrimaryKeyNullAllowed: when auto-increment)
     * @exception org.seasar.dbflute.exception.EntityAlreadyExistsException When the entity already exists. (unique constraint violation)
     */
    public void insert(ProductsOptions productsOptions) {
        doInsert(productsOptions, null);
    }

    protected void doInsert(ProductsOptions productsOptions, InsertOption<ProductsOptionsCB> option) {
        assertObjectNotNull("productsOptions", productsOptions);
        prepareInsertOption(option);
        delegateInsert(productsOptions, option);
    }

    protected void prepareInsertOption(InsertOption<ProductsOptionsCB> option) {
        if (option == null) { return; }
        assertInsertOptionStatus(option);
        if (option.hasSpecifiedInsertColumn()) {
            option.resolveInsertColumnSpecification(createCBForSpecifiedUpdate());
        }
    }

    @Override
    protected void doCreate(Entity entity, InsertOption<? extends ConditionBean> option) {
        if (option == null) { insert(downcast(entity)); }
        else { varyingInsert(downcast(entity), downcast(option)); }
    }

    /**
     * Update the entity modified-only. (ZeroUpdateException, NonExclusiveControl)
     * <pre>
     * ProductsOptions productsOptions = new ProductsOptions();
     * productsOptions.setPK...(value); <span style="color: #3F7E5E">// required</span>
     * productsOptions.setFoo...(value); <span style="color: #3F7E5E">// you should set only modified columns</span>
     * <span style="color: #3F7E5E">// you don't need to set values of common columns</span>
     * <span style="color: #3F7E5E">//productsOptions.setRegisterUser(value);</span>
     * <span style="color: #3F7E5E">//productsOptions.set...;</span>
     * <span style="color: #3F7E5E">// if exclusive control, the value of exclusive control column is required</span>
     * productsOptions.<span style="color: #FD4747">setVersionNo</span>(value);
     * try {
     *     productsOptionsBhv.<span style="color: #FD4747">update</span>(productsOptions);
     * } catch (EntityAlreadyUpdatedException e) { <span style="color: #3F7E5E">// if concurrent update</span>
     *     ...
     * } 
     * </pre>
     * @param productsOptions The entity of update target. (NotNull, PrimaryKeyNotNull, ConcurrencyColumnRequired)
     * @exception org.seasar.dbflute.exception.EntityAlreadyDeletedException When the entity has already been deleted. (not found)
     * @exception org.seasar.dbflute.exception.EntityDuplicatedException When the entity has been duplicated.
     * @exception org.seasar.dbflute.exception.EntityAlreadyExistsException When the entity already exists. (unique constraint violation)
     */
    public void update(final ProductsOptions productsOptions) {
        doUpdate(productsOptions, null);
    }

    protected void doUpdate(ProductsOptions productsOptions, final UpdateOption<ProductsOptionsCB> option) {
        assertObjectNotNull("productsOptions", productsOptions);
        prepareUpdateOption(option);
        helpUpdateInternally(productsOptions, new InternalUpdateCallback<ProductsOptions>() {
            public int callbackDelegateUpdate(ProductsOptions entity) { return delegateUpdate(entity, option); } });
    }

    protected void prepareUpdateOption(UpdateOption<ProductsOptionsCB> option) {
        if (option == null) { return; }
        assertUpdateOptionStatus(option);
        if (option.hasSelfSpecification()) {
            option.resolveSelfSpecification(createCBForVaryingUpdate());
        }
        if (option.hasSpecifiedUpdateColumn()) {
            option.resolveUpdateColumnSpecification(createCBForSpecifiedUpdate());
        }
    }

    protected ProductsOptionsCB createCBForVaryingUpdate() {
        ProductsOptionsCB cb = newMyConditionBean();
        cb.xsetupForVaryingUpdate();
        return cb;
    }

    protected ProductsOptionsCB createCBForSpecifiedUpdate() {
        ProductsOptionsCB cb = newMyConditionBean();
        cb.xsetupForSpecifiedUpdate();
        return cb;
    }

    @Override
    protected void doModify(Entity entity, UpdateOption<? extends ConditionBean> option) {
        if (option == null) { update(downcast(entity)); }
        else { varyingUpdate(downcast(entity), downcast(option)); }
    }

    @Override
    protected void doModifyNonstrict(Entity entity, UpdateOption<? extends ConditionBean> option) {
        doModify(entity, option);
    }

    /**
     * Insert or update the entity modified-only. (DefaultConstraintsEnabled, NonExclusiveControl) <br />
     * if (the entity has no PK) { insert() } else { update(), but no data, insert() } <br />
     * <p><span style="color: #FD4747; font-size: 120%">Attention, you cannot update by unique keys instead of PK.</span></p>
     * @param productsOptions The entity of insert or update target. (NotNull)
     * @exception org.seasar.dbflute.exception.EntityAlreadyDeletedException When the entity has already been deleted. (not found)
     * @exception org.seasar.dbflute.exception.EntityDuplicatedException When the entity has been duplicated.
     * @exception org.seasar.dbflute.exception.EntityAlreadyExistsException When the entity already exists. (unique constraint violation)
     */
    public void insertOrUpdate(ProductsOptions productsOptions) {
        doInesrtOrUpdate(productsOptions, null, null);
    }

    protected void doInesrtOrUpdate(ProductsOptions productsOptions, final InsertOption<ProductsOptionsCB> insertOption, final UpdateOption<ProductsOptionsCB> updateOption) {
        helpInsertOrUpdateInternally(productsOptions, new InternalInsertOrUpdateCallback<ProductsOptions, ProductsOptionsCB>() {
            public void callbackInsert(ProductsOptions entity) { doInsert(entity, insertOption); }
            public void callbackUpdate(ProductsOptions entity) { doUpdate(entity, updateOption); }
            public ProductsOptionsCB callbackNewMyConditionBean() { return newMyConditionBean(); }
            public int callbackSelectCount(ProductsOptionsCB cb) { return selectCount(cb); }
        });
    }

    @Override
    protected void doCreateOrModify(Entity entity, InsertOption<? extends ConditionBean> insertOption,
            UpdateOption<? extends ConditionBean> updateOption) {
        if (insertOption == null && updateOption == null) { insertOrUpdate(downcast(entity)); }
        else {
            insertOption = insertOption == null ? new InsertOption<ProductsOptionsCB>() : insertOption;
            updateOption = updateOption == null ? new UpdateOption<ProductsOptionsCB>() : updateOption;
            varyingInsertOrUpdate(downcast(entity), downcast(insertOption), downcast(updateOption));
        }
    }

    @Override
    protected void doCreateOrModifyNonstrict(Entity entity, InsertOption<? extends ConditionBean> insertOption,
            UpdateOption<? extends ConditionBean> updateOption) {
        doCreateOrModify(entity, insertOption, updateOption);
    }

    /**
     * Delete the entity. (ZeroUpdateException, NonExclusiveControl)
     * <pre>
     * ProductsOptions productsOptions = new ProductsOptions();
     * productsOptions.setPK...(value); <span style="color: #3F7E5E">// required</span>
     * <span style="color: #3F7E5E">// if exclusive control, the value of exclusive control column is required</span>
     * productsOptions.<span style="color: #FD4747">setVersionNo</span>(value);
     * try {
     *     productsOptionsBhv.<span style="color: #FD4747">delete</span>(productsOptions);
     * } catch (EntityAlreadyUpdatedException e) { <span style="color: #3F7E5E">// if concurrent update</span>
     *     ...
     * } 
     * </pre>
     * @param productsOptions The entity of delete target. (NotNull, PrimaryKeyNotNull, ConcurrencyColumnRequired)
     * @exception org.seasar.dbflute.exception.EntityAlreadyDeletedException When the entity has already been deleted. (not found)
     * @exception org.seasar.dbflute.exception.EntityDuplicatedException When the entity has been duplicated.
     */
    public void delete(ProductsOptions productsOptions) {
        doDelete(productsOptions, null);
    }

    protected void doDelete(ProductsOptions productsOptions, final DeleteOption<ProductsOptionsCB> option) {
        assertObjectNotNull("productsOptions", productsOptions);
        prepareDeleteOption(option);
        helpDeleteInternally(productsOptions, new InternalDeleteCallback<ProductsOptions>() {
            public int callbackDelegateDelete(ProductsOptions entity) { return delegateDelete(entity, option); } });
    }

    protected void prepareDeleteOption(DeleteOption<ProductsOptionsCB> option) {
        if (option == null) { return; }
        assertDeleteOptionStatus(option);
    }

    @Override
    protected void doRemove(Entity entity, DeleteOption<? extends ConditionBean> option) {
        if (option == null) { delete(downcast(entity)); }
        else { varyingDelete(downcast(entity), downcast(option)); }
    }

    @Override
    protected void doRemoveNonstrict(Entity entity, DeleteOption<? extends ConditionBean> option) {
        doRemove(entity, option);
    }

    // ===================================================================================
    //                                                                        Batch Update
    //                                                                        ============
    /**
     * Batch-insert the entity list modified-only of same-set columns. (DefaultConstraintsEnabled) <br />
     * This method uses executeBatch() of java.sql.PreparedStatement. <br />
     * <p><span style="color: #FD4747; font-size: 120%">The columns of least common multiple are registered like this:</span></p>
     * <pre>
     * for (... : ...) {
     *     ProductsOptions productsOptions = new ProductsOptions();
     *     productsOptions.setFooName("foo");
     *     if (...) {
     *         productsOptions.setFooPrice(123);
     *     }
     *     <span style="color: #3F7E5E">// FOO_NAME and FOO_PRICE (and record meta columns) are registered</span>
     *     <span style="color: #3F7E5E">// FOO_PRICE not-called in any entities are registered as null without default value</span>
     *     <span style="color: #3F7E5E">// columns not-called in all entities are registered as null or default value</span>
     *     productsOptionsList.add(productsOptions);
     * }
     * productsOptionsBhv.<span style="color: #FD4747">batchInsert</span>(productsOptionsList);
     * </pre>
     * <p>While, when the entities are created by select, all columns are registered.</p>
     * <p>And if the table has an identity, entities after the process don't have incremented values.
     * (When you use the (normal) insert(), you can get the incremented value from your entity)</p>
     * @param productsOptionsList The list of the entity. (NotNull, EmptyAllowed, PrimaryKeyNullAllowed: when auto-increment)
     * @return The array of inserted count. (NotNull, EmptyAllowed)
     */
    public int[] batchInsert(List<ProductsOptions> productsOptionsList) {
        InsertOption<ProductsOptionsCB> option = createInsertUpdateOption();
        return doBatchInsert(productsOptionsList, option);
    }

    protected int[] doBatchInsert(List<ProductsOptions> productsOptionsList, InsertOption<ProductsOptionsCB> option) {
        assertObjectNotNull("productsOptionsList", productsOptionsList);
        prepareBatchInsertOption(productsOptionsList, option);
        return delegateBatchInsert(productsOptionsList, option);
    }

    protected void prepareBatchInsertOption(List<ProductsOptions> productsOptionsList, InsertOption<ProductsOptionsCB> option) {
        option.xallowInsertColumnModifiedPropertiesFragmented();
        option.xacceptInsertColumnModifiedPropertiesIfNeeds(productsOptionsList);
        prepareInsertOption(option);
    }

    @Override
    protected int[] doLumpCreate(List<Entity> ls, InsertOption<? extends ConditionBean> option) {
        if (option == null) { return batchInsert(downcast(ls)); }
        else { return varyingBatchInsert(downcast(ls), downcast(option)); }
    }

    /**
     * Batch-update the entity list modified-only of same-set columns. (NonExclusiveControl) <br />
     * This method uses executeBatch() of java.sql.PreparedStatement. <br />
     * <span style="color: #FD4747; font-size: 120%">You should specify same-set columns to all entities like this:</span>
     * <pre>
     * for (... : ...) {
     *     ProductsOptions productsOptions = new ProductsOptions();
     *     productsOptions.setFooName("foo");
     *     if (...) {
     *         productsOptions.setFooPrice(123);
     *     } else {
     *         productsOptions.setFooPrice(null); <span style="color: #3F7E5E">// updated as null</span>
     *         <span style="color: #3F7E5E">//productsOptions.setFooDate(...); // *not allowed, fragmented</span>
     *     }
     *     <span style="color: #3F7E5E">// FOO_NAME and FOO_PRICE (and record meta columns) are updated</span>
     *     <span style="color: #3F7E5E">// (others are not updated: their values are kept)</span>
     *     productsOptionsList.add(productsOptions);
     * }
     * productsOptionsBhv.<span style="color: #FD4747">batchUpdate</span>(productsOptionsList);
     * </pre>
     * @param productsOptionsList The list of the entity. (NotNull, EmptyAllowed, PrimaryKeyNotNull)
     * @return The array of updated count. (NotNull, EmptyAllowed)
     * @exception org.seasar.dbflute.exception.EntityAlreadyDeletedException When the entity has already been deleted. (not found)
     */
    public int[] batchUpdate(List<ProductsOptions> productsOptionsList) {
        UpdateOption<ProductsOptionsCB> option = createPlainUpdateOption();
        return doBatchUpdate(productsOptionsList, option);
    }

    protected int[] doBatchUpdate(List<ProductsOptions> productsOptionsList, UpdateOption<ProductsOptionsCB> option) {
        assertObjectNotNull("productsOptionsList", productsOptionsList);
        prepareBatchUpdateOption(productsOptionsList, option);
        return delegateBatchUpdate(productsOptionsList, option);
    }

    protected void prepareBatchUpdateOption(List<ProductsOptions> productsOptionsList, UpdateOption<ProductsOptionsCB> option) {
        option.xacceptUpdateColumnModifiedPropertiesIfNeeds(productsOptionsList);
        prepareUpdateOption(option);
    }

    @Override
    protected int[] doLumpModify(List<Entity> ls, UpdateOption<? extends ConditionBean> option) {
        if (option == null) { return batchUpdate(downcast(ls)); }
        else { return varyingBatchUpdate(downcast(ls), downcast(option)); }
    }

    /**
     * Batch-update the entity list specified-only. (NonExclusiveControl) <br />
     * This method uses executeBatch() of java.sql.PreparedStatement.
     * <pre>
     * <span style="color: #3F7E5E">// e.g. update two columns only</span> 
     * productsOptionsBhv.<span style="color: #FD4747">batchUpdate</span>(productsOptionsList, new SpecifyQuery<ProductsOptionsCB>() {
     *     public void specify(ProductsOptionsCB cb) { <span style="color: #3F7E5E">// the two only updated</span>
     *         cb.specify().<span style="color: #FD4747">columnFooStatusCode()</span>; <span style="color: #3F7E5E">// should be modified in any entities</span>
     *         cb.specify().<span style="color: #FD4747">columnBarDate()</span>; <span style="color: #3F7E5E">// should be modified in any entities</span>
     *     }
     * });
     * <span style="color: #3F7E5E">// e.g. update every column in the table</span> 
     * productsOptionsBhv.<span style="color: #FD4747">batchUpdate</span>(productsOptionsList, new SpecifyQuery<ProductsOptionsCB>() {
     *     public void specify(ProductsOptionsCB cb) { <span style="color: #3F7E5E">// all columns are updated</span>
     *         cb.specify().<span style="color: #FD4747">columnEveryColumn()</span>; <span style="color: #3F7E5E">// no check of modified properties</span>
     *     }
     * });
     * </pre>
     * <p>You can specify update columns used on set clause of update statement.
     * However you do not need to specify common columns for update
     * and an optimistic lock column because they are specified implicitly.</p>
     * <p>And you should specify columns that are modified in any entities (at least one entity).
     * But if you specify every column, it has no check.</p>
     * @param productsOptionsList The list of the entity. (NotNull, EmptyAllowed, PrimaryKeyNotNull)
     * @param updateColumnSpec The specification of update columns. (NotNull)
     * @return The array of updated count. (NotNull, EmptyAllowed)
     * @exception org.seasar.dbflute.exception.EntityAlreadyDeletedException When the entity has already been deleted. (not found)
     */
    public int[] batchUpdate(List<ProductsOptions> productsOptionsList, SpecifyQuery<ProductsOptionsCB> updateColumnSpec) {
        return doBatchUpdate(productsOptionsList, createSpecifiedUpdateOption(updateColumnSpec));
    }

    @Override
    protected int[] doLumpModifyNonstrict(List<Entity> ls, UpdateOption<? extends ConditionBean> option) {
        return doLumpModify(ls, option);
    }

    /**
     * Batch-delete the entity list. (NonExclusiveControl) <br />
     * This method uses executeBatch() of java.sql.PreparedStatement.
     * @param productsOptionsList The list of the entity. (NotNull, EmptyAllowed, PrimaryKeyNotNull)
     * @return The array of deleted count. (NotNull, EmptyAllowed)
     * @exception org.seasar.dbflute.exception.EntityAlreadyDeletedException When the entity has already been deleted. (not found)
     */
    public int[] batchDelete(List<ProductsOptions> productsOptionsList) {
        return doBatchDelete(productsOptionsList, null);
    }

    protected int[] doBatchDelete(List<ProductsOptions> productsOptionsList, DeleteOption<ProductsOptionsCB> option) {
        assertObjectNotNull("productsOptionsList", productsOptionsList);
        prepareDeleteOption(option);
        return delegateBatchDelete(productsOptionsList, option);
    }

    @Override
    protected int[] doLumpRemove(List<Entity> ls, DeleteOption<? extends ConditionBean> option) {
        if (option == null) { return batchDelete(downcast(ls)); }
        else { return varyingBatchDelete(downcast(ls), downcast(option)); }
    }

    @Override
    protected int[] doLumpRemoveNonstrict(List<Entity> ls, DeleteOption<? extends ConditionBean> option) {
        return doLumpRemove(ls, option);
    }

    // ===================================================================================
    //                                                                        Query Update
    //                                                                        ============
    /**
     * Insert the several entities by query (modified-only for fixed value).
     * <pre>
     * productsOptionsBhv.<span style="color: #FD4747">queryInsert</span>(new QueryInsertSetupper&lt;ProductsOptions, ProductsOptionsCB&gt;() {
     *     public ConditionBean setup(productsOptions entity, ProductsOptionsCB intoCB) {
     *         FooCB cb = FooCB();
     *         cb.setupSelect_Bar();
     * 
     *         <span style="color: #3F7E5E">// mapping</span>
     *         intoCB.specify().columnMyName().mappedFrom(cb.specify().columnFooName());
     *         intoCB.specify().columnMyCount().mappedFrom(cb.specify().columnFooCount());
     *         intoCB.specify().columnMyDate().mappedFrom(cb.specify().specifyBar().columnBarDate());
     *         entity.setMyFixedValue("foo"); <span style="color: #3F7E5E">// fixed value</span>
     *         <span style="color: #3F7E5E">// you don't need to set values of common columns</span>
     *         <span style="color: #3F7E5E">//entity.setRegisterUser(value);</span>
     *         <span style="color: #3F7E5E">//entity.set...;</span>
     *         <span style="color: #3F7E5E">// you don't need to set a value of exclusive control column</span>
     *         <span style="color: #3F7E5E">//entity.setVersionNo(value);</span>
     * 
     *         return cb;
     *     }
     * });
     * </pre>
     * @param setupper The setup-per of query-insert. (NotNull)
     * @return The inserted count.
     */
    public int queryInsert(QueryInsertSetupper<ProductsOptions, ProductsOptionsCB> setupper) {
        return doQueryInsert(setupper, null);
    }

    protected int doQueryInsert(QueryInsertSetupper<ProductsOptions, ProductsOptionsCB> setupper, InsertOption<ProductsOptionsCB> option) {
        assertObjectNotNull("setupper", setupper);
        prepareInsertOption(option);
        ProductsOptions entity = new ProductsOptions();
        ProductsOptionsCB intoCB = createCBForQueryInsert();
        ConditionBean resourceCB = setupper.setup(entity, intoCB);
        return delegateQueryInsert(entity, intoCB, resourceCB, option);
    }

    protected ProductsOptionsCB createCBForQueryInsert() {
        ProductsOptionsCB cb = newMyConditionBean();
        cb.xsetupForQueryInsert();
        return cb;
    }

    @Override
    protected int doRangeCreate(QueryInsertSetupper<? extends Entity, ? extends ConditionBean> setupper, InsertOption<? extends ConditionBean> option) {
        if (option == null) { return queryInsert(downcast(setupper)); }
        else { return varyingQueryInsert(downcast(setupper), downcast(option)); }
    }

    /**
     * Update the several entities by query non-strictly modified-only. (NonExclusiveControl)
     * <pre>
     * ProductsOptions productsOptions = new ProductsOptions();
     * <span style="color: #3F7E5E">// you don't need to set PK value</span>
     * <span style="color: #3F7E5E">//productsOptions.setPK...(value);</span>
     * productsOptions.setFoo...(value); <span style="color: #3F7E5E">// you should set only modified columns</span>
     * <span style="color: #3F7E5E">// you don't need to set values of common columns</span>
     * <span style="color: #3F7E5E">//productsOptions.setRegisterUser(value);</span>
     * <span style="color: #3F7E5E">//productsOptions.set...;</span>
     * <span style="color: #3F7E5E">// you don't need to set a value of exclusive control column</span>
     * <span style="color: #3F7E5E">// (auto-increment for version number is valid though non-exclusive control)</span>
     * <span style="color: #3F7E5E">//productsOptions.setVersionNo(value);</span>
     * ProductsOptionsCB cb = new ProductsOptionsCB();
     * cb.query().setFoo...(value);
     * productsOptionsBhv.<span style="color: #FD4747">queryUpdate</span>(productsOptions, cb);
     * </pre>
     * @param productsOptions The entity that contains update values. (NotNull, PrimaryKeyNullAllowed)
     * @param cb The condition-bean of ProductsOptions. (NotNull)
     * @return The updated count.
     * @exception org.seasar.dbflute.exception.NonQueryUpdateNotAllowedException When the query has no condition.
     */
    public int queryUpdate(ProductsOptions productsOptions, ProductsOptionsCB cb) {
        return doQueryUpdate(productsOptions, cb, null);
    }

    protected int doQueryUpdate(ProductsOptions productsOptions, ProductsOptionsCB cb, UpdateOption<ProductsOptionsCB> option) {
        assertObjectNotNull("productsOptions", productsOptions); assertCBStateValid(cb);
        prepareUpdateOption(option);
        return checkCountBeforeQueryUpdateIfNeeds(cb) ? delegateQueryUpdate(productsOptions, cb, option) : 0;
    }

    @Override
    protected int doRangeModify(Entity entity, ConditionBean cb, UpdateOption<? extends ConditionBean> option) {
        if (option == null) { return queryUpdate(downcast(entity), (ProductsOptionsCB)cb); }
        else { return varyingQueryUpdate(downcast(entity), (ProductsOptionsCB)cb, downcast(option)); }
    }

    /**
     * Delete the several entities by query. (NonExclusiveControl)
     * <pre>
     * ProductsOptionsCB cb = new ProductsOptionsCB();
     * cb.query().setFoo...(value);
     * productsOptionsBhv.<span style="color: #FD4747">queryDelete</span>(productsOptions, cb);
     * </pre>
     * @param cb The condition-bean of ProductsOptions. (NotNull)
     * @return The deleted count.
     * @exception org.seasar.dbflute.exception.NonQueryDeleteNotAllowedException When the query has no condition.
     */
    public int queryDelete(ProductsOptionsCB cb) {
        return doQueryDelete(cb, null);
    }

    protected int doQueryDelete(ProductsOptionsCB cb, DeleteOption<ProductsOptionsCB> option) {
        assertCBStateValid(cb);
        prepareDeleteOption(option);
        return checkCountBeforeQueryUpdateIfNeeds(cb) ? delegateQueryDelete(cb, option) : 0;
    }

    @Override
    protected int doRangeRemove(ConditionBean cb, DeleteOption<? extends ConditionBean> option) {
        if (option == null) { return queryDelete((ProductsOptionsCB)cb); }
        else { return varyingQueryDelete((ProductsOptionsCB)cb, downcast(option)); }
    }

    // ===================================================================================
    //                                                                      Varying Update
    //                                                                      ==============
    // -----------------------------------------------------
    //                                         Entity Update
    //                                         -------------
    /**
     * Insert the entity with varying requests. <br />
     * For example, disableCommonColumnAutoSetup(), disablePrimaryKeyIdentity(). <br />
     * Other specifications are same as insert(entity).
     * <pre>
     * ProductsOptions productsOptions = new ProductsOptions();
     * <span style="color: #3F7E5E">// if auto-increment, you don't need to set the PK value</span>
     * productsOptions.setFoo...(value);
     * productsOptions.setBar...(value);
     * InsertOption<ProductsOptionsCB> option = new InsertOption<ProductsOptionsCB>();
     * <span style="color: #3F7E5E">// you can insert by your values for common columns</span>
     * option.disableCommonColumnAutoSetup();
     * productsOptionsBhv.<span style="color: #FD4747">varyingInsert</span>(productsOptions, option);
     * ... = productsOptions.getPK...(); <span style="color: #3F7E5E">// if auto-increment, you can get the value after</span>
     * </pre>
     * @param productsOptions The entity of insert target. (NotNull, PrimaryKeyNullAllowed: when auto-increment)
     * @param option The option of insert for varying requests. (NotNull)
     * @exception org.seasar.dbflute.exception.EntityAlreadyExistsException When the entity already exists. (unique constraint violation)
     */
    public void varyingInsert(ProductsOptions productsOptions, InsertOption<ProductsOptionsCB> option) {
        assertInsertOptionNotNull(option);
        doInsert(productsOptions, option);
    }

    /**
     * Update the entity with varying requests modified-only. (ZeroUpdateException, NonExclusiveControl) <br />
     * For example, self(selfCalculationSpecification), specify(updateColumnSpecification), disableCommonColumnAutoSetup(). <br />
     * Other specifications are same as update(entity).
     * <pre>
     * ProductsOptions productsOptions = new ProductsOptions();
     * productsOptions.setPK...(value); <span style="color: #3F7E5E">// required</span>
     * productsOptions.setOther...(value); <span style="color: #3F7E5E">// you should set only modified columns</span>
     * <span style="color: #3F7E5E">// if exclusive control, the value of exclusive control column is required</span>
     * productsOptions.<span style="color: #FD4747">setVersionNo</span>(value);
     * try {
     *     <span style="color: #3F7E5E">// you can update by self calculation values</span>
     *     UpdateOption&lt;ProductsOptionsCB&gt; option = new UpdateOption&lt;ProductsOptionsCB&gt;();
     *     option.self(new SpecifyQuery&lt;ProductsOptionsCB&gt;() {
     *         public void specify(ProductsOptionsCB cb) {
     *             cb.specify().<span style="color: #FD4747">columnXxxCount()</span>;
     *         }
     *     }).plus(1); <span style="color: #3F7E5E">// XXX_COUNT = XXX_COUNT + 1</span>
     *     productsOptionsBhv.<span style="color: #FD4747">varyingUpdate</span>(productsOptions, option);
     * } catch (EntityAlreadyUpdatedException e) { <span style="color: #3F7E5E">// if concurrent update</span>
     *     ...
     * }
     * </pre>
     * @param productsOptions The entity of update target. (NotNull, PrimaryKeyNotNull, ConcurrencyColumnRequired)
     * @param option The option of update for varying requests. (NotNull)
     * @exception org.seasar.dbflute.exception.EntityAlreadyDeletedException When the entity has already been deleted. (not found)
     * @exception org.seasar.dbflute.exception.EntityDuplicatedException When the entity has been duplicated.
     * @exception org.seasar.dbflute.exception.EntityAlreadyExistsException When the entity already exists. (unique constraint violation)
     */
    public void varyingUpdate(ProductsOptions productsOptions, UpdateOption<ProductsOptionsCB> option) {
        assertUpdateOptionNotNull(option);
        doUpdate(productsOptions, option);
    }

    /**
     * Insert or update the entity with varying requests. (ExclusiveControl: when update) <br />
     * Other specifications are same as insertOrUpdate(entity).
     * @param productsOptions The entity of insert or update target. (NotNull)
     * @param insertOption The option of insert for varying requests. (NotNull)
     * @param updateOption The option of update for varying requests. (NotNull)
     * @exception org.seasar.dbflute.exception.EntityAlreadyDeletedException When the entity has already been deleted. (not found)
     * @exception org.seasar.dbflute.exception.EntityDuplicatedException When the entity has been duplicated.
     * @exception org.seasar.dbflute.exception.EntityAlreadyExistsException When the entity already exists. (unique constraint violation)
     */
    public void varyingInsertOrUpdate(ProductsOptions productsOptions, InsertOption<ProductsOptionsCB> insertOption, UpdateOption<ProductsOptionsCB> updateOption) {
        assertInsertOptionNotNull(insertOption); assertUpdateOptionNotNull(updateOption);
        doInesrtOrUpdate(productsOptions, insertOption, updateOption);
    }

    /**
     * Delete the entity with varying requests. (ZeroUpdateException, NonExclusiveControl) <br />
     * Now a valid option does not exist. <br />
     * Other specifications are same as delete(entity).
     * @param productsOptions The entity of delete target. (NotNull, PrimaryKeyNotNull, ConcurrencyColumnRequired)
     * @param option The option of update for varying requests. (NotNull)
     * @exception org.seasar.dbflute.exception.EntityAlreadyDeletedException When the entity has already been deleted. (not found)
     * @exception org.seasar.dbflute.exception.EntityDuplicatedException When the entity has been duplicated.
     */
    public void varyingDelete(ProductsOptions productsOptions, DeleteOption<ProductsOptionsCB> option) {
        assertDeleteOptionNotNull(option);
        doDelete(productsOptions, option);
    }

    // -----------------------------------------------------
    //                                          Batch Update
    //                                          ------------
    /**
     * Batch-insert the list with varying requests. <br />
     * For example, disableCommonColumnAutoSetup()
     * , disablePrimaryKeyIdentity(), limitBatchInsertLogging(). <br />
     * Other specifications are same as batchInsert(entityList).
     * @param productsOptionsList The list of the entity. (NotNull, EmptyAllowed, PrimaryKeyNotNull)
     * @param option The option of insert for varying requests. (NotNull)
     * @return The array of updated count. (NotNull, EmptyAllowed)
     */
    public int[] varyingBatchInsert(List<ProductsOptions> productsOptionsList, InsertOption<ProductsOptionsCB> option) {
        assertInsertOptionNotNull(option);
        return doBatchInsert(productsOptionsList, option);
    }

    /**
     * Batch-update the list with varying requests. <br />
     * For example, self(selfCalculationSpecification), specify(updateColumnSpecification)
     * , disableCommonColumnAutoSetup(), limitBatchUpdateLogging(). <br />
     * Other specifications are same as batchUpdate(entityList).
     * @param productsOptionsList The list of the entity. (NotNull, EmptyAllowed, PrimaryKeyNotNull)
     * @param option The option of update for varying requests. (NotNull)
     * @return The array of updated count. (NotNull, EmptyAllowed)
     */
    public int[] varyingBatchUpdate(List<ProductsOptions> productsOptionsList, UpdateOption<ProductsOptionsCB> option) {
        assertUpdateOptionNotNull(option);
        return doBatchUpdate(productsOptionsList, option);
    }

    /**
     * Batch-delete the list with varying requests. <br />
     * For example, limitBatchDeleteLogging(). <br />
     * Other specifications are same as batchDelete(entityList).
     * @param productsOptionsList The list of the entity. (NotNull, EmptyAllowed, PrimaryKeyNotNull)
     * @param option The option of delete for varying requests. (NotNull)
     * @return The array of deleted count. (NotNull, EmptyAllowed)
     */
    public int[] varyingBatchDelete(List<ProductsOptions> productsOptionsList, DeleteOption<ProductsOptionsCB> option) {
        assertDeleteOptionNotNull(option);
        return doBatchDelete(productsOptionsList, option);
    }

    // -----------------------------------------------------
    //                                          Query Update
    //                                          ------------
    /**
     * Insert the several entities by query with varying requests (modified-only for fixed value). <br />
     * For example, disableCommonColumnAutoSetup(), disablePrimaryKeyIdentity(). <br />
     * Other specifications are same as queryInsert(entity, setupper). 
     * @param setupper The setup-per of query-insert. (NotNull)
     * @param option The option of insert for varying requests. (NotNull)
     * @return The inserted count.
     */
    public int varyingQueryInsert(QueryInsertSetupper<ProductsOptions, ProductsOptionsCB> setupper, InsertOption<ProductsOptionsCB> option) {
        assertInsertOptionNotNull(option);
        return doQueryInsert(setupper, option);
    }

    /**
     * Update the several entities by query with varying requests non-strictly modified-only. {NonExclusiveControl} <br />
     * For example, self(selfCalculationSpecification), specify(updateColumnSpecification)
     * , disableCommonColumnAutoSetup(), allowNonQueryUpdate(). <br />
     * Other specifications are same as queryUpdate(entity, cb). 
     * <pre>
     * <span style="color: #3F7E5E">// ex) you can update by self calculation values</span>
     * ProductsOptions productsOptions = new ProductsOptions();
     * <span style="color: #3F7E5E">// you don't need to set PK value</span>
     * <span style="color: #3F7E5E">//productsOptions.setPK...(value);</span>
     * productsOptions.setOther...(value); <span style="color: #3F7E5E">// you should set only modified columns</span>
     * <span style="color: #3F7E5E">// you don't need to set a value of exclusive control column</span>
     * <span style="color: #3F7E5E">// (auto-increment for version number is valid though non-exclusive control)</span>
     * <span style="color: #3F7E5E">//productsOptions.setVersionNo(value);</span>
     * ProductsOptionsCB cb = new ProductsOptionsCB();
     * cb.query().setFoo...(value);
     * UpdateOption&lt;ProductsOptionsCB&gt; option = new UpdateOption&lt;ProductsOptionsCB&gt;();
     * option.self(new SpecifyQuery&lt;ProductsOptionsCB&gt;() {
     *     public void specify(ProductsOptionsCB cb) {
     *         cb.specify().<span style="color: #FD4747">columnFooCount()</span>;
     *     }
     * }).plus(1); <span style="color: #3F7E5E">// FOO_COUNT = FOO_COUNT + 1</span>
     * productsOptionsBhv.<span style="color: #FD4747">varyingQueryUpdate</span>(productsOptions, cb, option);
     * </pre>
     * @param productsOptions The entity that contains update values. (NotNull) {PrimaryKeyNotRequired}
     * @param cb The condition-bean of ProductsOptions. (NotNull)
     * @param option The option of update for varying requests. (NotNull)
     * @return The updated count.
     * @exception org.seasar.dbflute.exception.NonQueryUpdateNotAllowedException When the query has no condition (if not allowed).
     */
    public int varyingQueryUpdate(ProductsOptions productsOptions, ProductsOptionsCB cb, UpdateOption<ProductsOptionsCB> option) {
        assertUpdateOptionNotNull(option);
        return doQueryUpdate(productsOptions, cb, option);
    }

    /**
     * Delete the several entities by query with varying requests non-strictly. <br />
     * For example, allowNonQueryDelete(). <br />
     * Other specifications are same as batchUpdateNonstrict(entityList).
     * @param cb The condition-bean of ProductsOptions. (NotNull)
     * @param option The option of delete for varying requests. (NotNull)
     * @return The deleted count.
     * @exception org.seasar.dbflute.exception.NonQueryDeleteNotAllowedException When the query has no condition (if not allowed).
     */
    public int varyingQueryDelete(ProductsOptionsCB cb, DeleteOption<ProductsOptionsCB> option) {
        assertDeleteOptionNotNull(option);
        return doQueryDelete(cb, option);
    }

    // ===================================================================================
    //                                                                          OutsideSql
    //                                                                          ==========
    /**
     * Prepare the basic executor of outside-SQL to execute it. <br />
     * The invoker of behavior command should be not null when you call this method.
     * <pre>
     * You can use the methods for outside-SQL are as follows:
     * {Basic}
     *   o selectList()
     *   o execute()
     *   o call()
     * 
     * {Entity}
     *   o entityHandling().selectEntity()
     *   o entityHandling().selectEntityWithDeletedCheck()
     * 
     * {Paging}
     *   o autoPaging().selectList()
     *   o autoPaging().selectPage()
     *   o manualPaging().selectList()
     *   o manualPaging().selectPage()
     * 
     * {Cursor}
     *   o cursorHandling().selectCursor()
     * 
     * {Option}
     *   o dynamicBinding().selectList()
     *   o removeBlockComment().selectList()
     *   o removeLineComment().selectList()
     *   o formatSql().selectList()
     * </pre>
     * @return The basic executor of outside-SQL. (NotNull) 
     */
    public OutsideSqlBasicExecutor<ProductsOptionsBhv> outsideSql() {
        return doOutsideSql();
    }

    // ===================================================================================
    //                                                                     Delegate Method
    //                                                                     ===============
    // [Behavior Command]
    // -----------------------------------------------------
    //                                                Select
    //                                                ------
    protected int delegateSelectCountUniquely(ProductsOptionsCB cb) { return invoke(createSelectCountCBCommand(cb, true)); }
    protected int delegateSelectCountPlainly(ProductsOptionsCB cb) { return invoke(createSelectCountCBCommand(cb, false)); }
    protected <ENTITY extends ProductsOptions> void delegateSelectCursor(ProductsOptionsCB cb, EntityRowHandler<ENTITY> erh, Class<ENTITY> et)
    { invoke(createSelectCursorCBCommand(cb, erh, et)); }
    protected <ENTITY extends ProductsOptions> List<ENTITY> delegateSelectList(ProductsOptionsCB cb, Class<ENTITY> et)
    { return invoke(createSelectListCBCommand(cb, et)); }

    // -----------------------------------------------------
    //                                                Update
    //                                                ------
    protected int delegateInsert(ProductsOptions e, InsertOption<ProductsOptionsCB> op)
    { if (!processBeforeInsert(e, op)) { return 0; }
      return invoke(createInsertEntityCommand(e, op)); }
    protected int delegateUpdate(ProductsOptions e, UpdateOption<ProductsOptionsCB> op)
    { if (!processBeforeUpdate(e, op)) { return 0; }
      return delegateUpdateNonstrict(e, op); }
    protected int delegateUpdateNonstrict(ProductsOptions e, UpdateOption<ProductsOptionsCB> op)
    { if (!processBeforeUpdate(e, op)) { return 0; }
      return invoke(createUpdateNonstrictEntityCommand(e, op)); }
    protected int delegateDelete(ProductsOptions e, DeleteOption<ProductsOptionsCB> op)
    { if (!processBeforeDelete(e, op)) { return 0; }
      return delegateDeleteNonstrict(e, op); }
    protected int delegateDeleteNonstrict(ProductsOptions e, DeleteOption<ProductsOptionsCB> op)
    { if (!processBeforeDelete(e, op)) { return 0; }
      return invoke(createDeleteNonstrictEntityCommand(e, op)); }

    protected int[] delegateBatchInsert(List<ProductsOptions> ls, InsertOption<ProductsOptionsCB> op)
    { if (ls.isEmpty()) { return new int[]{}; }
      return invoke(createBatchInsertCommand(processBatchInternally(ls, op), op)); }
    protected int[] delegateBatchUpdate(List<ProductsOptions> ls, UpdateOption<ProductsOptionsCB> op)
    { if (ls.isEmpty()) { return new int[]{}; }
      return delegateBatchUpdateNonstrict(ls, op); }
    protected int[] delegateBatchUpdateNonstrict(List<ProductsOptions> ls, UpdateOption<ProductsOptionsCB> op)
    { if (ls.isEmpty()) { return new int[]{}; }
      return invoke(createBatchUpdateNonstrictCommand(processBatchInternally(ls, op, true), op)); }
    protected int[] delegateBatchDelete(List<ProductsOptions> ls, DeleteOption<ProductsOptionsCB> op)
    { if (ls.isEmpty()) { return new int[]{}; }
      return delegateBatchDeleteNonstrict(ls, op); }
    protected int[] delegateBatchDeleteNonstrict(List<ProductsOptions> ls, DeleteOption<ProductsOptionsCB> op)
    { if (ls.isEmpty()) { return new int[]{}; }
      return invoke(createBatchDeleteNonstrictCommand(processBatchInternally(ls, op, true), op)); }

    protected int delegateQueryInsert(ProductsOptions e, ProductsOptionsCB inCB, ConditionBean resCB, InsertOption<ProductsOptionsCB> op)
    { if (!processBeforeQueryInsert(e, inCB, resCB, op)) { return 0; } return invoke(createQueryInsertCBCommand(e, inCB, resCB, op));  }
    protected int delegateQueryUpdate(ProductsOptions e, ProductsOptionsCB cb, UpdateOption<ProductsOptionsCB> op)
    { if (!processBeforeQueryUpdate(e, cb, op)) { return 0; } return invoke(createQueryUpdateCBCommand(e, cb, op));  }
    protected int delegateQueryDelete(ProductsOptionsCB cb, DeleteOption<ProductsOptionsCB> op)
    { if (!processBeforeQueryDelete(cb, op)) { return 0; } return invoke(createQueryDeleteCBCommand(cb, op));  }

    // ===================================================================================
    //                                                                Optimistic Lock Info
    //                                                                ====================
    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean hasVersionNoValue(Entity entity) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean hasUpdateDateValue(Entity entity) {
        return false;
    }

    // ===================================================================================
    //                                                                     Downcast Helper
    //                                                                     ===============
    protected ProductsOptions downcast(Entity entity) {
        return helpEntityDowncastInternally(entity, ProductsOptions.class);
    }

    protected ProductsOptionsCB downcast(ConditionBean cb) {
        return helpConditionBeanDowncastInternally(cb, ProductsOptionsCB.class);
    }

    @SuppressWarnings("unchecked")
    protected List<ProductsOptions> downcast(List<? extends Entity> entityList) {
        return (List<ProductsOptions>)entityList;
    }

    @SuppressWarnings("unchecked")
    protected InsertOption<ProductsOptionsCB> downcast(InsertOption<? extends ConditionBean> option) {
        return (InsertOption<ProductsOptionsCB>)option;
    }

    @SuppressWarnings("unchecked")
    protected UpdateOption<ProductsOptionsCB> downcast(UpdateOption<? extends ConditionBean> option) {
        return (UpdateOption<ProductsOptionsCB>)option;
    }

    @SuppressWarnings("unchecked")
    protected DeleteOption<ProductsOptionsCB> downcast(DeleteOption<? extends ConditionBean> option) {
        return (DeleteOption<ProductsOptionsCB>)option;
    }

    @SuppressWarnings("unchecked")
    protected QueryInsertSetupper<ProductsOptions, ProductsOptionsCB> downcast(QueryInsertSetupper<? extends Entity, ? extends ConditionBean> option) {
        return (QueryInsertSetupper<ProductsOptions, ProductsOptionsCB>)option;
    }
}
