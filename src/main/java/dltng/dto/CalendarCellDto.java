package dltng.dto;


public class CalendarCellDto {

	public CalendarCellDto(int count, int day) {
		this.count = count;
		this.day = day;
	}

	public int count;

	public int day;

}
