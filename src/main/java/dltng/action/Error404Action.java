package dltng.action;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.seasar.struts.annotation.Execute;
import org.seasar.struts.util.RequestUtil;

public class Error404Action {
	@Execute(validator = false)
	public String index() {
		HttpServletRequest request = RequestUtil.getRequest();
		String referer = request.getHeader("Referer");

		// Enumeration<String> header = request.getHeaderNames();
		// while (header.hasMoreElements()) {
		// String h = header.nextElement();
		// System.out.println(h);
		// }
		String nu = (String) null;
		Enumeration<String> attribute = request.getAttributeNames();
		String uri = (String) request.getAttribute("javax.servlet.forward.query_string");
		// while (attribute.hasMoreElements()) {
		// String attr = attribute.nextElement();
		// System.out.println(attr);
		// }
		return "error404.jsp";
	}
}
