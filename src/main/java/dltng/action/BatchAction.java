package dltng.action;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.seasar.struts.annotation.Execute;
import org.seasar.struts.util.ResponseUtil;

import dltng.entity.AccountDeduct;
import dltng.entity.Deduct;
import dltng.entity.Request;
import dltng.service.AccountDeductService;
import dltng.service.DeductService;
import dltng.service.RequestDeductService;
import dltng.service.RequestService;
import dltng.util.ZenkakuKatakanaToHankakuKatakana;

public class BatchAction {
	@Resource
	RequestService requestService;

	@Resource
	RequestDeductService requestDeductService;

	@Resource
	DeductService deductService;

	@Resource
	AccountDeductService accountDeductService;

	@Execute(validator = false)
	public String index() {

		Date date = new Date();
		date = DateUtils.addMonths(date, 1);
		List<Request> requestList = requestService.findForBatch(date);

		// 銀行ごとに請求データを分ける
		Map<String, List<Request>> map = requestService.splitByBankCode(requestList);

		AccountDeduct ad = new AccountDeduct();
		Deduct deduct = new Deduct();
		int sum;
		for (Entry<String, List<Request>> entry : map.entrySet()) {
			sum = 0;
			setOwnBankInfo(entry.getKey(), ad);
			ad.count = entry.getValue().size();
			accountDeductService.insert(ad);

			for (Request request : entry.getValue()) {
				deduct.accountDeductId = ad.id;
				setDeductInfo(request, deduct);
				deductService.insert(deduct);
				sum += request.money;
			}

			ad.sum = sum;
			accountDeductService.update(ad);
		}
		return null;
	}

	private void setOwnBankInfo(String bankCode, AccountDeduct ad) {
		// TODO 口座振替データ設定
	}

	private void setDeductInfo(Request request, Deduct deduct) {
		// TODO 振替データ設定

	}

	@Execute(validator = false)
	public String trans() {
		Integer id = 1;
		AccountDeduct ad = accountDeductService.findByIdJoinDeduct(id);

		StringBuilder sb = new StringBuilder();

		/** ヘッダーレコード */
		// データ区分
		sb.append("1");
		// 種別コード
		sb.append("91");
		// コード区分
		sb.append("0");
		// 委託者コード
		sb.append("1122334455");
		// 委託者名
		String itakushamei = "イタクシャメイ";
		sb.append(StringUtils.rightPad(
			ZenkakuKatakanaToHankakuKatakana.zenkakuKatakanaToHankakuKatakana(itakushamei),
			40));
		// 引き落とし日
		Date date = new Date();
		// TODO DateUtilでパース
		sb.append("0915");
		// 銀行番号
		sb.append("0138");
		// 銀行名
		sb.append(StringUtils.rightPad(ZenkakuKatakanaToHankakuKatakana.zenkakuKatakanaToHankakuKatakana("ヨコハマ"), 15));
		// 支店番号
		sb.append("111");
		// 支店名
		sb
			.append(StringUtils.rightPad(
				ZenkakuKatakanaToHankakuKatakana.zenkakuKatakanaToHankakuKatakana("ミナトミライ"),
				15));
		// 預金種目
		sb.append(1);
		// 口座番号
		sb.append("0987654");
		// ダミー
		sb.append(StringUtils.rightPad(ZenkakuKatakanaToHankakuKatakana.zenkakuKatakanaToHankakuKatakana(""), 17));

		/** データレコード */
		for (Deduct deduct : ad.deduct) {
			// sb.append(deduct)
		}
		/** トレーラ レコード */

		/** エンド レコード */
		sb.append(StringUtils.rightPad(ZenkakuKatakanaToHankakuKatakana.zenkakuKatakanaToHankakuKatakana("9"), 120));

		try {
			ResponseUtil.download("furikae.txt", sb.toString().getBytes("JIS"));
		} catch (UnsupportedEncodingException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return null;
	}
}
