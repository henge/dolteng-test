package dltng.action;

import javax.annotation.Resource;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import dltng.form.InputForm;

public class InputAction {

	@ActionForm
	@Resource
	public InputForm inputForm;

	@Execute(validator = false)
	public String index() {
		return "input";
	}

	@Execute(validator = false)
	public String input() {
		return "input.jsp";
	}

	@Execute(validator = true, validate = "validate", input = "index")
	public String confirm() {
		return "confirm.jsp";
	}
}
