package dltng.action;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.annotation.Resource;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import dltng.form.UploadFileForm;

public class UploadFileAction {

	@ActionForm
	@Resource
	public UploadFileForm uploadFileForm;

	@Execute(validator = false)
	public String index() {
		return "index.jsp";
	}

	@Execute(validator = false)
	public String upload() {

		try {
			BufferedReader reader =
				new BufferedReader(new InputStreamReader(new BufferedInputStream(
					uploadFileForm.formFile.getInputStream()), "JIS"));
			char[] buff = new char[120];
			int off = 0;
			String str;

			System.out.println("--------ヘッダーレコード---------");

			while (reader.read(buff, off, 120) != -1) {
				switch (buff[0]) {
				case 'a':
					System.out.println("--------データレコード---------");
					System.out.print(buff[3]);
					str = String.valueOf(buff);
					System.out.println(str.substring(4, 6));
					break;

				case 'y':
					str = String.valueOf(buff);
					System.out.println("--------トレーラレコード---------");
					System.out.print(str.substring(19, 25));
					System.out.print(str.substring(25, 37));
					System.out.print(str.substring(37, 42));
					System.out.println(str.substring(42, 54));
					break;
				default:

					continue;
				}
			}
			System.out.println("--------エンドレコード---------");

		} catch (FileNotFoundException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		return null;
	}
}
