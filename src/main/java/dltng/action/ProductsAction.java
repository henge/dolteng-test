package dltng.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import dltng.code.ProductTypeCode;
import dltng.dto.ProductsDto;
import dltng.entity.Products;
import dltng.form.ProductForm;
import dltng.service.ProductsOptionsService;
import dltng.service.ProductsService;

public class ProductsAction {

	@Resource
	@ActionForm
	public ProductForm productForm;

	@Resource
	public ProductsService productsService;

	@Resource
	ProductsOptionsService productsOptionsService;

	public Products product;

	public List<Products> productsList;

	public Map<String, String[]> map;

	public List<ProductsDto> productDtoList;

	public List<ProductsDto> productsDtoList;

	@Execute(validator = false)
	public String index() {
		// product = productsService.findByIdJoin(1);
		productsList = productsService.findAllOrderById();
		// return "single.jsp";
		map = new HashMap<String, String[]>();
		String[] str1 = { "a", "b", "c" };
		String[] str2 = { "d", "e", "f" };
		// List<String> str1 = new ArrayList<String>();
		// str1.add("a");
		// str1.add("b");
		// str1.add("c");
		// List<String> str2 = new ArrayList<String>();
		// str2.add("d");
		// str2.add("e");
		// str2.add("f");

		map.put("1", str1);
		map.put("2", str2);

		// System.out.println("@@@@@" + ProductRankCode.GOOD.property);
		ProductTypeCode code = ProductTypeCode.GOOD;

		switch (code) {
		case BAD:
			System.out.println("~~~ bad! ~~~");
			break;

		case GOOD:
			System.out.println("~~~ good! ~~~");
			break;

		case NORMAL:
			System.out.println("~~~ normal! ~~~");
			break;

		case NEW:
			System.out.println("~~~ new! ~~~");
			break;

		default:
			break;
		}
		// return "index.jsp";
		return "slideshow.jsp";
	}

	@Execute(validator = false)
	public String outside() {
		productsDtoList = productsService.findByOutsideSQL();

		return null;
	}

	@Execute(validator = false)
	public String optionNameSearch() {
		productsList = productsService.findByOptionName(productForm.optionNameQuery);
		productsOptionsService.setOptionsToProductsList(productsList);

		return "list.jsp";
	}
}
