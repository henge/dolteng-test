package dltng.action;

import java.util.Calendar;

import javax.annotation.Resource;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import dltng.form.ResetForm;

public class ResetAction {
	@Resource
	@ActionForm
	public ResetForm resetForm;

	@Execute(validator = false, reset = "reset")
	public String index() {
		Calendar cal = Calendar.getInstance();
		if (resetForm.year == null) {
			resetForm.year = String.valueOf(cal.get(Calendar.YEAR));
		}
		if (resetForm.month == null) {
			resetForm.month = String.valueOf(cal.get(Calendar.MONTH) + 1);
		}
		return "index.jsp";
	}
}
