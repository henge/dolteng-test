package dltng.action;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.seasar.struts.annotation.Execute;

import dltng.dto.CalendarCellDto;

public class CalendarAction {

	public CalendarCellDto[][] calendarCellMatrix = new CalendarCellDto[6][7];

	@Execute(validator = false)
	public String index() {

		Calendar cal = Calendar.getInstance();
		Calendar firstDate = DateUtils.truncate(cal, Calendar.MONTH);
		Calendar lastDate = DateUtils.ceiling(cal, Calendar.MONTH);
		lastDate.add(Calendar.DATE, -1);
		int lastDay = lastDate.get(Calendar.DATE);

		// DBから取得したカウント数のMapを作る(日付=>カウント数) 0のものは作らない？もしくはこの段階で0を入れておく
		Map<Integer, Integer> countMap = new HashMap<Integer, Integer>();
		countMap.put(3, (int) Math.round(Math.random() * 100));
		countMap.put(6, (int) Math.round(Math.random() * 100));
		countMap.put(11, (int) Math.round(Math.random() * 100));
		countMap.put(12, (int) Math.round(Math.random() * 100));
		countMap.put(17, (int) Math.round(Math.random() * 100));
		countMap.put(21, (int) Math.round(Math.random() * 100));
		countMap.put(29, (int) Math.round(Math.random() * 100));

		// マトリックスに値を詰めていく
		int row = 0;
		int column = firstDate.get(Calendar.DAY_OF_WEEK) - 1;
		int count = 0;
		for (int day = 1; day <= lastDay; day++) {
			// count = (int) Math.round(Math.random() * 100);
			if (countMap.containsKey(day)) {
				count = countMap.get(day);
			} else {
				count = 0;
			}
			calendarCellMatrix[row][column] = new CalendarCellDto(count, day);
			if (column >= 6) {
				row++;
				column = 0;
			} else {
				column++;
			}
		}

		return "index.jsp";
	}
}
