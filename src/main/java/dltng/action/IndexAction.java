/*
 * Copyright 2004-2008 the Seasar Foundation and the Others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package dltng.action;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.time.DateUtils;
import org.seasar.struts.annotation.Execute;
import org.seasar.struts.util.RequestUtil;

public class IndexAction {

	public String host;
	public Date now;
	public Locale locale;

    @Execute(validator = false)
	public String index() {
		HttpServletRequest request = RequestUtil.getRequest();
		host = request.getHeader("Host");
		now = new Date();
		now = DateUtils.truncate(now, Calendar.MONTH);
		locale = Locale.getDefault();

		return "index.jsp";
	}
}
