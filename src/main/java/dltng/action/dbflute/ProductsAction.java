package dltng.action.dbflute;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import dltng.code.ProductTypeCode;
import dltng.dbflute.exbhv.ProductsBhv;
import dltng.dbflute.exentity.Products;
import dltng.form.ProductForm;

public class ProductsAction {

	@Resource
	@ActionForm
	public ProductForm productForm;

	@Resource
	public ProductsBhv productsBhv;

	public List<Products> productList;

	public Map<String, String[]> map;

	@Execute(validator = false)
	public String index() {

		productList = productsBhv.findAllOrderById();
		productList.get(0).getValidFlgAlias();
		map = new HashMap<String, String[]>();
		String[] str1 = { "a", "b", "c" };
		String[] str2 = { "d", "e", "f" };

		map.put("1", str1);
		map.put("2", str2);

		ProductTypeCode code = ProductTypeCode.GOOD;

		switch (code) {
		case BAD:
			System.out.println("~~~ bad! ~~~");
			break;

		case GOOD:
			System.out.println("~~~ good! ~~~");
			break;

		case NORMAL:
			System.out.println("~~~ normal! ~~~");
			break;

		case NEW:
			System.out.println("~~~ new! ~~~");
			break;

		default:
			break;
		}
		return "index.jsp";
	}
}

