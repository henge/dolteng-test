package dltng.action.dbflute;

import javax.annotation.Resource;

import org.seasar.framework.beans.util.Beans;
import org.seasar.struts.annotation.ActionForm;
import org.seasar.struts.annotation.Execute;

import dltng.dbflute.exbhv.ProductsBhv;
import dltng.dbflute.exentity.Products;
import dltng.form.ProductForm;

public class ProductsRegistAction {
	@Resource
	@ActionForm
	public ProductForm productForm;

	@Resource
	public ProductsBhv productsBhv;

	@Execute(validator = false)
	public String index() {

		return "input.jsp";
	}

	@Execute(validator = true, input = "index")
	public String complete() {
		Products entity = new Products();
		// コピーする前に判定する場合
		// CDef.Flg flg = CDef.Flg.codeOf(entity.getValidFlg());
		// if (flg.compareTo(CDef.Flg.Invalid) == 0) {
		// entity.setName(entity.getName() + "(無効)");
		// }
		Beans.copy(productForm, entity).execute();
		if (entity.isValidFlgInvalid()) {
			entity.setName(entity.getName() + "(無効)");
		}
		productsBhv.insert(entity);
		return "complete.jsp";
	}
}
