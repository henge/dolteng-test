package dltng.code;

public enum ProductRankCode {
	BAD("0", "悪い", "bad"), NORMAL("1", "普通", "normal"), GOOD("2", "良い", "good"), NEW("3", "新品", "new");

	public String key;

	public String name;

	public String property;

	private ProductRankCode(String key, String name, String property) {
		this.key = key;
		this.name = name;
		this.property = property;
	}

	public String getKey() {
		return this.key;
	}

	public String getName() {
		return this.name;
	}

	public String getProperty() {
		return this.property;
	}
}
