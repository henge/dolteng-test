package dltng.code;

public enum ProductTypeCode {
	BAD("0", "悪い"), NORMAL("1", "普通"), GOOD("2", "良い"), NEW("3", "新品");

	public String key;

	public String name;

	private ProductTypeCode(String key, String name) {
		this.key = key;
		this.name = name;
	}

	public String getKey() {
		return this.key;
	}

	public String getName() {
		return this.name;
	}
}
