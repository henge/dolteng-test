package dltng.service;

import dltng.entity.Categories;
import java.util.List;
import javax.annotation.Generated;

import static dltng.entity.CategoriesNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Categories}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2013/07/10 23:19:24")
public class CategoriesService extends AbstractService<Categories> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param id
     *            識別子
     * @return エンティティ
     */
    public Categories findById(Integer id) {
        return select().id(id).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Categories> findAllOrderById() {
        return select().orderBy(asc(id())).getResultList();
    }
}