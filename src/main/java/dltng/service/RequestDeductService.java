package dltng.service;

import dltng.entity.RequestDeduct;
import java.util.List;
import javax.annotation.Generated;

import static dltng.entity.RequestDeductNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link RequestDeduct}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2013/10/30 21:19:59")
public class RequestDeductService extends AbstractService<RequestDeduct> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param id
     *            識別子
     * @return エンティティ
     */
    public RequestDeduct findById(Integer id) {
        return select().id(id).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<RequestDeduct> findAllOrderById() {
        return select().orderBy(asc(id())).getResultList();
    }
}