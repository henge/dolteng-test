package dltng.service;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.service.S2AbstractService;

/**
 * サービスの抽象クラスです。
 * 
 * @param <ENTITY>
 *            エンティティの型 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.AbstServiceModelFactoryImpl"}, date = "2013/07/10 23:19:24")
public abstract class AbstractService<ENTITY> extends S2AbstractService<ENTITY> {
}