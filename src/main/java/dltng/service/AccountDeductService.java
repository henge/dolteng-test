package dltng.service;

import static dltng.entity.AccountDeductNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

import java.util.List;

import javax.annotation.Generated;

import dltng.entity.AccountDeduct;

/**
 * {@link AccountDeduct}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2013/10/30 21:19:59")
public class AccountDeductService extends AbstractService<AccountDeduct> {

    /**
	 * 識別子でエンティティを検索します。
	 * 
	 * @param id
	 *            識別子
	 * @return エンティティ
	 */
    public AccountDeduct findById(Integer id) {
        return select().id(id).getSingleResult();
    }

    /**
	 * 識別子の昇順ですべてのエンティティを検索します。
	 * 
	 * @return エンティティのリスト
	 */
    public List<AccountDeduct> findAllOrderById() {
        return select().orderBy(asc(id())).getResultList();
    }
	
	public AccountDeduct findByIdJoinDeduct(Integer id) {
		return select().leftOuterJoin(deduct()).id(id).getSingleResult();
	}
}