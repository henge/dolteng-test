package dltng.service;

import dltng.entity.Additions;
import java.util.List;
import javax.annotation.Generated;

import static dltng.entity.AdditionsNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Additions}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2013/07/10 23:19:24")
public class AdditionsService extends AbstractService<Additions> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param id
     *            識別子
     * @return エンティティ
     */
    public Additions findById(Integer id) {
        return select().id(id).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Additions> findAllOrderById() {
        return select().orderBy(asc(id())).getResultList();
    }
}