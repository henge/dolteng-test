package dltng.service;

import dltng.entity.Options;
import java.util.List;
import javax.annotation.Generated;

import static dltng.entity.OptionsNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Options}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2013/07/10 23:19:24")
public class OptionsService extends AbstractService<Options> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param id
     *            識別子
     * @return エンティティ
     */
    public Options findById(Integer id) {
        return select().id(id).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Options> findAllOrderById() {
        return select().orderBy(asc(id())).getResultList();
    }
}