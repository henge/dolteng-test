package dltng.service;

import static dltng.entity.ProductsOptionsNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Generated;

import org.seasar.extension.jdbc.where.SimpleWhere;

import dltng.entity.Products;
import dltng.entity.ProductsOptions;

/**
 * {@link ProductsOptions}のサービスクラスです。
 * 
 */
@Generated(value = { "S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl" }, date = "2013/07/10 23:19:24")
public class ProductsOptionsService extends AbstractService<ProductsOptions> {

	/**
	 * 識別子でエンティティを検索します。
	 * 
	 * @param productId
	 *            識別子
	 * @param optionId
	 *            識別子
	 * @return エンティティ
	 */
	public ProductsOptions findById(Integer productId, Integer optionId) {
		return select().id(productId, optionId).getSingleResult();
	}

	/**
	 * 識別子の昇順ですべてのエンティティを検索します。
	 * 
	 * @return エンティティのリスト
	 */
	public List<ProductsOptions> findAllOrderById() {
		return select().orderBy(asc(productId()), asc(optionId())).getResultList();
	}

	public void setOptionsToProductsList(List<Products> productsList) {
		// IN句用の商品IDのリスト作成
		List<Integer> productIdList = new ArrayList<Integer>();
		for (Products product : productsList) {
			productIdList.add(product.id);
		}

		// IN句を用いてproductsOptionsList取得
		List<ProductsOptions> productsOptionsList =
			select()
.leftOuterJoin(option())
				.where(new SimpleWhere().in(productId(), productIdList))
				.getResultList();

		// 仕分け
		Map<Integer, List<ProductsOptions>> map = new HashMap<Integer, List<ProductsOptions>>();
		for (ProductsOptions productsOptions : productsOptionsList) {
			if (!map.containsKey(productsOptions.productId)) {
				map.put(productsOptions.productId, new ArrayList<ProductsOptions>());
			}
			map.get(productsOptions.productId).add(productsOptions);
		}

		// 仕分けしたMapをproductsListに詰める
		for (Products product : productsList) {
			product.productsOptionsList = map.get(product.id);
		}
	}
}