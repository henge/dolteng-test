package dltng.service;

import static dltng.entity.ProductsNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

import java.util.List;

import javax.annotation.Generated;

import org.seasar.extension.jdbc.where.SimpleWhere;

import dltng.dto.ProductsDto;
import dltng.entity.Products;

/**
 * {@link Products}のサービスクラスです。
 *
 */
@Generated(value = { "S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl" }, date = "2013/07/10 23:19:24")
public class ProductsService extends AbstractService<Products> {


	/**
	 * 識別子でエンティティを検索します。
	 *
	 * @param id
	 *            識別子
	 * @return エンティティ
	 */
	public Products findById(Integer id) {
		return select().id(id).getSingleResult();
	}

	/**
	 * 識別子の昇順ですべてのエンティティを検索します。
	 *
	 * @return エンティティのリスト
	 */
	public List<Products> findAllOrderById() {
		return select().orderBy(asc(id())).getResultList();
	}

	public Products findByIdJoin(Integer id) {
		SimpleWhere where = new SimpleWhere();
		where.eq(id(), id);
		return select()
			.innerJoin("addition")
			.innerJoin("category")
			.innerJoin("productsOptionsList")
			.where(where)
			.getSingleResult();
	}

	public List<Products> findAllJoin(Integer id) {
		return select().innerJoin("addition").innerJoin("category").innerJoin("productsOptionsList").getResultList();
	}

	public List<ProductsDto> findByOutsideSQL() {
		return jdbcManager
.selectBySqlFile(ProductsDto.class, "dltng/sql/test.sql")
		// .limit(5)
			.getResultList();

	}

	public List<Products> findByOptionName(String query) {
		return jdbcManager.selectBySqlFile(Products.class, "dltng/sql/findProductsByOptionName.sql", "%" + query + "%")
		// .limit(5)
			.getResultList();
	}

}