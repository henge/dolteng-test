package dltng.service;

import static dltng.entity.RequestNames.*;
import static org.seasar.extension.jdbc.operation.Operations.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Generated;

import org.apache.commons.lang3.time.DateUtils;
import org.seasar.extension.jdbc.where.SimpleWhere;

import dltng.entity.Request;

/**
 * {@link Request}のサービスクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl"}, date = "2013/10/30 20:55:00")
public class RequestService extends AbstractService<Request> {

    /**
	 * 識別子でエンティティを検索します。
	 * 
	 * @param id
	 *            識別子
	 * @return エンティティ
	 */
    public Request findById(Integer id) {
        return select().id(id).getSingleResult();
    }

    /**
	 * 識別子の昇順ですべてのエンティティを検索します。
	 * 
	 * @return エンティティのリスト
	 */
    public List<Request> findAllOrderById() {
        return select().orderBy(asc(id())).getResultList();
    }

	public List<Request> findForBatch(Date date) {
		Date start = DateUtils.setDays(date, 1);
		Date end = DateUtils.addDays(DateUtils.addMonths(start, 1), -1);

		SimpleWhere where = new SimpleWhere();
		where.ge(deductDate(), start);
		where.le(deductDate(), end);
		// TODO 種別が駐車場のみ
		// TODO 支払い方法がが振替のみ
		return select().where(where).getResultList();
	}

	/**
	 * Requestエンティティのリストを銀行コードで仕分け
	 * 
	 * @param requestList
	 * @return
	 */
	public Map<String, List<Request>> splitByBankCode(List<Request> requestList) {
		Map<String, List<Request>> map = new HashMap<String, List<Request>>();
		for (Request request : requestList) {
			if (!map.containsKey(request.bankCode)) {
				map.put(request.bankCode, new ArrayList<Request>());
			}
			map.get(request.bankCode).add(request);
		}
		return map;
	}
}