package dltng.entity;

import dltng.entity.AdditionsNames._AdditionsNames;
import dltng.entity.CategoriesNames._CategoriesNames;
import dltng.entity.ProductsOptionsNames._ProductsOptionsNames;
import java.sql.Timestamp;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Products}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2013/12/05 23:48:14")
public class ProductsNames {

    /**
     * idのプロパティ名を返します。
     * 
     * @return idのプロパティ名
     */
    public static PropertyName<Integer> id() {
        return new PropertyName<Integer>("id");
    }

    /**
     * categoryIdのプロパティ名を返します。
     * 
     * @return categoryIdのプロパティ名
     */
    public static PropertyName<Integer> categoryId() {
        return new PropertyName<Integer>("categoryId");
    }

    /**
     * nameのプロパティ名を返します。
     * 
     * @return nameのプロパティ名
     */
    public static PropertyName<String> name() {
        return new PropertyName<String>("name");
    }

    /**
     * priceのプロパティ名を返します。
     * 
     * @return priceのプロパティ名
     */
    public static PropertyName<Integer> price() {
        return new PropertyName<Integer>("price");
    }

    /**
     * typeのプロパティ名を返します。
     * 
     * @return typeのプロパティ名
     */
    public static PropertyName<Integer> type() {
        return new PropertyName<Integer>("type");
    }

    /**
     * validFlgのプロパティ名を返します。
     * 
     * @return validFlgのプロパティ名
     */
    public static PropertyName<Integer> validFlg() {
        return new PropertyName<Integer>("validFlg");
    }

    /**
     * registeredのプロパティ名を返します。
     * 
     * @return registeredのプロパティ名
     */
    public static PropertyName<Timestamp> registered() {
        return new PropertyName<Timestamp>("registered");
    }

    /**
     * productsOptionsListのプロパティ名を返します。
     * 
     * @return productsOptionsListのプロパティ名
     */
    public static _ProductsOptionsNames productsOptionsList() {
        return new _ProductsOptionsNames("productsOptionsList");
    }

    /**
     * categoryのプロパティ名を返します。
     * 
     * @return categoryのプロパティ名
     */
    public static _CategoriesNames category() {
        return new _CategoriesNames("category");
    }

    /**
     * additionのプロパティ名を返します。
     * 
     * @return additionのプロパティ名
     */
    public static _AdditionsNames addition() {
        return new _AdditionsNames("addition");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _ProductsNames extends PropertyName<Products> {

        /**
         * インスタンスを構築します。
         */
        public _ProductsNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _ProductsNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _ProductsNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * idのプロパティ名を返します。
         *
         * @return idのプロパティ名
         */
        public PropertyName<Integer> id() {
            return new PropertyName<Integer>(this, "id");
        }

        /**
         * categoryIdのプロパティ名を返します。
         *
         * @return categoryIdのプロパティ名
         */
        public PropertyName<Integer> categoryId() {
            return new PropertyName<Integer>(this, "categoryId");
        }

        /**
         * nameのプロパティ名を返します。
         *
         * @return nameのプロパティ名
         */
        public PropertyName<String> name() {
            return new PropertyName<String>(this, "name");
        }

        /**
         * priceのプロパティ名を返します。
         *
         * @return priceのプロパティ名
         */
        public PropertyName<Integer> price() {
            return new PropertyName<Integer>(this, "price");
        }

        /**
         * typeのプロパティ名を返します。
         *
         * @return typeのプロパティ名
         */
        public PropertyName<Integer> type() {
            return new PropertyName<Integer>(this, "type");
        }

        /**
         * validFlgのプロパティ名を返します。
         *
         * @return validFlgのプロパティ名
         */
        public PropertyName<Integer> validFlg() {
            return new PropertyName<Integer>(this, "validFlg");
        }

        /**
         * registeredのプロパティ名を返します。
         *
         * @return registeredのプロパティ名
         */
        public PropertyName<Timestamp> registered() {
            return new PropertyName<Timestamp>(this, "registered");
        }

        /**
         * productsOptionsListのプロパティ名を返します。
         * 
         * @return productsOptionsListのプロパティ名
         */
        public _ProductsOptionsNames productsOptionsList() {
            return new _ProductsOptionsNames(this, "productsOptionsList");
        }

        /**
         * categoryのプロパティ名を返します。
         * 
         * @return categoryのプロパティ名
         */
        public _CategoriesNames category() {
            return new _CategoriesNames(this, "category");
        }

        /**
         * additionのプロパティ名を返します。
         * 
         * @return additionのプロパティ名
         */
        public _AdditionsNames addition() {
            return new _AdditionsNames(this, "addition");
        }
    }
}