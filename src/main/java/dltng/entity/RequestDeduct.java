package dltng.entity;

import java.io.Serializable;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * 振替請求エンティティクラス
 * 
 */
@Entity
@Generated(value = { "S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl" }, date = "2013/07/10 23:19:19")
public class RequestDeduct implements Serializable {

	private static final long serialVersionUID = 1L;

	/** idプロパティ */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(precision = 10, nullable = false, unique = true)
	public Integer id;

	/** 請求ID */
	@Column()
	public Integer requestId;

	/** 振替ID */
	@Column()
	public Integer deductId;

	@ManyToOne
	@JoinColumn(name = "REQUEST_ID", referencedColumnName = "id")
	public Request request;

	@ManyToOne
	@JoinColumn(name = "DEDUCT_ID", referencedColumnName = "id")
	public Deduct deduct;
}