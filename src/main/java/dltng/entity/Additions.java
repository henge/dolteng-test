package dltng.entity;

import java.io.Serializable;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * Additionsエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2013/07/10 23:19:18")
public class Additions implements Serializable {

    private static final long serialVersionUID = 1L;

    /** idプロパティ */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(precision = 10, nullable = false, unique = true)
    public Integer id;

    /** productIdプロパティ */
    @Column(precision = 10, nullable = false, unique = false)
    public Integer productId;

    /** infoプロパティ */
    @Column(length = 50, nullable = true, unique = false)
    public String info;
	
	@OneToOne
	@JoinColumn(name = "product_id", referencedColumnName = "id")
	public Products product;
}