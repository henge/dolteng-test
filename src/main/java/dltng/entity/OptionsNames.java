package dltng.entity;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Options}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2013/12/05 23:48:14")
public class OptionsNames {

    /**
     * idのプロパティ名を返します。
     * 
     * @return idのプロパティ名
     */
    public static PropertyName<Integer> id() {
        return new PropertyName<Integer>("id");
    }

    /**
     * optionNameのプロパティ名を返します。
     * 
     * @return optionNameのプロパティ名
     */
    public static PropertyName<String> optionName() {
        return new PropertyName<String>("optionName");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _OptionsNames extends PropertyName<Options> {

        /**
         * インスタンスを構築します。
         */
        public _OptionsNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _OptionsNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _OptionsNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * idのプロパティ名を返します。
         *
         * @return idのプロパティ名
         */
        public PropertyName<Integer> id() {
            return new PropertyName<Integer>(this, "id");
        }

        /**
         * optionNameのプロパティ名を返します。
         *
         * @return optionNameのプロパティ名
         */
        public PropertyName<String> optionName() {
            return new PropertyName<String>(this, "optionName");
        }
    }
}