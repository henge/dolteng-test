package dltng.entity;

import dltng.entity.DeductNames._DeductNames;
import dltng.entity.RequestNames._RequestNames;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link RequestDeduct}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2013/12/05 23:48:14")
public class RequestDeductNames {

    /**
     * idのプロパティ名を返します。
     * 
     * @return idのプロパティ名
     */
    public static PropertyName<Integer> id() {
        return new PropertyName<Integer>("id");
    }

    /**
     * requestIdのプロパティ名を返します。
     * 
     * @return requestIdのプロパティ名
     */
    public static PropertyName<Integer> requestId() {
        return new PropertyName<Integer>("requestId");
    }

    /**
     * deductIdのプロパティ名を返します。
     * 
     * @return deductIdのプロパティ名
     */
    public static PropertyName<Integer> deductId() {
        return new PropertyName<Integer>("deductId");
    }

    /**
     * requestのプロパティ名を返します。
     * 
     * @return requestのプロパティ名
     */
    public static _RequestNames request() {
        return new _RequestNames("request");
    }

    /**
     * deductのプロパティ名を返します。
     * 
     * @return deductのプロパティ名
     */
    public static _DeductNames deduct() {
        return new _DeductNames("deduct");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _RequestDeductNames extends PropertyName<RequestDeduct> {

        /**
         * インスタンスを構築します。
         */
        public _RequestDeductNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _RequestDeductNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _RequestDeductNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * idのプロパティ名を返します。
         *
         * @return idのプロパティ名
         */
        public PropertyName<Integer> id() {
            return new PropertyName<Integer>(this, "id");
        }

        /**
         * requestIdのプロパティ名を返します。
         *
         * @return requestIdのプロパティ名
         */
        public PropertyName<Integer> requestId() {
            return new PropertyName<Integer>(this, "requestId");
        }

        /**
         * deductIdのプロパティ名を返します。
         *
         * @return deductIdのプロパティ名
         */
        public PropertyName<Integer> deductId() {
            return new PropertyName<Integer>(this, "deductId");
        }

        /**
         * requestのプロパティ名を返します。
         * 
         * @return requestのプロパティ名
         */
        public _RequestNames request() {
            return new _RequestNames(this, "request");
        }

        /**
         * deductのプロパティ名を返します。
         * 
         * @return deductのプロパティ名
         */
        public _DeductNames deduct() {
            return new _DeductNames(this, "deduct");
        }
    }
}