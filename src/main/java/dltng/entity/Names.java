package dltng.entity;

import dltng.entity.AccountDeductNames._AccountDeductNames;
import dltng.entity.AdditionsNames._AdditionsNames;
import dltng.entity.CategoriesNames._CategoriesNames;
import dltng.entity.DeductNames._DeductNames;
import dltng.entity.OptionsNames._OptionsNames;
import dltng.entity.ProductsNames._ProductsNames;
import dltng.entity.ProductsOptionsNames._ProductsOptionsNames;
import dltng.entity.RequestDeductNames._RequestDeductNames;
import dltng.entity.RequestNames._RequestNames;
import javax.annotation.Generated;

/**
 * 名前クラスの集約です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.NamesAggregateModelFactoryImpl"}, date = "2013/12/05 23:48:14")
public class Names {

    /**
     * {@link AccountDeduct}の名前クラスを返します。
     * 
     * @return AccountDeductの名前クラス
     */
    public static _AccountDeductNames accountDeduct() {
        return new _AccountDeductNames();
    }

    /**
     * {@link Additions}の名前クラスを返します。
     * 
     * @return Additionsの名前クラス
     */
    public static _AdditionsNames additions() {
        return new _AdditionsNames();
    }

    /**
     * {@link Categories}の名前クラスを返します。
     * 
     * @return Categoriesの名前クラス
     */
    public static _CategoriesNames categories() {
        return new _CategoriesNames();
    }

    /**
     * {@link Deduct}の名前クラスを返します。
     * 
     * @return Deductの名前クラス
     */
    public static _DeductNames deduct() {
        return new _DeductNames();
    }

    /**
     * {@link Options}の名前クラスを返します。
     * 
     * @return Optionsの名前クラス
     */
    public static _OptionsNames options() {
        return new _OptionsNames();
    }

    /**
     * {@link Products}の名前クラスを返します。
     * 
     * @return Productsの名前クラス
     */
    public static _ProductsNames products() {
        return new _ProductsNames();
    }

    /**
     * {@link ProductsOptions}の名前クラスを返します。
     * 
     * @return ProductsOptionsの名前クラス
     */
    public static _ProductsOptionsNames productsOptions() {
        return new _ProductsOptionsNames();
    }

    /**
     * {@link Request}の名前クラスを返します。
     * 
     * @return Requestの名前クラス
     */
    public static _RequestNames request() {
        return new _RequestNames();
    }

    /**
     * {@link RequestDeduct}の名前クラスを返します。
     * 
     * @return RequestDeductの名前クラス
     */
    public static _RequestDeductNames requestDeduct() {
        return new _RequestDeductNames();
    }
}