package dltng.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * 請求エンティティクラス
 * 
 */
@Entity
@Generated(value = { "S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl" }, date = "2013/07/10 23:19:19")
public class Request implements Serializable {

	private static final long serialVersionUID = 1L;

	/** idプロパティ */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(precision = 10, nullable = false, unique = true)
	public Integer id;

	/** 銀行コード */
	@Column()
	public String bankCode;

	/** 銀行コード */
	@Column()
	public String bankName;

	/** 支店コード */
	@Column()
	public String branchCode;

	/** 支店コード */
	@Column()
	public String branchName;

	/** 口座種別 */
	@Column()
	public String accountType;

	/** 口座番号 */
	@Column()
	public String accountNumber;

	/** 宛先名カナ */
	@Column()
	public String addressNameKana;

	/** 引き落とし日 */
	@Column()
	public Date deductDate;

	/** 金額 */
	@Column()
	public Long money;

	@OneToMany(mappedBy = "request")
	public List<RequestDeduct> requestDeduct;

}