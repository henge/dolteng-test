package dltng.entity;

import dltng.entity.ProductsNames._ProductsNames;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Categories}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2013/12/05 23:48:14")
public class CategoriesNames {

    /**
     * idのプロパティ名を返します。
     * 
     * @return idのプロパティ名
     */
    public static PropertyName<Integer> id() {
        return new PropertyName<Integer>("id");
    }

    /**
     * nameのプロパティ名を返します。
     * 
     * @return nameのプロパティ名
     */
    public static PropertyName<String> name() {
        return new PropertyName<String>("name");
    }

    /**
     * productsLsitのプロパティ名を返します。
     * 
     * @return productsLsitのプロパティ名
     */
    public static _ProductsNames productsLsit() {
        return new _ProductsNames("productsLsit");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _CategoriesNames extends PropertyName<Categories> {

        /**
         * インスタンスを構築します。
         */
        public _CategoriesNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _CategoriesNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _CategoriesNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * idのプロパティ名を返します。
         *
         * @return idのプロパティ名
         */
        public PropertyName<Integer> id() {
            return new PropertyName<Integer>(this, "id");
        }

        /**
         * nameのプロパティ名を返します。
         *
         * @return nameのプロパティ名
         */
        public PropertyName<String> name() {
            return new PropertyName<String>(this, "name");
        }

        /**
         * productsLsitのプロパティ名を返します。
         * 
         * @return productsLsitのプロパティ名
         */
        public _ProductsNames productsLsit() {
            return new _ProductsNames(this, "productsLsit");
        }
    }
}