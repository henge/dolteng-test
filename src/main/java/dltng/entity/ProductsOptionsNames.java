package dltng.entity;

import dltng.entity.OptionsNames._OptionsNames;
import dltng.entity.ProductsNames._ProductsNames;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link ProductsOptions}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2013/12/05 23:48:14")
public class ProductsOptionsNames {

    /**
     * productIdのプロパティ名を返します。
     * 
     * @return productIdのプロパティ名
     */
    public static PropertyName<Integer> productId() {
        return new PropertyName<Integer>("productId");
    }

    /**
     * optionIdのプロパティ名を返します。
     * 
     * @return optionIdのプロパティ名
     */
    public static PropertyName<Integer> optionId() {
        return new PropertyName<Integer>("optionId");
    }

    /**
     * productのプロパティ名を返します。
     * 
     * @return productのプロパティ名
     */
    public static _ProductsNames product() {
        return new _ProductsNames("product");
    }

    /**
     * optionのプロパティ名を返します。
     * 
     * @return optionのプロパティ名
     */
    public static _OptionsNames option() {
        return new _OptionsNames("option");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _ProductsOptionsNames extends PropertyName<ProductsOptions> {

        /**
         * インスタンスを構築します。
         */
        public _ProductsOptionsNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _ProductsOptionsNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _ProductsOptionsNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * productIdのプロパティ名を返します。
         *
         * @return productIdのプロパティ名
         */
        public PropertyName<Integer> productId() {
            return new PropertyName<Integer>(this, "productId");
        }

        /**
         * optionIdのプロパティ名を返します。
         *
         * @return optionIdのプロパティ名
         */
        public PropertyName<Integer> optionId() {
            return new PropertyName<Integer>(this, "optionId");
        }

        /**
         * productのプロパティ名を返します。
         * 
         * @return productのプロパティ名
         */
        public _ProductsNames product() {
            return new _ProductsNames(this, "product");
        }

        /**
         * optionのプロパティ名を返します。
         * 
         * @return optionのプロパティ名
         */
        public _OptionsNames option() {
            return new _OptionsNames(this, "option");
        }
    }
}