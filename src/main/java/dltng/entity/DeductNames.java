package dltng.entity;

import dltng.entity.AccountDeductNames._AccountDeductNames;
import dltng.entity.RequestDeductNames._RequestDeductNames;
import java.sql.Date;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Deduct}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2013/12/05 23:48:14")
public class DeductNames {

    /**
     * idのプロパティ名を返します。
     * 
     * @return idのプロパティ名
     */
    public static PropertyName<Integer> id() {
        return new PropertyName<Integer>("id");
    }

    /**
     * accountDeductIdのプロパティ名を返します。
     * 
     * @return accountDeductIdのプロパティ名
     */
    public static PropertyName<Integer> accountDeductId() {
        return new PropertyName<Integer>("accountDeductId");
    }

    /**
     * bankCodeのプロパティ名を返します。
     * 
     * @return bankCodeのプロパティ名
     */
    public static PropertyName<String> bankCode() {
        return new PropertyName<String>("bankCode");
    }

    /**
     * bankNameのプロパティ名を返します。
     * 
     * @return bankNameのプロパティ名
     */
    public static PropertyName<String> bankName() {
        return new PropertyName<String>("bankName");
    }

    /**
     * branchCodeのプロパティ名を返します。
     * 
     * @return branchCodeのプロパティ名
     */
    public static PropertyName<String> branchCode() {
        return new PropertyName<String>("branchCode");
    }

    /**
     * branchNameのプロパティ名を返します。
     * 
     * @return branchNameのプロパティ名
     */
    public static PropertyName<String> branchName() {
        return new PropertyName<String>("branchName");
    }

    /**
     * accountTypeのプロパティ名を返します。
     * 
     * @return accountTypeのプロパティ名
     */
    public static PropertyName<String> accountType() {
        return new PropertyName<String>("accountType");
    }

    /**
     * accountNumberのプロパティ名を返します。
     * 
     * @return accountNumberのプロパティ名
     */
    public static PropertyName<String> accountNumber() {
        return new PropertyName<String>("accountNumber");
    }

    /**
     * addressNameKanaのプロパティ名を返します。
     * 
     * @return addressNameKanaのプロパティ名
     */
    public static PropertyName<String> addressNameKana() {
        return new PropertyName<String>("addressNameKana");
    }

    /**
     * deductDateのプロパティ名を返します。
     * 
     * @return deductDateのプロパティ名
     */
    public static PropertyName<Date> deductDate() {
        return new PropertyName<Date>("deductDate");
    }

    /**
     * moneyのプロパティ名を返します。
     * 
     * @return moneyのプロパティ名
     */
    public static PropertyName<Long> money() {
        return new PropertyName<Long>("money");
    }

    /**
     * requestDeductのプロパティ名を返します。
     * 
     * @return requestDeductのプロパティ名
     */
    public static _RequestDeductNames requestDeduct() {
        return new _RequestDeductNames("requestDeduct");
    }

    /**
     * accountDeductのプロパティ名を返します。
     * 
     * @return accountDeductのプロパティ名
     */
    public static _AccountDeductNames accountDeduct() {
        return new _AccountDeductNames("accountDeduct");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _DeductNames extends PropertyName<Deduct> {

        /**
         * インスタンスを構築します。
         */
        public _DeductNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _DeductNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _DeductNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * idのプロパティ名を返します。
         *
         * @return idのプロパティ名
         */
        public PropertyName<Integer> id() {
            return new PropertyName<Integer>(this, "id");
        }

        /**
         * accountDeductIdのプロパティ名を返します。
         *
         * @return accountDeductIdのプロパティ名
         */
        public PropertyName<Integer> accountDeductId() {
            return new PropertyName<Integer>(this, "accountDeductId");
        }

        /**
         * bankCodeのプロパティ名を返します。
         *
         * @return bankCodeのプロパティ名
         */
        public PropertyName<String> bankCode() {
            return new PropertyName<String>(this, "bankCode");
        }

        /**
         * bankNameのプロパティ名を返します。
         *
         * @return bankNameのプロパティ名
         */
        public PropertyName<String> bankName() {
            return new PropertyName<String>(this, "bankName");
        }

        /**
         * branchCodeのプロパティ名を返します。
         *
         * @return branchCodeのプロパティ名
         */
        public PropertyName<String> branchCode() {
            return new PropertyName<String>(this, "branchCode");
        }

        /**
         * branchNameのプロパティ名を返します。
         *
         * @return branchNameのプロパティ名
         */
        public PropertyName<String> branchName() {
            return new PropertyName<String>(this, "branchName");
        }

        /**
         * accountTypeのプロパティ名を返します。
         *
         * @return accountTypeのプロパティ名
         */
        public PropertyName<String> accountType() {
            return new PropertyName<String>(this, "accountType");
        }

        /**
         * accountNumberのプロパティ名を返します。
         *
         * @return accountNumberのプロパティ名
         */
        public PropertyName<String> accountNumber() {
            return new PropertyName<String>(this, "accountNumber");
        }

        /**
         * addressNameKanaのプロパティ名を返します。
         *
         * @return addressNameKanaのプロパティ名
         */
        public PropertyName<String> addressNameKana() {
            return new PropertyName<String>(this, "addressNameKana");
        }

        /**
         * deductDateのプロパティ名を返します。
         *
         * @return deductDateのプロパティ名
         */
        public PropertyName<Date> deductDate() {
            return new PropertyName<Date>(this, "deductDate");
        }

        /**
         * moneyのプロパティ名を返します。
         *
         * @return moneyのプロパティ名
         */
        public PropertyName<Long> money() {
            return new PropertyName<Long>(this, "money");
        }

        /**
         * requestDeductのプロパティ名を返します。
         * 
         * @return requestDeductのプロパティ名
         */
        public _RequestDeductNames requestDeduct() {
            return new _RequestDeductNames(this, "requestDeduct");
        }

        /**
         * accountDeductのプロパティ名を返します。
         * 
         * @return accountDeductのプロパティ名
         */
        public _AccountDeductNames accountDeduct() {
            return new _AccountDeductNames(this, "accountDeduct");
        }
    }
}