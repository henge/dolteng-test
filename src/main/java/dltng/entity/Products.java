package dltng.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * Productsエンティティクラス
 * 
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2013/07/10 23:19:19")
public class Products implements Serializable {

    private static final long serialVersionUID = 1L;

    /** idプロパティ */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(precision = 10, nullable = false, unique = true)
    public Integer id;

    /** categoryIdプロパティ */
    @Column(precision = 10, nullable = false, unique = false)
    public Integer categoryId;

    /** nameプロパティ */
    @Column(length = 100, nullable = false, unique = false)
    public String name;

    /** priceプロパティ */
    @Column(precision = 10, nullable = false, unique = false)
    public Integer price;

    /** typeプロパティ */
    @Column(precision = 10, nullable = false, unique = false)
    public Integer type;

    /** validFlgプロパティ */
    @Column(precision = 10, nullable = false, unique = false)
    public Integer validFlg;

    /** registeredプロパティ */
    @Column(nullable = false, unique = false)
    public Timestamp registered;

	@OneToMany(mappedBy = "product")
	public List<ProductsOptions> productsOptionsList;

	@ManyToOne
	@JoinColumn(name = "category_id", referencedColumnName = "id")
	public Categories category;

	@OneToOne(mappedBy = "product")
	public Additions addition;
}