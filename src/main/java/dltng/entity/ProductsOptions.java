package dltng.entity;

import java.io.Serializable;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * ProductsOptionsエンティティクラス
 *
 */
@Entity
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.EntityModelFactoryImpl"}, date = "2013/07/10 23:19:19")
public class ProductsOptions implements Serializable {

    private static final long serialVersionUID = 1L;

    /** productIdプロパティ */
    @Id
    @Column(precision = 10, nullable = false, unique = false)
    public Integer productId;

    /** optionIdプロパティ */
    @Id
    @Column(precision = 10, nullable = false, unique = false)
    public Integer optionId;

    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "id")
	public Products product;

	@OneToOne
	@JoinColumn(name = "option_id", referencedColumnName = "id")
	public Options option;
}