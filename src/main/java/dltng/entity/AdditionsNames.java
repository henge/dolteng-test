package dltng.entity;

import dltng.entity.ProductsNames._ProductsNames;
import javax.annotation.Generated;
import org.seasar.extension.jdbc.name.PropertyName;

/**
 * {@link Additions}のプロパティ名の集合です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.NamesModelFactoryImpl"}, date = "2013/12/05 23:48:14")
public class AdditionsNames {

    /**
     * idのプロパティ名を返します。
     * 
     * @return idのプロパティ名
     */
    public static PropertyName<Integer> id() {
        return new PropertyName<Integer>("id");
    }

    /**
     * productIdのプロパティ名を返します。
     * 
     * @return productIdのプロパティ名
     */
    public static PropertyName<Integer> productId() {
        return new PropertyName<Integer>("productId");
    }

    /**
     * infoのプロパティ名を返します。
     * 
     * @return infoのプロパティ名
     */
    public static PropertyName<String> info() {
        return new PropertyName<String>("info");
    }

    /**
     * productのプロパティ名を返します。
     * 
     * @return productのプロパティ名
     */
    public static _ProductsNames product() {
        return new _ProductsNames("product");
    }

    /**
     * @author S2JDBC-Gen
     */
    public static class _AdditionsNames extends PropertyName<Additions> {

        /**
         * インスタンスを構築します。
         */
        public _AdditionsNames() {
        }

        /**
         * インスタンスを構築します。
         * 
         * @param name
         *            名前
         */
        public _AdditionsNames(final String name) {
            super(name);
        }

        /**
         * インスタンスを構築します。
         * 
         * @param parent
         *            親
         * @param name
         *            名前
         */
        public _AdditionsNames(final PropertyName<?> parent, final String name) {
            super(parent, name);
        }

        /**
         * idのプロパティ名を返します。
         *
         * @return idのプロパティ名
         */
        public PropertyName<Integer> id() {
            return new PropertyName<Integer>(this, "id");
        }

        /**
         * productIdのプロパティ名を返します。
         *
         * @return productIdのプロパティ名
         */
        public PropertyName<Integer> productId() {
            return new PropertyName<Integer>(this, "productId");
        }

        /**
         * infoのプロパティ名を返します。
         *
         * @return infoのプロパティ名
         */
        public PropertyName<String> info() {
            return new PropertyName<String>(this, "info");
        }

        /**
         * productのプロパティ名を返します。
         * 
         * @return productのプロパティ名
         */
        public _ProductsNames product() {
            return new _ProductsNames(this, "product");
        }
    }
}