package dltng.form;

import java.io.Serializable;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

import dltng.util.StringValidator;

@Component(instance = InstanceType.SESSION)
public class InputForm implements Serializable {

	private static final long serialVersionUID = 1L;

	public String num;
	public String hankakueisu;
	public String hiragana;
	public String katakana;
	public String hankakukana;

	public ActionMessages validate() {
		ActionMessages errors = new ActionMessages();

		if (!num.matches("^[1-9][0-9]*")) {
			errors.add("num", new ActionMessage("errors.integer", "num"));
		}
		if (!hankakueisu.matches("[0-9a-zA-Z]")) {
			errors.add("hankakueisu", new ActionMessage("errors.hankakueisu", "hankakueisu"));
		}

		if (!hiragana.matches("[ぁ-ん]+")) {
			errors.add("hiragana", new ActionMessage("errors.hiragana", "hiragana"));
		}
		System.out.println(StringValidator.isUTF8(hiragana));

		if (!katakana.matches("[ァ-ヴ]+")) {
			errors.add("katakana", new ActionMessage("errors.katakana", "katakana"));
		}

		if (!hankakukana.matches("[ｦ-ﾟﾞ]+")) {
			errors.add("hankakukana", new ActionMessage("errors.hankakukana", "hankakukana"));
		}
		return errors;
	}
}
