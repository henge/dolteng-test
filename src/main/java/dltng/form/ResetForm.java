package dltng.form;

import java.io.Serializable;

import org.seasar.framework.container.annotation.tiger.Component;
import org.seasar.framework.container.annotation.tiger.InstanceType;

@Component(instance = InstanceType.SESSION)
public class ResetForm implements Serializable {

	private static final long serialVersionUID = 1L;

	public String page = "1";

	public String limit = "20";

	public String year;

	public String month;

	// public ResetForm() {
	// Calendar cal = Calendar.getInstance();
	// year = String.valueOf(cal.get(Calendar.YEAR));
	// month = String.valueOf(cal.get(Calendar.MONTH) + 1);
	// }

	public void reset() {
		// page = "1";
	}
}
