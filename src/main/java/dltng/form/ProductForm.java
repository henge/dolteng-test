package dltng.form;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.seasar.struts.annotation.Required;

import dltng.code.ProductRankCode;
import dltng.code.ProductTypeCode;
import dltng.dbflute.allcommon.CDef.Flg;


public class ProductForm {
	public ProductTypeCode[] types = ProductTypeCode.values();
	public ProductRankCode[] ranks = ProductRankCode.values();


	// public String test = types[0].name;
	// public Map<String, String> typesMap =
	// newLinkedHashMap(ProductTypeCode.values());
	@Required
	public String categoryId;
	@Required
	public String name;
	@Required
	public String price;
	@Required
	public String type;
	@Required
	public String validFlg;

	public List<Flg> flgList = Flg.listAll();

	public Map<String, String> flgMap;

	public ProductForm() {
		flgMap = new HashMap<String, String>();
		for (Flg flg : flgList) {
			flgMap.put(flg.code(), flg.alias());
		}
	}
	
	public String optionNameQuery;

}
