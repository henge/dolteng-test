package dltng.form;

import org.apache.struts.upload.FormFile;
import org.seasar.struts.annotation.Required;

public class UploadFileForm {

	@Required
	public FormFile formFile;
}