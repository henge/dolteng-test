package dltng.task;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.seasar.framework.container.SingletonS2Container;

import dltng.entity.Request;
import dltng.service.AccountDeductService;
import dltng.service.DeductService;
import dltng.service.RequestDeductService;
import dltng.service.RequestService;

public class CreateDeductDataBatch {
	public static void main(String[] args) {
		System.out.println("start CreateDeductDataBatch");

		RequestService requestService = SingletonS2Container.getComponent(RequestService.class);
		RequestDeductService requestDeductService = SingletonS2Container.getComponent(RequestDeductService.class);
		DeductService deductService = SingletonS2Container.getComponent(DeductService.class);
		AccountDeductService accountDeductService = SingletonS2Container.getComponent(AccountDeductService.class);

		List<Request> requestList = requestService.findForBatch(new Date());

		// TODO 銀行ごとに請求データを分ける
		Map<String, List<Request>> map = requestService.splitByBankCode(requestList);

		System.out.println("end");
	}
}
