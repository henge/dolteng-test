package com.example.dbflute.flex.dbflute.dtomapper;

import java.util.Map;
import org.seasar.dbflute.Entity;
import com.example.dbflute.flex.dbflute.dtomapper.bs.BsOptionsDtoMapper;

/**
 * The DTO mapper of OPTIONS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class OptionsDtoMapper extends BsOptionsDtoMapper {

    /** Serial version UID. (Default) */
    private static final long serialVersionUID = 1L;

    public OptionsDtoMapper() {
    }

    public OptionsDtoMapper(Map<Entity, Object> relationDtoMap, Map<Object, Entity> relationEntityMap) {
        super(relationDtoMap, relationEntityMap);
    }
}
