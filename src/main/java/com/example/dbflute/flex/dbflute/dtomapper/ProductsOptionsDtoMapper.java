package com.example.dbflute.flex.dbflute.dtomapper;

import java.util.Map;
import org.seasar.dbflute.Entity;
import com.example.dbflute.flex.dbflute.dtomapper.bs.BsProductsOptionsDtoMapper;

/**
 * The DTO mapper of PRODUCTS_OPTIONS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class ProductsOptionsDtoMapper extends BsProductsOptionsDtoMapper {

    /** Serial version UID. (Default) */
    private static final long serialVersionUID = 1L;

    public ProductsOptionsDtoMapper() {
    }

    public ProductsOptionsDtoMapper(Map<Entity, Object> relationDtoMap, Map<Object, Entity> relationEntityMap) {
        super(relationDtoMap, relationEntityMap);
    }
}
