package com.example.dbflute.flex.dbflute.dtomapper;

import java.util.Map;
import org.seasar.dbflute.Entity;
import com.example.dbflute.flex.dbflute.dtomapper.bs.BsSchemaInfoDtoMapper;

/**
 * The DTO mapper of SCHEMA_INFO.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class SchemaInfoDtoMapper extends BsSchemaInfoDtoMapper {

    /** Serial version UID. (Default) */
    private static final long serialVersionUID = 1L;

    public SchemaInfoDtoMapper() {
    }

    public SchemaInfoDtoMapper(Map<Entity, Object> relationDtoMap, Map<Object, Entity> relationEntityMap) {
        super(relationDtoMap, relationEntityMap);
    }
}
