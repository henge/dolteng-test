package com.example.dbflute.flex.simpleflute.dto;

import com.example.dbflute.flex.simpleflute.dto.bs.BsProductsDto;

/**
 * The entity of PRODUCTS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class ProductsDto extends BsProductsDto {

    /** Serial version UID. (Default) */
    private static final long serialVersionUID = 1L;
}
