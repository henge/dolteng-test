package com.example.dbflute.flex.simpleflute.dto;

import com.example.dbflute.flex.simpleflute.dto.bs.BsProductsOptionsDto;

/**
 * The entity of PRODUCTS_OPTIONS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class ProductsOptionsDto extends BsProductsOptionsDto {

    /** Serial version UID. (Default) */
    private static final long serialVersionUID = 1L;
}
