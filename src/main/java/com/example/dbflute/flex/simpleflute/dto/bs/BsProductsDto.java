package com.example.dbflute.flex.simpleflute.dto.bs;

import java.io.Serializable;
import java.util.*;


/**
 * The simple DTO of PRODUCTS as TABLE. <br />
 * <pre>
 * [primary-key]
 *     ID
 * 
 * [column]
 *     ID, CATEGORY_ID, NAME, PRICE, TYPE, VALID_FLG, REGISTERED
 * 
 * [sequence]
 *     
 * 
 * [identity]
 *     ID
 * 
 * [version-no]
 *     
 * 
 * [foreign-table]
 *     
 * 
 * [referrer-table]
 *     
 * 
 * [foreign-property]
 *     
 * 
 * [referrer-property]
 *     
 * </pre>
 * @author DBFlute(AutoGenerator)
 */
public abstract class BsProductsDto implements Serializable {

    // ===================================================================================
    //                                                                          Definition
    //                                                                          ==========
    /** Serial version UID. (Default) */
    private static final long serialVersionUID = 1L;

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    // -----------------------------------------------------
    //                                                Column
    //                                                ------
    /** ID: {PK, ID, NotNull, INT(10)} */
    protected Integer _id;

    /** CATEGORY_ID: {IX, NotNull, INT(10)} */
    protected Integer _categoryId;

    /** NAME: {NotNull, VARCHAR(100)} */
    protected String _name;

    /** PRICE: {NotNull, INT(10)} */
    protected Integer _price;

    /** TYPE: {NotNull, INT(10)} */
    protected Integer _type;

    /** VALID_FLG: {NotNull, INT(10), classification=Flg} */
    protected Integer _validFlg;

    /** REGISTERED: {NotNull, TIMESTAMP(19), default=[CURRENT_TIMESTAMP]} */
    protected java.sql.Timestamp _registered;

    // -----------------------------------------------------
    //                                              Internal
    //                                              --------
    /** The modified properties for this DTO. */
    protected final Set<String> __modifiedProperties = new LinkedHashSet<String>();

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    public BsProductsDto() {
    }

    // ===================================================================================
    //                                                                       Foreign Table
    //                                                                       =============
    // ===================================================================================
    //                                                                      Referrer Table
    //                                                                      ==============
    // ===================================================================================
    //                                                                 Modified Properties
    //                                                                 ===================
    public Set<String> modifiedProperties() {
        return __modifiedProperties;
    }

    public void clearModifiedInfo() {
        __modifiedProperties.clear();
    }

    public boolean hasModification() {
        return !__modifiedProperties.isEmpty();
    }

    // ===================================================================================
    //                                                                      Basic Override
    //                                                                      ==============
    public boolean equals(Object other) {
        if (other == null || !(other instanceof BsProductsDto)) { return false; }
        final BsProductsDto otherEntity = (BsProductsDto)other;
        if (!helpComparingValue(getId(), otherEntity.getId())) { return false; }
        return true;
    }

    protected boolean helpComparingValue(Object value1, Object value2) {
        if (value1 == null && value2 == null) { return true; }
        return value1 != null && value2 != null && value1.equals(value2);
    }

    public int hashCode() {
        int result = 17;
        result = xCH(result, "PRODUCTS");
        result = xCH(result, getId());
        return result;
    }
    protected int xCH(int result, Object value) { // calculateHashcode()
        if (value == null) {
            return result;
        }
        return (31 * result) + (value instanceof byte[] ? ((byte[]) value).length : value.hashCode());
    }

    public int instanceHash() {
        return super.hashCode();
    }

    public String toString() {
        String c = ", ";
        StringBuilder sb = new StringBuilder();
        sb.append(c).append(getId());
        sb.append(c).append(getCategoryId());
        sb.append(c).append(getName());
        sb.append(c).append(getPrice());
        sb.append(c).append(getType());
        sb.append(c).append(getValidFlg());
        sb.append(c).append(getRegistered());
        if (sb.length() > 0) { sb.delete(0, c.length()); }
        sb.insert(0, "{").append("}");
        return sb.toString();
    }

    // ===================================================================================
    //                                                                            Accessor
    //                                                                            ========
    /**
     * [get] ID: {PK, ID, NotNull, INT(10)} <br />
     * @return The value of the column 'ID'. (NullAllowed)
     */
    public Integer getId() {
        return _id;
    }

    /**
     * [set] ID: {PK, ID, NotNull, INT(10)} <br />
     * @param id The value of the column 'ID'. (NullAllowed)
     */
    public void setId(Integer id) {
        __modifiedProperties.add("id");
        this._id = id;
    }

    /**
     * [get] CATEGORY_ID: {IX, NotNull, INT(10)} <br />
     * @return The value of the column 'CATEGORY_ID'. (NullAllowed)
     */
    public Integer getCategoryId() {
        return _categoryId;
    }

    /**
     * [set] CATEGORY_ID: {IX, NotNull, INT(10)} <br />
     * @param categoryId The value of the column 'CATEGORY_ID'. (NullAllowed)
     */
    public void setCategoryId(Integer categoryId) {
        __modifiedProperties.add("categoryId");
        this._categoryId = categoryId;
    }

    /**
     * [get] NAME: {NotNull, VARCHAR(100)} <br />
     * @return The value of the column 'NAME'. (NullAllowed)
     */
    public String getName() {
        return _name;
    }

    /**
     * [set] NAME: {NotNull, VARCHAR(100)} <br />
     * @param name The value of the column 'NAME'. (NullAllowed)
     */
    public void setName(String name) {
        __modifiedProperties.add("name");
        this._name = name;
    }

    /**
     * [get] PRICE: {NotNull, INT(10)} <br />
     * @return The value of the column 'PRICE'. (NullAllowed)
     */
    public Integer getPrice() {
        return _price;
    }

    /**
     * [set] PRICE: {NotNull, INT(10)} <br />
     * @param price The value of the column 'PRICE'. (NullAllowed)
     */
    public void setPrice(Integer price) {
        __modifiedProperties.add("price");
        this._price = price;
    }

    /**
     * [get] TYPE: {NotNull, INT(10)} <br />
     * @return The value of the column 'TYPE'. (NullAllowed)
     */
    public Integer getType() {
        return _type;
    }

    /**
     * [set] TYPE: {NotNull, INT(10)} <br />
     * @param type The value of the column 'TYPE'. (NullAllowed)
     */
    public void setType(Integer type) {
        __modifiedProperties.add("type");
        this._type = type;
    }

    /**
     * [get] VALID_FLG: {NotNull, INT(10), classification=Flg} <br />
     * @return The value of the column 'VALID_FLG'. (NullAllowed)
     */
    public Integer getValidFlg() {
        return _validFlg;
    }

    /**
     * [set] VALID_FLG: {NotNull, INT(10), classification=Flg} <br />
     * @param validFlg The value of the column 'VALID_FLG'. (NullAllowed)
     */
    public void setValidFlg(Integer validFlg) {
        __modifiedProperties.add("validFlg");
        this._validFlg = validFlg;
    }

    /**
     * [get] REGISTERED: {NotNull, TIMESTAMP(19), default=[CURRENT_TIMESTAMP]} <br />
     * @return The value of the column 'REGISTERED'. (NullAllowed)
     */
    public java.sql.Timestamp getRegistered() {
        return _registered;
    }

    /**
     * [set] REGISTERED: {NotNull, TIMESTAMP(19), default=[CURRENT_TIMESTAMP]} <br />
     * @param registered The value of the column 'REGISTERED'. (NullAllowed)
     */
    public void setRegistered(java.sql.Timestamp registered) {
        __modifiedProperties.add("registered");
        this._registered = registered;
    }

}
