package com.example.dbflute.flex.simpleflute.dto;

import com.example.dbflute.flex.simpleflute.dto.bs.BsOptionsDto;

/**
 * The entity of OPTIONS.
 * <p>
 * You can implement your original methods here.
 * This class remains when re-generating.
 * </p>
 * @author DBFlute(AutoGenerator)
 */
public class OptionsDto extends BsOptionsDto {

    /** Serial version UID. (Default) */
    private static final long serialVersionUID = 1L;
}
