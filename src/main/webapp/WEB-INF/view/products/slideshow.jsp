<%@page pageEncoding="UTF-8"%>
<html>
<head>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript">
$(function(){
	var setId = '#slideshow';
	var fadeTime = 500;
	var delayTime = 2000;

	$(setId + ' div div').each(function(i){
		$(this).attr('id','view' + (i + 1).toString());
		// $(setId + ' div div').css({zIndex:'98',opacity:'0'});
		// $(setId + ' div div:first').css({zIndex:'99'}).stop().animate({opacity:'1'},fadeTime);
		$(setId + ' div div').hide();
		$(setId + ' div div:first').show();
	});

	$(setId + ' ul li ul li').click(function(){
		clearInterval(setTimer);

		var connectCont = $(setId + ' ul li ul li').index(this);
		var showCont = connectCont+1;

		// $(setId + ' div div#view' + (showCont)).siblings().stop().animate({opacity:'0'},fadeTime,function(){$(this).css({zIndex:'98'})});
		// $(setId + ' div div#view' + (showCont)).stop().animate({opacity:'1'},fadeTime,function(){$(this).css({zIndex:'99'})});
		$(setId + ' div div#view' + (showCont)).stop().show(fadeTime);
		$(setId + ' div div#view' + (showCont)).siblings().stop().hide(fadeTime);

		$(setId + ' ul li ul li').removeClass('active');
		$(this).addClass('active');

		timer();

	});

	$(setId + ' ul li ul li:not(.active)').hover(function(){
		$(this).stop().animate({opacity:'1'},200);
	},function(){
		$(this).stop().animate({opacity:'0.5'},200);
	});

	$(setId + ' ul li ul li').css({opacity:'0.5'});
	$(setId + ' ul li ul li:first').addClass('active');
	flip();
	timer();

	function timer() {
		setTimer = setInterval(function(){
			$('li.active').each(function(){
				var wrapperLengh = $(setId + ' > ul > li').length;
				var wrapperIndex = $(setId + ' > ul > li').index($(this).parent('ul').parent('li')[0]);
				var wrapperCount = wrapperIndex + 1;

				var listLengh = $(this).parent('ul').find('li').length;
				var listIndex = $(this).parent('ul').find('li').index(this);
				var listCount = listIndex+1;

				if(wrapperLengh == wrapperCount && listLengh == listCount){
					// 最後のページの最後の要素の場合は最初のページの先頭のクリックイベントを発生させる
					$(setId + ' ul li:first ul li:first').click();
					// 先頭のページを表示
					flip();
				}else if(listLengh == listCount){
					// 次のページの先頭のクリックイベントを発生させる
					$(this).parent('ul').parent('li').next('li').find('ul li:first').click();
					flip();
				} else {
					$(this).next('li').click();
				};
			});
		},delayTime);
	};

	function flip(){
		$('li.active').parent('ul').parent('li').siblings().hide();
		$('li.active').parent('ul').parent('li').show();
	}
});
</script>
<style>
ul {
	list-style: none;
	padding: 0;
}
ul li {
	float: left;
	margin-right: 5px;
}
li.active a {
	color: red;
}
</style>
</head>
<body>
<div id="slideshow">
	<!-- 詳細情報 こちらはネストの必要なし -->
	<div>
		<div><a href="#1">タイトル1</a></div>
		<div><a href="#2">タイトル2</a></div>
		<div><a href="#3">タイトル3</a></div>
		<div><a href="#4">タイトル4</a></div>
		<div><a href="#5">タイトル5</a></div>
		<div><a href="#6">タイトル6</a></div>
		<div><a href="#7">タイトル7</a></div>
		<div><a href="#8">タイトル8</a></div>
		<div><a href="#9">タイトル9</a></div>
		<div><a href="#10">タイトル10</a></div>
		<div><a href="#11">タイトル11</a></div>
		<div><a href="#12">タイトル12</a></div>
		<div><a href="#13">タイトル13</a></div>
	</div>

	<!-- サムネイル ページごとにネストしてまとめておく -->
	<ul>
		<li>
			<ul>
				<li><a href="javascript:void(0);">1</a></li>
				<li><a href="javascript:void(0);">2</a></li>
				<li><a href="javascript:void(0);">3</a></li>
				<li><a href="javascript:void(0);">4</a></li>
				<li><a href="javascript:void(0);">5</a></li>
			</ul>
		</li>
		<li>
			<ul>
				<li><a href="javascript:void(0);">6</a></li>
				<li><a href="javascript:void(0);">7</a></li>
				<li><a href="javascript:void(0);">8</a></li>
				<li><a href="javascript:void(0);">9</a></li>
				<li><a href="javascript:void(0);">10</a></li>
			</ul>
		</li>
		<li>
			<ul>
				<li><a href="javascript:void(0);">11</a></li>
				<li><a href="javascript:void(0);">12</a></li>
				<li><a href="javascript:void(0);">13</a></li>
			</ul>
		</li>
	</ul>

</div><!--/#slideshow-->

</body>
</html>