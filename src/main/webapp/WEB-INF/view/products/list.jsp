<%@page pageEncoding="UTF-8"%>
<html>
<body>
<form action="">
	<input type="text" name="optionNameQuery" value="${optionNameQuery}"/>
</form>
<table border="1">
	<tr>
		<td>id</td>
		<td>name</td>
		<td>price</td>
		<td>type</td>
		<td>property</td>
		<td>validFlg</td>
		<td>optionName</td>
	</tr>
	<c:forEach items="${productsList}" var="product" varStatus="status">
	<tr>
		<td>${product.id}</td>
		<td>${product.name}</td>
		<td>${product.price}</td>
		<td>${product.type}</td>
		<td>${ranks[product.type].property}</td>
		<td>${product.validFlg}</td>
		<td>
			<c:forEach var="rec" items="${product.productsOptionsList}">
				${f:h(rec.option.optionName)}
			</c:forEach>
		</td>
	</tr>
	</c:forEach>
</table>
</body>
</html>