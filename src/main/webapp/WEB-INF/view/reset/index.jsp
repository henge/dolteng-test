<%@page pageEncoding="UTF-8"%>
<html>
<body>
<p><a href="/reset">reset</a></p>
<p>page: ${page}</p>
<p>limit: ${limit}</p>
<p>year: ${year}</p>
<p>month: ${month}</p>

<ul>
	<li><a href="?page=1">1</a></li>
	<li><a href="?page=2">2</a></li>
	<li><a href="?page=3">3</a></li>
	<li><a href="?page=4">4</a></li>
	<li><a href="?page=5">5</a></li>
</ul>

<form action="">
	<select name="limit" onChange="this.form.submit();">
		<option value="20"<c:if test="${limit == 20}">selected</c:if>>20</option>
		<option value="40"<c:if test="${limit == 40}">selected</c:if>>40</option>
	</select>
</form>

</body>
</html>