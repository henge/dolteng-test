<%@page pageEncoding="UTF-8"%>
<html>
<body>
<table border="1">
	<tr>
		<td>id</td>
		<td>name</td>
		<td>price</td>
		<td>type</td>
		<td>property</td>
		<td>validFlg</td>
		<td>aaa</td>
	</tr>
	<c:forEach items="${productList}" var="product" varStatus="status">
	<tr>
		<td>${product.id}</td>
		<td>${product.name}</td>
		<td>${product.price}</td>
		<td>${product.type}</td>
		<td>${ranks[product.type].property}</td>
		<td>${product.validFlgAlias}</td>
		<td>
			<c:set var="id">${product.id}</c:set>
			${id}
			<c:forEach items="${map[id]}" var="s">
				${s}
			</c:forEach>
		</td>
	</tr>
	</c:forEach>
</table>
<c:forEach items="${map[2]}" var="s">
	${s}
</c:forEach>
<%-- <c:forEach items="${map}" var="m">
	<c:forEach items="${m}" var="s">
		${s}
	</c:forEach>
</c:forEach> --%>
<%--
<c:forEach items="${types}" var="type">
	${type.key }
</c:forEach> --%>
</body>
</html>