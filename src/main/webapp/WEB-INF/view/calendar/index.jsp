<%@page pageEncoding="UTF-8"%>
<html>
<body>
<h1>カレンダー</h1>
<table border="1">
	<tr>
		<th>Sun</th>
		<th>Mon</th>
		<th>Tue</th>
		<th>Wed</th>
		<th>Thu</th>
		<th>Fri</th>
		<th>Sat</th>
	</tr>
<c:forEach var="week" items="${calendarCellMatrix}">
	<tr>
	<c:forEach var="day" items="${week}">
		<td>
			<c:choose>

			<c:when test="${day == null}">
			-
			</c:when>
			<c:otherwise>
			${day.day}<br />
			<span style="color:red;">[${day.count}]</span>
			</c:otherwise>
			</c:choose>
		</td>
	</c:forEach>
	</tr>
</c:forEach>
</table>
</body>
</html>