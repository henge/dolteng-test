package dltng.entity;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.JdbcManager;
import org.seasar.extension.unit.S2TestCase;

import static dltng.entity.DeductNames.*;

/**
 * {@link Deduct}のテストクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.EntityTestModelFactoryImpl"}, date = "2013/10/30 21:20:01")
public class DeductTest extends S2TestCase {

    private JdbcManager jdbcManager;

    /**
     * 事前処理をします。
     * 
     * @throws Exception
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        include("s2jdbc.dicon");
    }

    /**
     * 識別子による取得をテストします。
     * 
     * @throws Exception
     */
    public void testFindById() throws Exception {
        jdbcManager.from(Deduct.class).id(1).getSingleResult();
    }

    /**
     * requestDeductとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_requestDeduct() throws Exception {
        jdbcManager.from(Deduct.class).leftOuterJoin(requestDeduct()).id(1).getSingleResult();
    }

    /**
     * accountDeductとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_accountDeduct() throws Exception {
        jdbcManager.from(Deduct.class).leftOuterJoin(accountDeduct()).id(1).getSingleResult();
    }
}