package dltng.entity;

import javax.annotation.Generated;
import org.seasar.extension.jdbc.JdbcManager;
import org.seasar.extension.unit.S2TestCase;

import static dltng.entity.RequestDeductNames.*;

/**
 * {@link RequestDeduct}のテストクラスです。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.EntityTestModelFactoryImpl"}, date = "2013/10/30 21:20:01")
public class RequestDeductTest extends S2TestCase {

    private JdbcManager jdbcManager;

    /**
     * 事前処理をします。
     * 
     * @throws Exception
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        include("s2jdbc.dicon");
    }

    /**
     * 識別子による取得をテストします。
     * 
     * @throws Exception
     */
    public void testFindById() throws Exception {
        jdbcManager.from(RequestDeduct.class).id(1).getSingleResult();
    }

    /**
     * requestとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_request() throws Exception {
        jdbcManager.from(RequestDeduct.class).leftOuterJoin(request()).id(1).getSingleResult();
    }

    /**
     * deductとの外部結合をテストします。
     * 
     * @throws Exception
     */
    public void testLeftOuterJoin_deduct() throws Exception {
        jdbcManager.from(RequestDeduct.class).leftOuterJoin(deduct()).id(1).getSingleResult();
    }
}