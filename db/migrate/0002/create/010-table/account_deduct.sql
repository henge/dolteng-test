create table ACCOUNT_DEDUCT (
    ID int not null auto_increment,
    BANK_CODE varchar(255),
    BANK_NAME varchar(255),
    BRANCH_CODE varchar(255),
    BRANCH_NAME varchar(255),
    ACCOUNT_TYPE varchar(255),
    ACCOUNT_NUMBER varchar(255),
    ADDRESS_NAME_KANA varchar(255),
    DEDUCT_DATE date,
    MONEY bigint,
    COUNT int,
    SUM int,
    constraint ACCOUNT_DEDUCT_PK primary key(ID)
);
