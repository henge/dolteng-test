create table PRODUCTS_OPTIONS (
    PRODUCT_ID int not null,
    OPTION_ID int not null,
    constraint PRODUCTS_OPTIONS_PK primary key(PRODUCT_ID, OPTION_ID)
);
