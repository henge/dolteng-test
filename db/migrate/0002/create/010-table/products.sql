create table PRODUCTS (
    ID int not null auto_increment,
    CATEGORY_ID int not null,
    NAME varchar(100) not null,
    PRICE int not null,
    TYPE int not null,
    VALID_FLG int not null,
    REGISTERED timestamp not null,
    constraint PRODUCTS_PK primary key(ID)
);
