create table ADDITIONS (
    ID int not null auto_increment,
    PRODUCT_ID int not null,
    INFO varchar(50),
    constraint ADDITIONS_PK primary key(ID)
);
