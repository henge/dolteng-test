create table CATEGORIES (
    ID int not null auto_increment,
    NAME varchar(100),
    constraint CATEGORIES_PK primary key(ID)
);
