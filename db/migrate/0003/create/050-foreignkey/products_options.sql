alter table PRODUCTS_OPTIONS add constraint PRODUCTS_OPTIONS_FK1 foreign key (product_id) references PRODUCTS (id);
alter table PRODUCTS_OPTIONS add constraint PRODUCTS_OPTIONS_FK2 foreign key (option_id) references OPTIONS (id);
